import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelutilizadorComponent } from './painelutilizador.component';
import { FormsModule } from '@angular/forms';
import { MatSelectModule, MatStepperModule, MatTabsModule, MatIconModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/material.module';

describe('PainelutilizadorComponent', () => {
  let component: PainelutilizadorComponent;
  let fixture: ComponentFixture<PainelutilizadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainelutilizadorComponent ],
      imports: [FormsModule, MatSelectModule, MatStepperModule, MatTabsModule, HttpClientModule, MatIconModule, MaterialModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelutilizadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
