import * as THREE from 'three';
import * as TWEEN from "tween.js"
import { Injectable } from '@angular/core';
import { Decoracao } from '../visualizador/decoracao';
import { Luz } from '../visualizador/luz';
import { OrbitControls } from 'three-orbitcontrols-ts';
import { Acabamento } from '../models/acabamento';
import { Item } from '../models/item';
import { GeraModelo } from '../visualizador/geraModelo';
import { AcabamentoService } from 'src/app/services/acabamento.service';
import { HistoricoPrecoService } from 'src/app/services/historico-preco.service';
import { Material } from '../models/material';

@Injectable({
  providedIn: 'root'
})
export class VisualizadorService {

  private canvas: HTMLCanvasElement;
  private renderer: THREE.WebGLRenderer;
  private camera: THREE.PerspectiveCamera;
  private scene: THREE.Scene;
  private light: THREE.AmbientLight;
  public spotLight: THREE.SpotLight;
  private controlsPerspective: OrbitControls;
  // clique do rato
  private mouse = new THREE.Vector2();
  private raycaster = new THREE.Raycaster();

  public item = new Item();

  constructor(private decoracao: Decoracao, private luz: Luz, private geraModelo: GeraModelo, private acabamentoService: AcabamentoService, private historicoPrecoService: HistoricoPrecoService) {
  }

  createScene(elementId: string): void {
    console.log("createScene");
    // The first step is to get the reference of the canvas element from our HTML document
    this.canvas = <HTMLCanvasElement>document.getElementById(elementId);
    this.canvas.addEventListener('mousedown', (event) => {
      var meshClicada;
      var itemClicado;
      let canvasBounds = this.renderer.context.canvas.getBoundingClientRect();
      this.mouse.x = ((event.clientX - canvasBounds.left) / (canvasBounds.right - canvasBounds.left)) * 2 - 1;
      this.mouse.y = - ((event.clientY - canvasBounds.top) / (canvasBounds.bottom - canvasBounds.top)) * 2 + 1;

      this.raycaster.setFromCamera(this.mouse, this.camera);
      let intersects = this.raycaster.intersectObjects(this.geraModelo.listaTodosMeshes);
      if (intersects.length > 0) {

        if (intersects[0].object.name != '') {
          meshClicada = intersects[0].object;
          itemClicado = this.geraModelo.buscarItemPorMesh(meshClicada);
          this.geraModelo.verificarEvento(meshClicada, itemClicado);
        }
      }
    });

    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      alpha: true,    // transparent background
      antialias: true, // smooth edges
      preserveDrawingBuffer: true
    });
    this.renderer.autoClear = true;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFShadowMap;
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    // create the scene
    this.scene = new THREE.Scene();

    // Visualizacao das Camaras
    this.camera = new THREE.PerspectiveCamera(
      75, window.innerWidth / window.innerHeight, 1, 2500
    );
    this.camera.position.z = 400;
    this.camera.position.y = 80;
    this.scene.add(this.camera);

    this.controlsPerspective = new OrbitControls(this.camera, this.renderer.domElement);
    this.controlsPerspective.target.set(0, 40, 0);
    this.controlsPerspective.minDistance = 80;
    this.controlsPerspective.maxDistance = 400;
    this.controlsPerspective.maxPolarAngle = (Math.PI / 2.2);    // para não deixar ver abaixo do nível do solo
    this.controlsPerspective.enablePan = false;
    this.controlsPerspective.enableDamping = false;

    // Decoracao da cena
    this.decoracao.init(this.scene, this.renderer, this.camera);

    // soft ambient light and set lights
    this.light = new THREE.AmbientLight(0xffffff, 0.7);
    this.scene.add(this.light);

    this.spotLight = new THREE.SpotLight();
    this.spotLight = this.luz.ligarLuzHolofote(this.spotLight);
  }

  animate(): void {
    this.render();
    window.addEventListener('resize', () => {
      this.resize();
      this.camera.updateProjectionMatrix();
    });
  }

  render() {
    requestAnimationFrame(() => {
      this.render();
    });
    this.renderer.render(this.scene, this.camera);
  }

  resize() {
    let width = window.innerWidth;
    let height = window.innerHeight;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  ligarLuz() {
    this.luz.alterarStatusLuzHolofote(true, this.spotLight, this.scene);
  }

  desligarLuz() {
    this.luz.alterarStatusLuzHolofote(false, this.spotLight, this.scene);
  }

  alterarCor(cor) {
    this.luz.alterarCorHolofote(cor, this.spotLight);
  }

  alterarIntensidade(valor) {
    this.luz.alterarIntensidadeLuzHolofote(valor / 20, this.spotLight);
  }

  alterarCubo(altura, largura, profundidade, imagemTextura, material, acabamento: Acabamento, idProduto, itemsFilho): void {
    this.desenhaCubo(altura, largura, profundidade, imagemTextura, material, acabamento, idProduto, itemsFilho);
  }

  desenhaCubo(altura, largura, profundidade, imagemTextura, material: Material, acabamento, produto, itemsFilho): void {
    this.apagaCubo();
    this.item = new Item();
    this.item.altura = altura;
    this.item.posicaoY = altura / 2;
    this.item.largura = largura;
    this.item.profundidade = profundidade;
    this.item.itensFilhos = itemsFilho;
    this.item.idProduto = produto.id;
    this.item.idMaterial = material.id;
    this.item.urlMaterial = imagemTextura;
    this.item.material = material;
    this.item.acabamento = acabamento;
    this.item.idAcabamento = acabamento.id;
    this.item.produto = produto;

    this.historicoPrecoService.buscarPrecoAtualMaterial(material.id).subscribe(
      precoMaterial => {
        this.item.precoMaterial = precoMaterial;
        this.historicoPrecoService.buscarAcrescimoAtualMaterialAcabamento(material.id, acabamento.id).subscribe(
          precoAcabamento => {
            this.item.precoAcabamento = precoAcabamento;
            this.atribuirPai(this.item);
            if (this.item.calcularPosicaoFilhos()) {
              console.log(this.item);
              GeraModelo.precoTotal =0;
              this.geraModelo.desenharItem(this.item, this.scene);

            } else {
              alert("O/s Item/s filho selecionado/s não cabe/m dentro do Item a Criar");
              this.apagaCubo();
            }
          }
        );
      }
    );




    //console.log("preço");
    //console.log(this.item.precoItem);

  }

  atribuirPai(item: Item): void {
    let i;
    if (typeof item.itensFilhos !== 'undefined')
      for (i = 0; i < item.itensFilhos.length; i++) {
        item.itensFilhos[i].itemPai = item;
        this.atribuirPai(item.itensFilhos[i]);
      }
  }


  apagaCubo() {
    this.geraModelo.removerTodosItens();
    this.geraModelo.removerTodasMeshes2();
    this.geraModelo.removerTodasMeshes(this.item, this.scene);
  }
}
