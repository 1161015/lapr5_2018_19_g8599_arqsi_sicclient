import { Component, OnInit } from '@angular/core';
import { Produto } from '../../models/produto';
import { ProdutoService } from 'src/app/services/produto.service';
import { Material } from 'src/app/models/material';
import { MaterialService } from 'src/app/services/material.service';
import { Restricao } from '../../models/restricao';
import { RestricaoService } from 'src/app/services/restricao.service';
@Component({
  selector: 'app-restricao',
  templateUrl: './restricao.component.html',
  styleUrls: ['./restricao.component.css']
})
export class RestricaoComponent implements OnInit {

  /**
   * variáveis globais (usadas no criar,apagar,editar,etc)
   */

  listaProdutos: Produto[];
  listaRestricoes: Restricao[];

  /**
   * variáveis do criar restrição
   */

  produtoPaiCriar: Produto;
  produtoFilhoCriar: Produto;
  listaProdutosFilhos: Produto[];

  materialProduroCriar: Material;
  listaMateriaisPai: Material[];
  materialProduroConstituinteCriar: Material;
  listaMateriaisConstituinte: Material[];

  obrigatoriedade: boolean;

  alturaMinima: number;
  alturaMaxima: number;
  larguraMinima: number;
  larguraMaxima: number;
  profundidadeMinima: number;
  profundidadeMaxima: number;

  valorRadioButton: number;


  estadoRestricaoObrigatoriedade: boolean = false;
  estadoRestricaoPercentual: boolean = false;
  estadoRestricaoMaterial: boolean = false;

  /**
   * varíaveis do editar restrição
   */

  restricaoSelecionadaEditar: Restricao;

  alturaMinimaEditar: number;
  alturaMaximaEditar: number;
  larguraMinimaEditar: number;
  larguraMaximaEditar: number;
  profundidadeMinimaEditar: number;
  profundidadeMaximaEditar: number;

  materialProduroEditar: Material;
  materialProduroConstituinteEditar: Material;

  listaMateriaisPaiEditar: Material[];
  listaMateriaisConstituinteEditar: Material[];

  obrigatoriedadeEditar: boolean; // é o toggle

  editarObrigatoriedade: boolean = false;
  editarMaterial: boolean = false;
  editarPercentual: boolean = false;

  /**
   * variáveis do apagar restrição
   */

  restricaoSelecionadaApagar: Restricao;
  /**
   * flags de controlo da UI
   */
  restricaoCriar: boolean = false;
  restricoesListar: boolean = false;
  restricaoApagar: boolean = false;
  restricaoEditar: boolean = false;

  constructor(private produtoService: ProdutoService, private materialService: MaterialService, private restricaoService: RestricaoService) { }

  ngOnInit() {
    this.buscarProdutos();
    this.buscarRestricoes();
  }

  /**
   * funções de preenchimento globais
   */

  buscarRestricoes() {
    this.restricaoService.buscarRestricoes().subscribe(
      restricoes => {
        this.listaRestricoes = restricoes;
      }
    );
  }

  buscarProdutos() {
    this.produtoService.buscarProdutos().subscribe(produtos => { // aqui está o resultado

      this.listaProdutos = produtos;
    }, error => { // aqui está o erro

    }, () => { // aqui está quando completa sem erros

    });
  }

  /**
   * funções de atualizar restrição
   */

  atualizarInformacao() {
    if (this.restricaoSelecionadaEditar.obrigatoriedade != null) {
      this.editarObrigatoriedade = true;
      this.editarMaterial = false;
      this.editarPercentual = false;
      this.obrigatoriedadeEditar = this.restricaoSelecionadaEditar.obrigatoriedade;
    } else if (this.restricaoSelecionadaEditar.percentagemAlturaMinima != null) {
      this.alturaMinimaEditar = null;
      this.alturaMaximaEditar = null;
      this.larguraMinimaEditar = null;
      this.larguraMaximaEditar = null;
      this.profundidadeMinimaEditar = null;
      this.profundidadeMaximaEditar = null;

      this.editarObrigatoriedade = false;
      this.editarMaterial = false;
      this.editarPercentual = true;
      if (this.restricaoSelecionadaEditar.percentagemAlturaMinima != -1) {
        this.alturaMinimaEditar = this.restricaoSelecionadaEditar.percentagemAlturaMinima;
        this.alturaMaximaEditar = this.restricaoSelecionadaEditar.percentagemAlturaMaxima;
      }
      if (this.restricaoSelecionadaEditar.percentagemLarguraMinima != -1) {
        this.larguraMinimaEditar = this.restricaoSelecionadaEditar.percentagemLarguraMinima;
        this.larguraMaximaEditar = this.restricaoSelecionadaEditar.percentagemLarguraMaxima;
      }
      if (this.restricaoSelecionadaEditar.percentagemProfundidadeMinima != -1) {
        this.profundidadeMinimaEditar = this.restricaoSelecionadaEditar.percentagemProfundidadeMinima;
        this.profundidadeMaximaEditar = this.restricaoSelecionadaEditar.percentagemProfundidadeMaxima;
      }
    } else if (this.restricaoSelecionadaEditar.idMaterialPai != null) {
      this.editarObrigatoriedade = false;
      this.editarMaterial = true;
      this.editarPercentual = false;
      this.materialService.buscarMateriaisDeProduto(this.restricaoSelecionadaEditar.idPai).subscribe(
        materiais => {
          this.listaMateriaisPaiEditar = materiais;
        }
      );
      this.materialService.buscarMateriaisDeProduto(this.restricaoSelecionadaEditar.idFilho).subscribe(
        materiais => {
          this.listaMateriaisConstituinteEditar = materiais;
        }
      );
      this.materialService.buscarMaterialPorId(this.restricaoSelecionadaEditar.idMaterialPai).subscribe(
        material => {
          this.materialProduroEditar = material;
        }
      );
      this.materialService.buscarMaterialPorId(this.restricaoSelecionadaEditar.idMaterialFilho).subscribe(
        material => {
          this.materialProduroConstituinteEditar = material;
        }
      );
    }

  }

  atualizarRestricao() {
    if (this.restricaoSelecionadaEditar == null) {
      alert('Selecione a restrição que deseja alterar.');
      return;
    }
    let restricao = new Restricao();
    restricao.id = this.restricaoSelecionadaEditar.id;
    restricao.idPai = this.restricaoSelecionadaEditar.idPai;
    restricao.idFilho = this.restricaoSelecionadaEditar.idFilho;
    if (this.obrigatoriedadeEditar != null) {
      restricao.obrigatoriedade = this.obrigatoriedadeEditar;
    } else if (this.materialProduroEditar != null) {
      restricao.idMaterialPai = this.materialProduroEditar.id;
      restricao.idMaterialFilho = this.materialProduroConstituinteEditar.id;
    } else {
      if (this.alturaMinimaEditar != null) {
        if (this.alturaMinimaEditar > this.alturaMaximaEditar || this.alturaMinimaEditar < 0 || this.alturaMaximaEditar > 100) {
          alert('Verifique as percentagens da altura');
          return;
        } else {
          console.log(this.alturaMinimaEditar);
          restricao.percentagemAlturaMinima = this.alturaMinimaEditar;
          restricao.percentagemAlturaMaxima = this.alturaMaximaEditar;
        }

      } else {
        restricao.percentagemAlturaMinima = -1;
        restricao.percentagemAlturaMaxima = -1;
      }
      if (this.larguraMinimaEditar != null) {
        if (this.larguraMinimaEditar > this.larguraMaximaEditar || this.larguraMinimaEditar < 0 || this.larguraMaximaEditar > 100) {
          alert('Verifique as percentagens da largura');
          return;
        } else {
          restricao.percentagemLarguraMinima = this.larguraMinimaEditar;
          restricao.percentagemLarguraMaxima = this.larguraMaximaEditar;
        }
      } else {
        restricao.percentagemLarguraMinima = -1;
        restricao.percentagemLarguraMaxima = -1;
      }
      if (this.profundidadeMinimaEditar != null) {
        if (this.profundidadeMinimaEditar > this.profundidadeMaximaEditar || this.profundidadeMinimaEditar < 0 || this.profundidadeMaximaEditar > 100) {
          alert('Verifique as percentagens da profundidade');
          return;
        } else {
          restricao.percentagemProfundidadeMinima = this.profundidadeMinimaEditar;
          restricao.percentagemProfundidadeMaxima = this.profundidadeMaximaEditar;
        }
      } else {
        restricao.percentagemProfundidadeMinima = -1;
        restricao.percentagemProfundidadeMaxima = -1;
      }

    }

    this.restricaoService.atualizarRestricao(restricao).subscribe(
      _ => {
        this.buscarRestricoes();
        this.obrigatoriedadeEditar = null;
        this.materialProduroEditar = null;
        this.materialProduroConstituinteEditar = null;
        this.larguraMinimaEditar = null;
        this.larguraMaximaEditar = null;
        this.alturaMinimaEditar = null;
        this.alturaMaximaEditar = null;
        this.profundidadeMinimaEditar = null;
        this.profundidadeMaximaEditar = null;
      }, _ => {
        this.obrigatoriedadeEditar = null;
        this.materialProduroEditar = null;
        this.materialProduroConstituinteEditar = null;
        this.larguraMinimaEditar = null;
        this.larguraMaximaEditar = null;
        this.alturaMinimaEditar = null;
        this.alturaMaximaEditar = null;
        this.profundidadeMinimaEditar = null;
        this.profundidadeMaximaEditar = null;
      }
    );

    // no fim resetar tudo

  }

  /**
   * funções de criar restrição
   */

  criarRestricao() {
    let novaRestricao = new Restricao();
    novaRestricao.idPai = this.produtoPaiCriar.produtoId;
    novaRestricao.idFilho = this.produtoFilhoCriar.produtoId;
    if (this.valorRadioButton == 1) { // se for restrição obrigatoriedade
      novaRestricao.obrigatoriedade = this.obrigatoriedade;
    } else if (this.valorRadioButton == 2) { // se for restrição percentual
      if (this.alturaMinima != null) {
        if (this.alturaMinima > this.alturaMaxima || this.alturaMinima < 0 || this.alturaMaxima > 100) {
          alert('Verifique as percentagens da altura');
          return;
        } else {
          novaRestricao.percentagemAlturaMinima = this.alturaMinima;
          novaRestricao.percentagemAlturaMaxima = this.alturaMaxima;
        }
      } else {
        novaRestricao.percentagemAlturaMinima = -1;
        novaRestricao.percentagemAlturaMaxima = 1;
      }
      if (this.larguraMinima != null) {
        if (this.larguraMinima > this.larguraMaxima || this.larguraMinima < 0 || this.larguraMaxima > 100) {
          alert('Verifique as percentagens da largura');
          return;
        } else {
          novaRestricao.percentagemLarguraMinima = this.larguraMinima;
          novaRestricao.percentagemLarguraMaxima = this.larguraMaxima;
        }
      } else {
        novaRestricao.percentagemLarguraMinima = -1;
        novaRestricao.percentagemLarguraMaxima = -1;
      }
      if (this.profundidadeMinima != null) {
        if (this.profundidadeMinima > this.profundidadeMaxima || this.profundidadeMinima < 0 || this.profundidadeMaxima > 100) {
          alert('Verifique as percentagens da profundidade');
          return;
        } else {
          novaRestricao.percentagemProfundidadeMinima = this.profundidadeMinima;
          novaRestricao.percentagemProfundidadeMaxima = this.profundidadeMaxima;
        }
      } else {
        novaRestricao.percentagemProfundidadeMinima = -1;
        novaRestricao.percentagemProfundidadeMaxima = -1;
      }
    } else if (this.valorRadioButton == 3) { // se for restrição de material
      novaRestricao.idMaterialFilho = this.materialProduroConstituinteCriar.id;
      novaRestricao.idMaterialPai = this.materialProduroCriar.id;
    }
    this.restricaoService.criarRestricao(novaRestricao).subscribe(
      _ => {
        this.buscarRestricoes();
      }, erro => {
        alert('Já existe essa restrição para os produtos selecionados');
      }, () => {

      }

    );

  }

  /**
   * funções para eliminar uma restrição
   */

  eliminarRestricao() {
    if (this.restricaoSelecionadaApagar == null) {
      alert('Por favor selecione uma restrição');
      return;
    }
    this.restricaoService.apagarRestricao(this.restricaoSelecionadaApagar.id).subscribe(
      _ => {
        this.buscarRestricoes();
        this.restricaoSelecionadaApagar = null;
      }, error => {
        alert(error);
      }
    );
  }



  listarFilhos() {
    this.produtoService.buscarProdutosFilho(this.produtoPaiCriar.produtoId).subscribe(
      produtosFilhos => {
        this.listaProdutosFilhos = produtosFilhos;
      }, error => {

      }, () => {

      }
    );
  }

  restricaoObrigatoriedade() {
    if (!this.validarProdutosSelecionados()) return;
    this.estadoRestricaoObrigatoriedade = !this.estadoRestricaoObrigatoriedade;
    this.estadoRestricaoPercentual = false;
    this.estadoRestricaoMaterial = false;
  }

  restricaoPercentualidade() {
    if (!this.validarProdutosSelecionados()) return;
    this.estadoRestricaoPercentual = !this.estadoRestricaoPercentual;
    this.estadoRestricaoMaterial = false;
    this.estadoRestricaoObrigatoriedade = false;
  }

  restricaoMaterial() {
    if (!this.validarProdutosSelecionados()) return;
    this.estadoRestricaoMaterial = !this.estadoRestricaoMaterial;
    this.estadoRestricaoObrigatoriedade = false;
    this.estadoRestricaoPercentual = false;
    this.materialService.buscarMateriaisDeProduto(this.produtoPaiCriar.produtoId).subscribe(
      materiais => {
        this.listaMateriaisPai = materiais;
      }
    );
    this.materialService.buscarMateriaisDeProduto(this.produtoFilhoCriar.produtoId).subscribe(
      materiais => {
        this.listaMateriaisConstituinte = materiais;
      }
    );
  }

  validarProdutosSelecionados(): boolean {
    if (this.produtoPaiCriar == null || this.produtoFilhoCriar == null) {
      alert('Certifique-se que selecionou ambos os produtos.');
      return false;
    }
    return true;
  }

  /**
   * funções de controlo da UI
   */

  limparDadosCriar() {
    this.produtoPaiCriar = null;
    this.produtoFilhoCriar = null;
    this.listaProdutosFilhos = [];

    this.materialProduroCriar = null;
    this.listaMateriaisPai = [];
    this.materialProduroConstituinteCriar = null;
    this.listaMateriaisConstituinte = [];

    this.obrigatoriedade = false;

    this.alturaMinima = null;
    this.alturaMaxima = null;
    this.larguraMinima = null;
    this.larguraMaxima = null;
    this.profundidadeMinima = null;
    this.profundidadeMaxima = null;

    this.valorRadioButton = null;

    this.restricaoSelecionadaEditar = null;

    this.alturaMinimaEditar = null;
    this.alturaMaximaEditar = null;
    this.larguraMinimaEditar = null;
    this.larguraMaximaEditar = null;
    this.profundidadeMinimaEditar = null;
    this.profundidadeMaximaEditar = null;

    this.materialProduroEditar = null;
    this.materialProduroConstituinteEditar = null;

    this.listaMateriaisPaiEditar = [];
    this.listaMateriaisConstituinteEditar = [];

    this.obrigatoriedadeEditar = null; // é o toggle

    this.editarObrigatoriedade = false;
    this.editarMaterial = false;
    this.editarPercentual = false;
  }



  estadoCriarRestricao() {
    this.restricaoCriar = !this.restricaoCriar;
    this.restricoesListar = false;
    this.restricaoApagar = false;
    this.restricaoEditar = false;
    this.limparDadosCriar();
  }

  estadoListarRestricoes() {
    this.restricoesListar = !this.restricoesListar;
    this.restricaoCriar = false;
    this.restricaoEditar = false;
    this.restricaoApagar = false;
    this.limparDadosCriar();
  }

  estadoApagarRestricao() {
    this.restricaoApagar = !this.restricaoApagar;
    this.restricaoCriar = false;
    this.restricaoEditar = false;
    this.restricoesListar = false;
    this.limparDadosCriar();
  }

  estadoEditarRestricao() {
    this.restricaoEditar = !this.restricaoEditar;
    this.restricaoCriar = false;
    this.restricaoApagar = false;
    this.restricoesListar = false;
    this.limparDadosCriar();
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }



}
