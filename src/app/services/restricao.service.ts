import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { Restricao } from '../models/restricao';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { ProdutoService } from './produto.service';
import { MaterialService } from './material.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RestricaoService {

  private restricaoURL = 'https://sic-productions.azurewebsites.net/api/restricao';
 // private restricaoURL = 'https://localhost:44352/api/restricao';

  constructor(private httpClient: HttpClient, private produtoService: ProdutoService, private materialService: MaterialService) { }

  private log(message: string) {
    console.log(message);
  }

  criarRestricao(restricao: Restricao): Observable<Restricao> {
    return this.httpClient.post<Restricao>(this.restricaoURL, restricao, httpOptions).pipe(
      tap(_ => {
        alert('Restrição adicionada')
      }, catchError(this.handleError<Restricao>('adicionar restrição')))
    );
  }

  atualizarRestricao(restricao: Restricao): Observable<any> {
    let url = this.restricaoURL + '/' + restricao.id;
    console.log(restricao);
    return this.httpClient.put(url, restricao, httpOptions).pipe(
      tap(_ => alert(`Restrição atualizada`)),
      catchError(this.handleError<any>('atualizar restrição'))
    );
  }

  apagarRestricao(idRestricao: number): Observable<Restricao> {
    let url = this.restricaoURL + '/' + idRestricao;
    return this.httpClient.delete<Restricao>(url, httpOptions).pipe(
      tap(_ => {
        alert('Restrição eliminada com sucesso');
      }, catchError(this.handleError('Apagar restrição')))
    );
  }

  buscarRestricoes(): Observable<Restricao[]> {
    return this.httpClient.get<Restricao[]>(this.restricaoURL).pipe(
      tap(restricoes => {
        console.log(restricoes);
        restricoes.forEach(restricaoAtual => {
          this.produtoService.buscarProdutoPorId(restricaoAtual.idPai).subscribe(
            produtoPai => {
              restricaoAtual.nomeProdutoPai = produtoPai.nome.conteudo;
            }
          );
          this.produtoService.buscarProdutoPorId(restricaoAtual.idFilho).subscribe(
            produtoFilho => {
              restricaoAtual.nomeProdutoFilho = produtoFilho.nome.conteudo;
            }
          );
          if (restricaoAtual.idMaterialPai != null) {
            this.materialService.buscarMaterialPorId(restricaoAtual.idMaterialPai).subscribe(materialPai => {
              restricaoAtual.nomeMaterialPai = materialPai.nome.conteudo;
            });
            this.materialService.buscarMaterialPorId(restricaoAtual.idMaterialFilho).subscribe(materialFilho => {
              restricaoAtual.nomeMaterialFilho = materialFilho.nome.conteudo;
            });
          }
          if (restricaoAtual.percentagemAlturaMaxima != null) { // restricao tamanho
            if (restricaoAtual.percentagemLarguraMinima == -1) {
              restricaoAtual.percentagemLarguraMinimaMostrar = '-';
              restricaoAtual.percentagemLarguraMaximaMostrar = '-';
            } else {
              restricaoAtual.percentagemLarguraMinimaMostrar = restricaoAtual.percentagemLarguraMinima.toString() + '%';
              restricaoAtual.percentagemLarguraMaximaMostrar = restricaoAtual.percentagemLarguraMaxima.toString() + '%';
            }
            if (restricaoAtual.percentagemAlturaMinima == -1) {
              restricaoAtual.percentagemAlturaMinimaMostrar = '-';
              restricaoAtual.percentagemAlturaMaximaMostrar = '-';
            } else {
              restricaoAtual.percentagemAlturaMinimaMostrar = restricaoAtual.percentagemAlturaMinima.toString() + '%';
              restricaoAtual.percentagemAlturaMaximaMostrar = restricaoAtual.percentagemAlturaMaxima.toString() + '%';
            }
            if (restricaoAtual.percentagemProfundidadeMinima == -1) {
              restricaoAtual.percentagemProfundidadeMinimaMostrar = '-';
              restricaoAtual.percentagemProfundidadeMaximaMostrar = '-';
            } else {
              restricaoAtual.percentagemProfundidadeMinimaMostrar = restricaoAtual.percentagemProfundidadeMinima.toString() + '%';
              restricaoAtual.percentagemProfundidadeMaximaMostrar = restricaoAtual.percentagemProfundidadeMaxima.toString()  + '%';
            }
          }
        })
      }), catchError(this.handleError<Restricao[]>('buscar restrições', [])));

  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
