import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { PainelutilizadorService } from './services/painelutilizador.service';

describe('PainelutilizadorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]}));

  it('should be created', () => {
    const service: PainelutilizadorService = TestBed.get(PainelutilizadorService);
    expect(service).toBeTruthy();
  });
});
