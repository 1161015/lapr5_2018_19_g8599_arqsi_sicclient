import { browser, by, element, ElementFinder } from 'protractor';

export class AppPage {

  //navigateToProduto(){
    //return browser.get('/produto');
  //}

  // navigateToAcabamento(){
  //   return browser.get('/acabamento');
  // }

  // navigateToCategoria(){
  //   return browser.get('/categoria');
  // }
  
  // navigateToMaterial(){
  //   return browser.get('/material');
  // }
  navigateTo() {
    return browser.get('/');
  }
  
  getItemNavigator() {
    return element(by.css('[routerlink="/item"]'));
  }

  navigateToItem(){
    return browser.get('/item');
  }

  navigateToLogin(){
    return browser.get('/login');
  }

  navigateToRegistar(){
    return browser.get('/registar');
  }

  navigateToEncomenda(){
    return browser.get('/encomenda');

  }
  
  getCriarButton(): ElementFinder{
    return element(by.id("botaoCriar"));
  }

  getRegistarButton(): ElementFinder{
    return element(by.id("botaoRegistar"));
  }

  getTermosButton(): ElementFinder{
    return element(by.id("TermosCondicoes"));
  }

  getTermosParaTratamentoButton(): ElementFinder{
    return element(by.id("TermosParaTratamento"));
  }

  getTermosVoltar(): ElementFinder{
    return element(by.id("voltarTermos"));
  }

  getTratamentoVoltar(): ElementFinder{
    return element(by.id("voltarTratamento"));
  }

  getTratamentoButton(): ElementFinder{
    return element(by.id("TratamentoDados"));
  }

  getVoltarButton(): ElementFinder{
    return element(by.id("botaoVoltar"));
  }

  getRecuperarButton(): ElementFinder{
    return element(by.id("botaoRecuperar"));
  }

  getListarButton(): ElementFinder{
    return element(by.id("botaoListar"));
  }

  getProcurarButton(): ElementFinder{
    return element(by.id("botaoProcurar"));
  }
  
  getEliminarButton(): ElementFinder{
    return element(by.id("botaoEliminar"));
  }

  getEditarButton(): ElementFinder{
    return element(by.id("botaoEditar"));
  }


  

}
