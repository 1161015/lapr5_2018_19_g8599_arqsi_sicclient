import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { LoginService } from '../services/login.service';

@Injectable({ providedIn: 'root' })
export class GateKeeper implements CanActivate {
    constructor(
        private router: Router,
        private loginService: LoginService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const utilizadorAtual = this.loginService.currentUserValue;
        if (utilizadorAtual) {
            // se a route for somente para um determinado tipo de utilizador
            if (route.data.roles && route.data.roles.indexOf(utilizadorAtual.cargo) === -1) {               //    se o cargo do utilizador, não estiver no vetor de cargos da route (route.data.roles.indexOf(currentUser.cargo) === -1 )
                // redirecionar para a homepage    
                alert('Não tem acesso a esta página, será redirecionado para a página inicial.')
                this.router.navigate(['/']);
                return false;
            }

            //autorizado
            return true;
        }  else { // se nao tiver logado
            if(route.url[0].path=='encomenda' || route.url[0].path=='item'){ // pode ver os itens e encomendas
                return true;
            }
        }

        // não está logado portanto redirecionar para a homepage
        alert('Para visualizar esta página é necessário efetuar login.');
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}