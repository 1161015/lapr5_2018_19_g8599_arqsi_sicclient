import { Component, OnInit } from '@angular/core';
import { Utilizador } from '../../models/utilizador';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { ItemComponent } from '../item/item.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mostrarLogin: boolean = true;

  mostrarBotaoLogin: boolean = true;
  mostrarSpinnerLogin: boolean = false;

  mostrarBotaoLogar: boolean = true;
  mostrarSpinnerLogar: boolean = false;

  mostrarBotaoVoltarRec: boolean = true;
  mostrarBotaoRec: boolean = true;
  mostrarSpinnerRecuperar: boolean = false;

  mostrarRecuperarPassword: boolean = false;
  mostrarAutenticacao: boolean = false;
  utilizador: Utilizador;

  hide = false;
  hide2 = true;

  constructor(private loginService: LoginService, private router: Router) {
    if (this.loginService.currentUserValue) { // não deixar um user já logado no painel de login
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    ItemComponent.podeCriarScene = true;
  }

  recuperarPassword() {
    this.mostrarLogin = false;
    this.mostrarRecuperarPassword = true;
    this.mostrarAutenticacao = false;
  }

  voltarAtras() {
    this.mostrarLogin = true;
    this.mostrarRecuperarPassword = false;
    this.mostrarAutenticacao = false;
  }

  loginRealizado() {
    this.mostrarLogin = false;
    this.mostrarRecuperarPassword = false;
    this.mostrarAutenticacao = false;
  }

  loginAutenticacaoDoisFatores() {
    this.mostrarLogin = false;
    this.mostrarRecuperarPassword = false;
    this.mostrarAutenticacao = true;
  }

  enviarPassword(emailInserido: string, usernameInserido: string) {
    var verificaEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailInserido);
    var verificaUsername = /^[a-z0-9]{3,}$/i.test(usernameInserido);
    if (!verificaUsername) {
      alert("Por favor, insira um Username válido.")
      return;
    }
    if (!verificaEmail) {
      alert("Por favor, insira um Email válido.");
      return;
    }

    this.mostrarBotaoVoltarRec = false;
    this.mostrarBotaoRec = false;
    this.mostrarSpinnerRecuperar = true;
    let utilizador = new Utilizador();

    utilizador.email = {
      email: emailInserido
    };
    utilizador.username = {
      username: usernameInserido
    };

    this.loginService.recuperarPassword(utilizador).subscribe(_ => {
      this.mostrarBotaoVoltarRec = true;
      this.mostrarBotaoRec = true;
      this.mostrarSpinnerRecuperar = false;

      alert('Se os dados introduzidos corresponderem a um utilizador do sistema, será enviado um email para "' + emailInserido + '" com a nova password.')
      this.voltarAtras();
    }, error => {
      alert('Ocorreu um erro a recuperar a password, contacte um administrador!');
      this.mostrarBotaoVoltarRec = true;
      this.mostrarBotaoRec = true;
      this.mostrarSpinnerRecuperar = false;
    });

  }

  logar(usernameInserido: string, passwordInserida: string) {
    var verificaUsername = /^[a-z0-9]{3,}$/i.test(usernameInserido);
    var verificarPassword = /[#$%^&*/\[\]()"\\:{}|<>]/.test(passwordInserida);
    var verificarPassword2 = /(.){8,}/.test(passwordInserida);
    if (!verificaUsername) {
      alert("Por favor, insira um Username válido.")
      return;
    }
    if (!verificarPassword2) {
      alert("Por favor, insira uma Password com mais que 8 caracteres.");
      return;
    }
    if (verificarPassword) {
      alert("Por favor, insira uma Password válida.");
      return;
    }
    this.mostrarBotaoLogin = false;
    this.mostrarSpinnerLogin = true;

    let utilizador = new Utilizador();

    utilizador.password = {
      password: passwordInserida
    };
    utilizador.username = {
      username: usernameInserido
    };

    this.loginService.entrarUtilizador(utilizador).subscribe(_ => {
      this.utilizador = utilizador;
      this.utilizador.password = null;
      //Tira Spinner
      this.mostrarBotaoLogin = true;
      this.mostrarSpinnerLogin = false;
      this.loginAutenticacaoDoisFatores();
      alert("Como passo final de autenticação, introduza o código enviado para o seu email.");
    }, error => {
      alert('Username ou Password inválido');
      this.mostrarBotaoLogin = true;
      this.mostrarSpinnerLogin = false;
    });

  }

  logarDoisFatores(codigoInserido: string) {
    var verificaCodigo = /^[a-z0-9]+$/i.test(codigoInserido);
    if (!verificaCodigo) {
      alert("Por favor, insira um código apenas com caracteres alfanuméricos.")
      return;
    }
    this.mostrarBotaoLogar = false;
    this.mostrarSpinnerLogar = true;

    this.utilizador.codigo = {
      codigo: codigoInserido
    }
    this.loginService.logarDoisFatores(this.utilizador).subscribe(_ => {
      this.utilizador = null;
      //Tira Spinner
      this.mostrarBotaoLogar = true;
      this.mostrarSpinnerLogar = false;
      this.loginRealizado();

      this.router.navigate(['/']);
    }, error => {
      alert('Código inserido Incorreto.');
      this.mostrarBotaoLogar = true;
      this.mostrarSpinnerLogar = false;
    });

  }
}
