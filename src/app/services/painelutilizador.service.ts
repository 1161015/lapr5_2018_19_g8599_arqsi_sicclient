import { Injectable } from '@angular/core';
import { Utilizador } from '../models/utilizador';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { LoginService } from './login.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PainelutilizadorService {

  private loginURL = 'https://sic-authz.azurewebsites.net/api/utilizador';
  //private loginURL = 'https://localhost:44309/api/utilizador';

  constructor(private httpClient: HttpClient, private loginService: LoginService) { }

  revalidarUtilizador(passwordRecebida: string): Observable<Utilizador> {
    let url = this.loginURL + '/revalidarutilizador';

    let utilizadorEnviar = new Utilizador();

    utilizadorEnviar.password = {
      password: passwordRecebida
    }

    utilizadorEnviar.id = this.loginService.currentUserValue.id;

    this.loginService.currentUserValue
    return this.httpClient.post<Utilizador>(url, utilizadorEnviar, httpOptions).pipe(
      tap(_ => {
      }, catchError(this.handleError<Utilizador>('entrar utilizador')))
    );
  }

  atualizarConta(novoNome: string, novaCidade: string, novaPassword: string, novoTratamento: boolean): Observable<any> {
    let url = this.loginURL + '/' + this.loginService.currentUserValue.id;

    let utilizadorEnviar = new Utilizador();

    utilizadorEnviar.nome = {
      nome: novoNome
    }
    utilizadorEnviar.morada = {
      morada: novaCidade
    }
    if (novaPassword == null) {
      utilizadorEnviar.password = null;
    } else {
      utilizadorEnviar.password = {
        password: novaPassword
      }
    }
    utilizadorEnviar.id = this.loginService.currentUserValue.id;
    utilizadorEnviar.tratamento = novoTratamento;

    console.log(utilizadorEnviar);
    return this.httpClient.put(url, utilizadorEnviar, httpOptions).pipe(
      tap(_ => { }),
      catchError(this.handleError<any>('atualizar conta de Utilizador'))
    );
  }

  apagarConta(): Observable<Utilizador> {
    let url = this.loginURL + '/' + this.loginService.currentUserValue.id;
    return this.httpClient.delete<Utilizador>(url, httpOptions).pipe(
      tap(_ => {
        //alert('Conta de Utilizador eliminada com sucesso');
      }), catchError(this.handleError<Utilizador>('Apagar conta de Utilizador'))
    );
  }

  buscarUtilizador(): Observable<Utilizador> {
    let url = this.loginURL + '/' + this.loginService.currentUserValue.id;

    return this.httpClient.get<Utilizador>(url).pipe(
      tap(_ => {
      }, catchError(this.handleError<Utilizador>('buscar utilizador')))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
