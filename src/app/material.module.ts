import { NgModule } from '@angular/core';
import { ColorPickerModule } from 'ngx-color-picker';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatListModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatTabsModule,
  MatStepperModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatRadioModule,
    MatSlideToggleModule,
    ColorPickerModule,
    MatSliderModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTabsModule,
    MatStepperModule,
    MatTableModule,
    MatTooltipModule
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatRadioModule,
    MatSlideToggleModule,
    ColorPickerModule,
    MatSliderModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTabsModule,
    MatStepperModule,
    MatTableModule,
    MatTooltipModule
  ]
})
export class MaterialModule {}