export class HistoricoPreco{
    id: number;
    id_Material: number;
    id_Acabamento: number;
    preco: {
        valor: number;
        dataInicio: Date;
    };

    nomeAcabamento : string; // Apenas para a UI
    nomeMaterial : string; // Apenas para a UI
    dataMostrar : string;
}