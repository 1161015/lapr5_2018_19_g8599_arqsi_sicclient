import { AppPage } from './app.po';
import { browser, by, element, promise, ElementFinder, ElementArrayFinder } from 'protractor';
describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();

  });

  // it('should display welcome message', () => {
  //   page.navigateTo();
  //   expect(page.getParagraphText()).toEqual('Welcome to sic-cliente!');
  // });


  // it('redirect to produto', () => {
  //   page.navigateToProduto();
  //   let elem = element(by.id('titulo')).getText();
  //   expect(elem).toEqual("Gestão de Produtos");
  // });

  //   it('redirect to criar produto', () => {
  //     page.navigateToProduto();
  //     const but =page.getCriarButton();
  //     but.click();
  //     let elem = element(by.id('tituloCriar')).getText();
  //     expect(elem).toEqual("Criar Produto");
  //   });

  //   it('redirect to listar produto', () => {
  //     page.navigateToProduto();
  //     const but =page.getListarButton();
  //     but.click();
  //     let elem = element(by.id('tituloListar')).getText();
  //     expect(elem).toEqual("Listar Produtos");
  //   });

  //   it('redirect to procurar produto', () => {
  //     page.navigateToProduto();
  //     const but =page.getProcurarButton();
  //     but.click();
  //     let elem = element(by.id('tituloProcurar')).getText();
  //     expect(elem).toEqual("Procurar Produto");
  //   });

  //   it('redirect to eliminar produto', () => {
  //     page.navigateToProduto();
  //     const but =page.getEliminarButton();
  //     but.click();
  //     let elem = element(by.id('tituloEliminar')).getText();
  //     expect(elem).toEqual("Eliminar Produto");
  //   });

  //   it('redirect to editar produto', () => {
  //     page.navigateToProduto();
  //     const but =page.getEditarButton();
  //     but.click();
  //     let elem = element(by.id('tituloEditar')).getText();
  //     expect(elem).toEqual("Editar Produto");
  //   });

  //   it('redirect to acabamento', () => {
  //     page.navigateToAcabamento();
  //     let elem = element(by.id('titulo')).getText();
  //     expect(elem).toEqual("Gestão de Acabamentos");
  //   });

  //   it('redirect to criar acabamento', () => {
  //     page.navigateToAcabamento();
  //     const but =page.getCriarButton();
  //     but.click();
  //     let elem = element(by.id('tituloCriar')).getText();
  //     expect(elem).toEqual("Criar Acabamento");
  //   });

  //   it('redirect to listar acabamento', () => {
  //     page.navigateToAcabamento();
  //     const but =page.getListarButton();
  //     but.click();
  //     let elem = element(by.id('tituloListar')).getText();
  //     expect(elem).toEqual("Listar Acabamentos");
  //   });

  //   it('redirect to procurar acabamento', () => {
  //     page.navigateToAcabamento();
  //     const but =page.getProcurarButton();
  //     but.click();
  //     let elem = element(by.id('tituloProcurar')).getText();
  //     expect(elem).toEqual("Procurar Acabamento");
  //   });

  //   it('redirect to eliminar acabamento', () => {
  //     page.navigateToAcabamento();
  //     const but =page.getEliminarButton();
  //     but.click();
  //     let elem = element(by.id('tituloEliminar')).getText();
  //     expect(elem).toEqual("Eliminar Acabamento");
  //   });

  //   it('redirect to editar acabamento', () => {
  //     page.navigateToAcabamento();
  //     const but =page.getEditarButton();
  //     but.click();
  //     let elem = element(by.id('tituloEditar')).getText();
  //     expect(elem).toEqual("Editar Acabamento");
  //   });
  // /* -------------------------------------------- */

  // it('redirect to categoria', () => {
  //   page.navigateToCategoria();
  //   let elem = element(by.id('titulo')).getText();
  //   expect(elem).toEqual("Gestão de Categorias");
  // });

  // it('redirect to criar categoria', () => {
  //   page.navigateToCategoria();
  //   const but =page.getCriarButton();
  //   but.click();
  //   let elem = element(by.id('tituloCriar')).getText();
  //   expect(elem).toEqual("Criar Categoria");
  // });

  // it('redirect to listar categoria', () => {
  //   page.navigateToCategoria();
  //   const but =page.getListarButton();
  //   but.click();
  //   let elem = element(by.id('tituloListar')).getText();
  //   expect(elem).toEqual("Listar Categorias");
  // });

  // it('redirect to procurar categoria', () => {
  //   page.navigateToCategoria();
  //   const but =page.getProcurarButton();
  //   but.click();
  //   let elem = element(by.id('tituloProcurar')).getText();
  //   expect(elem).toEqual("Procurar Categoria");
  // });

  // it('redirect to eliminar categoria', () => {
  //   page.navigateToCategoria();
  //   const but =page.getEliminarButton();
  //   but.click();
  //   let elem = element(by.id('tituloEliminar')).getText();
  //   expect(elem).toEqual("Eliminar Categoria");
  // });

  // it('redirect to editar categoria', () => {
  //   page.navigateToCategoria();
  //   const but =page.getEditarButton();
  //   but.click();
  //   let elem = element(by.id('tituloEditar')).getText();
  //   expect(elem).toEqual("Editar Categoria");
  // });

  // /* -------------------------------------------- */

  // it('redirect to item', () => {
  //   page.navigateToItem();
  //   let elem = element(by.id('titulo')).getText();
  //   expect(elem).toEqual("Gestão de Itens");
  // });

  // it('redirect to criar item', () => {
  //   page.navigateToItem();
  //   const but =page.getCriarButton();
  //   but.click();
  //   let elem = element(by.id('tituloCriar')).getText();
  //   expect(elem).toEqual("Criar Item");
  // });

  // it('redirect to listar item', () => {
  //   page.navigateToItem();
  //   const but =page.getListarButton();
  //   but.click();
  //   let elem = element(by.id('tituloListar')).getText();
  //   expect(elem).toEqual("Listar Itens");
  // });


  // it('redirect to eliminar item', () => {
  //   page.navigateToItem();
  //   const but =page.getEliminarButton();
  //   but.click();
  //   let elem = element(by.id('tituloEliminar')).getText();
  //   expect(elem).toEqual("Eliminar Item");
  // });

  // /* -------------------------------------------- */

  // it('redirect to material', () => {
  //   page.navigateToMaterial();
  //   let elem = element(by.id('titulo')).getText();
  //   expect(elem).toEqual("Gestão de Materiais");
  // });

  // it('redirect to criar material', () => {
  //   page.navigateToMaterial();
  //   const but =page.getCriarButton();
  //   but.click();
  //   let elem = element(by.id('tituloCriar')).getText();
  //   expect(elem).toEqual("Criar Material");
  // });

  // it('redirect to listar material', () => {
  //   page.navigateToMaterial();
  //   const but =page.getListarButton();
  //   but.click();
  //   let elem = element(by.id('tituloListar')).getText();
  //   expect(elem).toEqual("Listar Materiais");
  // });

  // it('redirect to procurar material', () => {
  //   page.navigateToMaterial();
  //   const but =page.getProcurarButton();
  //   but.click();
  //   let elem = element(by.id('tituloProcurar')).getText();
  //   expect(elem).toEqual("Procurar Material");
  // });

  // it('redirect to eliminar material', () => {
  //   page.navigateToMaterial();
  //   const but =page.getEliminarButton();
  //   but.click();
  //   let elem = element(by.id('tituloEliminar')).getText();
  //   expect(elem).toEqual("Eliminar Material");
  // });

  // it('redirect to editar material', () => {
  //   page.navigateToMaterial();
  //   const but =page.getEditarButton();
  //   but.click();
  //   let elem = element(by.id('tituloEditar')).getText();
  //   expect(elem).toEqual("Editar Material");
  // });
  // /* -------------------------------------------- */

  it('redirect to encomenda', () => {
    page.navigateToEncomenda();
    let elem = element(by.id('tituloEncomenda')).getText();
    expect(elem).toEqual("Gestão de Encomendas");
  });

  it('redirect to encomenda botaoCriar', () => {
    page.navigateToEncomenda();
    let elem = element(by.id('botaoCriar')).getText();
    expect(elem).toEqual("Criar Encomenda");
  });

  it('redirect to encomenda botaoListar', () => {
    page.navigateToEncomenda();
    let elem = element(by.id('botaoListar')).getText();
    expect(elem).toEqual("Listar Encomenda");
  });

  it('redirect to criar encomenda', () => {
    page.navigateToEncomenda();
    const but = page.getCriarButton();
    but.click();
    let elem = element(by.id('tituloCriar')).getText();
    expect(elem).toEqual("Criar Encomenda");
  });

  it('redirect to criar encomenda listaItems', () => {
    page.navigateToEncomenda();
    const but = page.getCriarButton();
    but.click();
    let elem = element(by.id('listaItems')).getText();
    expect(elem).toEqual("Itens");
  });

  it('redirect to criar encomenda listaCidades', () => {
    page.navigateToEncomenda();
    const but = page.getCriarButton();
    but.click();
    let elem = element(by.id('listaCidades')).getText();
    expect(elem).toEqual("Cidade");
  });

  it('redirect to listar encomenda', () => {
    page.navigateToEncomenda();
    const but = page.getListarButton();
    but.click();
    let elem = element(by.id('tituloListar')).getText();
    expect(elem).toEqual("Listar Encomendas");
  });

  it('redirect to listar encomenda listaEncomendas', () => {
    page.navigateToEncomenda();
    const but = page.getListarButton();
    but.click();
    let elem = element(by.id('listaEncomendas')).getText();
    expect(elem).toEqual("Encomendas");
  });

  /* -------------------------------------------- */
  
  it('redirect to login', () => {
    page.navigateToLogin();
    let elem = element(by.id('tituloLogin')).getText();
    expect(elem).toEqual("Login");
  });

  it('redirect to login nomeUtilizador', () => {
    page.navigateToLogin();
    let elem = element(by.id('nomeUtilizador')).getText();
    expect(elem).toEqual("");
  });

  it('redirect to login passwordUtilizador', () => {
    page.navigateToLogin();
    let elem = element(by.id('passwordUtilizador')).getText();
    expect(elem).toEqual("");
  });

  //<---------------------------------------------------->

  it('redirect to registar', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('tituloRegistar')).getText();
    expect(elem).toEqual("Registar Utilizador");
  });

  it('redirect to registar nomeUtilizadorRegisto', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('nomeUtilizadorRegisto')).getText();
    expect(elem).toEqual("");
  });

  it('redirect to registar emailUtilizadorRegisto', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('emailUtilizadorRegisto')).getText();
    expect(elem).toEqual("");
  });
  
  it('redirect to registar moradaUtilizadorRegisto', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('moradaUtilizadorRegisto')).getText();
    expect(elem).toEqual("Morada do Utilizador");
  });

  it('redirect to registar usernameUtilizadorRegisto', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('usernameUtilizadorRegisto')).getText();
    expect(elem).toEqual("");
  });

  it('redirect to registar passwordUtilizadorRegisto', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('passwordUtilizador')).getText();
    expect(elem).toEqual("");
  });

  it('redirect to registar passwordVerificarUtilizadorRegisto', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('passwordUtilizadorConfirmar')).getText();
    expect(elem).toEqual("");
  });

  it('redirect to registar TermosCondicoes', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('TermosCondicoes')).getText();
    expect(elem).toEqual("Termos e Condições");
  });

  it('redirect to registar TratamentoDados', () => {
    page.navigateToLogin();
    const but = page.getRegistarButton();
    but.click();
    let elem = element(by.id('TratamentoDados')).getText();
    expect(elem).toEqual("Tratamento de Dados Pessoais");
  });

  it('redirect to registar Div Termos', () => {
    page.navigateToRegistar();
    const but = page.getTermosButton();
    but.click();
    let elem = element(by.id('tituloTermos')).getText();
    expect(elem).toEqual("Termos e Condições de Serviço");
  });

  it('redirect to registar SiteSic', () => {
    page.navigateToRegistar();
    const but = page.getTermosButton();
    but.click();
    let elem = element(by.id('SiteSic')).getText();
    expect(elem).toEqual("https://sic-cliente.herokuapp.com");
  });

  it('redirect to registar TermosParaTratamento', () => {
    page.navigateToRegistar();
    const but = page.getTermosButton();
    but.click();
    let elem = element(by.id('TermosParaTratamento')).getText();
    expect(elem).toEqual("Tratamento de Dados Pessoais");
  });

  it('redirect to registar TermosParaTratamento Navigation', () => {
    page.navigateToRegistar();
    const but = page.getTermosButton();
    but.click();
    const but2 = page.getTermosParaTratamentoButton();
    but2.click();
    let elem = element(by.id('tituloTratamento')).getText();
    expect(elem).toEqual("Política de Tratamento de Dados Pessoais");
  });

  it('redirect to registar Termos Voltar', () => {
    page.navigateToRegistar();
    const but = page.getTermosButton();
    but.click();
    const but2 = page.getTermosVoltar();
    but2.click();
    let elem = element(by.id('tituloRegistar')).getText();
    expect(elem).toEqual("Registar Utilizador");
  });

  it('redirect to registar Div Tratamento', () => {
    page.navigateToRegistar();
    const but = page.getTratamentoButton();
    but.click();
    let elem = element(by.id('tituloTratamento')).getText();
    expect(elem).toEqual("Política de Tratamento de Dados Pessoais");
  });

  it('redirect to registar Tratamento Voltar', () => {
    page.navigateToRegistar();
    const but = page.getTratamentoButton();
    but.click();
    const but2 = page.getTratamentoVoltar();
    but2.click();
    let elem = element(by.id('tituloRegistar')).getText();
    expect(elem).toEqual("Registar Utilizador");
  });
  //<-------------------------------------------------------->

  it('redirect to recuperar password', () => {
    page.navigateToLogin();
    const but = page.getRecuperarButton();
    but.click();
    let elem = element(by.id('tituloRecuperacarPassword')).getText();
    expect(elem).toEqual("Recuperar Password");
  });

  it('redirect to recuperar password emailUtilizadorRecuperacao', () => {
    page.navigateToLogin();
    const but = page.getRecuperarButton();
    but.click();
    let elem = element(by.id('emailUtilizadorRecuperacao')).getText();
    expect(elem).toEqual("");
  });
  
  it('redirect to recuperar password nomeUtilizadorRecuperacao', () => {
    page.navigateToLogin();
    const but = page.getRecuperarButton();
    but.click();
    let elem = element(by.id('nomeUtilizadorRecuperacao')).getText();
    expect(elem).toEqual("");
  });

  // it('should have routelink to item', () => {
  //   page.navigateTo();
  //   let elem =page.getItemNavigator().click();
  //   expect(elem).toEqual('Gestão de Items');
  // });


  // it('redirect to item', () => {
  //   page.navigateToItem();
  //   let elem = element(by.id('botaoCriar')).getText();
  //   expect(elem).toEqual("Criar Item");
  // });

});
