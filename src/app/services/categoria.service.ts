import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Categoria } from '../models/categoria';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  
  private categoriaURL = 'https://sic-productions.azurewebsites.net/api/categoria'

  constructor(private http: HttpClient) { }

  private log(message: string): void {
    console.log(message);
  }

  buscarCategorias(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(this.categoriaURL).pipe(
      tap(pr => {
        this.log('Categorias carregados com sucesso');
        pr.forEach(categoria => {
          if (categoria.categoriaPai == null) {
            categoria.nomeCategoriaPai = 'sem pai';
          } else {
            categoria.nomeCategoriaPai = categoria.categoriaPai.nome.conteudo;
          }
        });
      }),
      catchError(this.handleError('buscar categorias', []))
    );
  }

  podeTerFilhos(idCategoria: number): Observable<boolean> {
    console.log('vou enviar este id da categoria ' + idCategoria);
    let url = this.categoriaURL + '/podeTerFilhos/' + idCategoria;
    return this.http.get<boolean>(url).pipe(
      tap(_ => { }),
      catchError(this.handleError<boolean>('Verificar se pode ter itens filho'))
    );
  }

  eliminarCategoria(idCategoria: number): Observable<Categoria> {
    let url = this.categoriaURL + '/' + idCategoria;
    return this.http.delete<Categoria>(url, httpOptions).pipe(
      tap(_ => alert('Categoria eliminada')),
      catchError(this.handleError<Categoria>('Apagar categoria'))
    );
  }

  atualizarCategoria(categoria: Categoria): Observable<any> {
    let url = this.categoriaURL + '/' + categoria.id;
    console.log(categoria);
    return this.http.put(url, categoria, httpOptions).pipe(
      tap(_ => alert('Categoria atualizada com sucesso.')),
      catchError(this.handleError<any>('atualizar categoria'))
    );
  }

  adicionarCategoria(categoria: Categoria): Observable<Categoria> {
    console.log(categoria);
    return this.http.post<Categoria>(this.categoriaURL, categoria, httpOptions).pipe(
      tap((_: Categoria) => alert('Categoria criada.')),
      catchError(this.handleError<Categoria>('adicionar categoria'))
    );
  }

  buscarCategoriaPorId(id: number): Observable<Categoria> {
    let urlFinal = this.categoriaURL + '/' + id;
    return this.http.get<Categoria>(urlFinal).pipe(
      tap(_ => {}),
      catchError(this.handleError<Categoria>('Erro ao obter categoria'))
    );
  }
  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }



}
