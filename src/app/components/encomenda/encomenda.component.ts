import { Component, OnInit } from '@angular/core';
import { Encomenda } from 'src/app/models/encomenda';
import { EncomendaService } from 'src/app/services/encomenda.service';
import { Item } from 'src/app/models/item';
import { ItemService } from 'src/app/services/item.service';
import { Cidade } from 'src/app/models/cidade';
import { LoginService } from 'src/app/services/login.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MaterialService } from 'src/app/services/material.service';
import { AcabamentoService } from 'src/app/services/acabamento.service';
import { ProdutoService } from 'src/app/services/produto.service';
import { ItemComponent } from '../item/item.component';

@Component({
  selector: 'app-encomenda',
  templateUrl: './encomenda.component.html',
  styleUrls: ['./encomenda.component.css']
})
export class EncomendaComponent implements OnInit {

  encomendas: Encomenda[] = [];
  encomendasCliente: Encomenda[];
  encomendaSelecionadaListagem: Encomenda;
  listaItens: Item[] = [];
  cidades: Cidade[] = [];
  itensSelecionados: Item[] = [];
  cidadeSelecionada: Cidade = null;
  mostrarListarEncomenda: boolean = false;
  mostrarCriarEncomenda: boolean = false;
  penultimoPasso: boolean = false;
  estadoPretendido: string;
  firstFormGroup: FormGroup;

  constructor(private encomendaService: EncomendaService, private itemService: ItemService, private loginService: LoginService, private _formBuilder: FormBuilder,
    private materialService: MaterialService, private acabamentoService: AcabamentoService, private produtoService: ProdutoService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.buscarEncomendas();
    this.buscarItens();
    this.buscarCidades();
    ItemComponent.podeCriarScene = true;
    // this.buscarEncomenda({
    //   selectedIndex :0
    // });
  }

  buscarCidades(): any {
    this.encomendaService.buscarCidades().subscribe(cidades => {
      this.cidades = cidades;
    });
  }

  buscarItens() {
    this.itemService.buscarItens().subscribe(itens => {
      console.log(itens);
      this.listaItens = itens;
    });
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  buscarEncomendas() {
    this.encomendaService.buscarEncomendas().subscribe(encomendas => {
      this.encomendas = encomendas;
    }
    );
  }

  buscarEncomenda(evento: any) {
    this.encomendaSelecionadaListagem = null;
    if (this.loginService.currentUserValue == null) {
      alert('Precisa de estar logado para listar as suas encomendas.');
      return;
    }
    this.penultimoPasso = false;
    let estadoProcurar: string = "";
    switch (evento.selectedIndex) {
      case 0:
        estadoProcurar = "Submetida";
        break;
      case 1:
        estadoProcurar = "Validada";
        break;
      case 2:
        estadoProcurar = "Assignada";
        break;
      case 3:
        estadoProcurar = "Em produção";
        break;
      case 4:
        estadoProcurar = "Em embalamento";
        break;
      case 5:
        estadoProcurar = "Pronta expedir";
        break;
      case 6:
        estadoProcurar = "Expedida";
        break;
      case 7:
        estadoProcurar = "Entregue";
        this.penultimoPasso = true;
        break;
      case 8:
        estadoProcurar = "Rececionada";
        break;
      default:
        return;
    }
    this.encomendaService.buscarEncomendasClienteEstado(estadoProcurar).subscribe(
      encomendasCliente => {
        encomendasCliente.forEach(encomendaAtual => {
          encomendaAtual.todosItens = [];
          this.preencherTodosItens(encomendaAtual);
          encomendaAtual.numeroItensEncomenda = encomendaAtual.todosItens.length;
          let dataDividida = encomendaAtual.dataEncomenda.split('T');
          let dia = dataDividida[0];
          let horas = dataDividida[1].split('\.');
          encomendaAtual.dataEncomendaUI = dia + ' ' + horas[0];
        });
        this.encomendasCliente = encomendasCliente;
      }, _ => {
        alert('Ocorreu um erro contacte um administrador');
      }
    );

  }

  preencherTodosItens(encomenda: Encomenda): void {
    encomenda.itens.forEach(itemRaiz => {
      this.preencherTodosItensRecursivamente(itemRaiz, encomenda);
    });
  }

  preencherTodosItensRecursivamente(item: Item, encomenda: Encomenda): void {
    encomenda.todosItens.push(item);
    this.materialService.buscarMaterialPorId(item.idMaterial).subscribe(material => {
      item.nomeMaterial = material.nome.conteudo;
    });
    this.acabamentoService.buscarAcabamentoPorId(item.idAcabamento).subscribe(acabamento => {
      item.nomeAcabamento = acabamento.nome.conteudo;
    });
    this.produtoService.buscarProdutoPorId(item.idProduto).subscribe(produto => {
      item.nomeProduto = produto.nome.conteudo;
    });
    for (let i = 0; i < item.itensFilhos.length; i++) {
      this.preencherTodosItensRecursivamente(item.itensFilhos[i], encomenda);
    }
  }

  confirmarRececao() {
    if (this.encomendaSelecionadaListagem == null) {
      alert('Por favor selecione uma encomenda');
      return;
    }
    let encomenda = new Encomenda();
    encomenda.idEncomenda = this.encomendaSelecionadaListagem.idEncomenda;
    this.encomendaService.atualizarEncomenda(encomenda).subscribe(
      _ => {
        this.encomendaSelecionadaListagem = null;
        this.encomendaService.buscarEncomendasClienteEstado("Entregue").subscribe(
          encomendasCliente => {
            this.encomendasCliente = encomendasCliente;
          }, _ => {
            alert('Ocorreu um erro contacte um administrador');
          }
        );
      }
    );
  }

  adicionarEncomenda() {
    if (this.itensSelecionados.length == 0) {
      alert('Por favor selecione pelo menos um item.');
      return;
    }
    if (this.cidadeSelecionada == null) {
      alert('Por favor selecione uma cidade.');
      return;
    }
    let idsItens = [];
    let encomendaPrecoTotal = 0;
    this.itensSelecionados.forEach(itemAtual => {
      encomendaPrecoTotal += itemAtual.preco;
    });
    this.itensSelecionados.forEach(item => {
      idsItens.push(item.idItem);
    });

    if (confirm('Esta encomenda terá o preço de: ' + encomendaPrecoTotal + '€, pretende efetuar a encomenda?')) {
      this.encomendaService.adicionarEncomenda({
        Itens: idsItens,
        Morada: this.cidadeSelecionada.idCidade,
        Preco: encomendaPrecoTotal
      } as Encomenda).subscribe(_ => {
        this.itensSelecionados = [];
        this.cidadeSelecionada = null;
        this.buscarEncomendas();
      }/*, _ => {
  
        alert('Impossível efetuar a encomenda, verifique todas as restrições.');
      }*/);
    }



  }

  estadoCriarEncomenda() {
    this.encomendaSelecionadaListagem = null;
    this.mostrarCriarEncomenda = !this.mostrarCriarEncomenda;
    this.mostrarListarEncomenda = false;
  }

  estadoListarEncomendas() {
    this.encomendaSelecionadaListagem = null;
    this.mostrarListarEncomenda = !this.mostrarListarEncomenda;
    this.mostrarCriarEncomenda = false;
  }
}
