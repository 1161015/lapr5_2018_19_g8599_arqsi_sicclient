import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RestricaoService } from './services/restricao.service';

describe('RestricaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]}));

  it('should be created', () => {
    const service: RestricaoService = TestBed.get(RestricaoService);
    expect(service).toBeTruthy();
  });
});
