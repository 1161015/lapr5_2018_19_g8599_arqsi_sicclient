import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Material } from '../models/material';
import { HistoricoPreco } from '../models/historicoPreco';
import { HistoricoPrecoService } from './historico-preco.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  private materialURL = 'https://sic-productions.azurewebsites.net/api/material';
  // private materialURL = 'https://localhost:44352/api/material';

  constructor(private http: HttpClient, private historicoService: HistoricoPrecoService) { }

  private log(message: string): void {
    console.log(message);
  }

  adicionarMaterial(material: Material): Observable<Material> {
    console.log(material);
    return this.http.post<Material>(this.materialURL, material, httpOptions).pipe(
      tap((material: Material) => alert(`Material adicionado`)),
      catchError(this.handleError<Material>('adicionar material'))
    );
  }

  buscarMateriaisDeProduto(idProduto: number): Observable<Material[]> {
    let url = this.materialURL + '/materiaisProduto/' + idProduto;
    return this.http.get<Material[]>(url).pipe(
      tap(materiais => {
        materiais.forEach(materialAtual => {
          this.historicoService.buscarPrecoAtualMaterial(materialAtual.id).subscribe(
            historicoAtual => {
              if (historicoAtual != null) {
                materialAtual.precoAtual = historicoAtual.preco.valor;
              }
            }
          );
        });
      },
        catchError(this.handleError('buscar materiais de um produto', [])))
    );
  }

  buscarMateriais(): Observable<Material[]> {
    return this.http.get<Material[]>(this.materialURL).pipe(
      tap(materiais => {
        materiais.forEach(materialAtual => {
          this.historicoService.buscarPrecoAtualMaterial(materialAtual.id).subscribe(
            historicoAtual => {
              if (historicoAtual != null) {
                materialAtual.precoAtual = historicoAtual.preco.valor;
              }
            }
          );
        });
      }),
      catchError(this.handleError('buscar materiais', [])
      )
    );
  }

  buscarMaterialPorId(id: number): Observable<Material> {
    let url = this.materialURL + '/' + id;
    return this.http.get<Material>(url).pipe(
      tap(materialAtual => {
        this.historicoService.buscarPrecoAtualMaterial(materialAtual.id).subscribe(
          historicoAtual => {
            if (historicoAtual != null) {
              materialAtual.precoAtual = historicoAtual.preco.valor;
            }
          }
        );
      }),
      catchError(this.handleError<Material>(`Erro ao buscar material com o id=${id}`))
    )
  }

  eliminarMaterial(id: number): Observable<Material> {
    let url = this.materialURL + '/' + id;
    return this.http.delete<Material>(url, httpOptions).pipe(
      tap(_ => alert(`Eliminado o material com sucesso`)),
      catchError(this.handleError<Material>('eliminar material'))
    );
  }

  atualizarMaterial(material: Material): Observable<any> {
    let url = this.materialURL + '/' + material.id;
    return this.http.put(url, material, httpOptions).pipe(
      tap(_ => alert(`Material atualizado`)),
      catchError(this.handleError<any>('atualizar material'))
    );
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
