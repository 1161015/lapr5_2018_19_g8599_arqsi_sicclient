export class Acabamento {
    id: number;
    nome: {conteudo:string};
    cor : string;
    opacidade : number;
    brilho : number;
    emissividade: number;
    fosco: boolean;
}