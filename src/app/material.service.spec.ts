import { TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';

import { MaterialService } from './services/material.service';

describe('MaterialService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
  ],
  }));

  it('should be created', () => {
    const service: MaterialService = TestBed.get(MaterialService);
    expect(service).toBeTruthy();
  });
});
