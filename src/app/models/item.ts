import * as THREE from 'three';
import { Injectable } from '@angular/core';
import { Acabamento } from './acabamento';
import { Material } from './material';
import { Categoria } from './categoria';
import { Produto } from './produto';
import { HistoricoPreco } from './historicoPreco';

@Injectable({
  providedIn: 'root'
})
export class Item {
    idItem: string;
    itensFilhos: Item[];
    idProduto: number;
    idMaterial: number;
    idAcabamento: number;
    altura: number;
    largura: number;
    profundidade: number;
    nomeProduto: string;
    nomeAcabamento: string;
    nomeMaterial: string;
    id_produto: number;
    id_material: number;
    id_acabamento: number;
    Altura: number;
    Largura: number;
    ItensFilhos: string[];
    Preco: number;
    Profundidade: number;
    _id: string;
    preco: number;

    urlMaterial : string; // para o geraModelo
    acabamento : Acabamento; // para o geraModelo
    material : Material; // para o geraModelo
    categoria : Categoria; // para o geraModelo
    produto : Produto; // para o GeraModelo

    precoMaterial : HistoricoPreco;
    precoAcabamento : HistoricoPreco;

    meshs = [];
    tipo: number; // para ser substituido pela categoria
    posicaoX = 0;
    posicaoY = this.altura/2; // (0,0,0) é a base do armário pai e não o seu centro
    posicaoZ = 0;
    posicaoTempX = 0;
    posicaoTempY = 0;
    posicaoTempZ = 0;

    itemPai = null;

    volumeItem = 0;
    areaItem = 0;
    precoItem = 0;
    precoArvore = 0;


    offsetProfundidade() : number {
      return 3 * this.espessura(); // este offset existe para evitar problemas com maçanetas e puxadores
    }

    espessura() : number {
      let x, resultado;
      if(this.largura > this.altura) x = this.largura;
      else x = this.altura;
      resultado = Math.floor(x/100);
      if(resultado < 2) return 2;
      return resultado;
    }

    resetPreco() : void {
      this.precoItem = 0;
      this.precoArvore = 0;
    }

    incrementarPreco(preco : number) : void {
      this.precoItem += preco;
    }

    // calcula o preço total de uma árvore de itens, a partir de um determinado item tido como raiz
    calcularPrecoArvore() : number {
      let totalFilhos = 0, i;
      if(typeof this.itensFilhos !== 'undefined')
      for(i = 0; i < this.itensFilhos.length; i++) {
        totalFilhos += this.itensFilhos[i].calcularPrecoArvore();
      }
      this.precoArvore = this.precoItem + totalFilhos;
      return this.precoArvore;
    }

    limparListaMeshs() : void {
      this.meshs = [];
    }

    adicionarMesh(mesh) : void {
      this.meshs.push(mesh);
    }

    redefinirProfundidade(profundidade : number) : boolean {
      // verificar se nenhum dos filhos tem profundidade menor ou igual
      let i;
      for(i = 0; i < this.itensFilhos.length; i++) {
        if(profundidade-this.offsetProfundidade() <= this.itensFilhos[i].profundidade) return false;
      }

      // verificar se cabe no pai
      if(this.itemPai != null) {
        if(profundidade >= this.itemPai.profundidade-this.offsetProfundidade()) return false;
      }

      this.profundidade = profundidade;
      this.posicaoZ = 0;
      if(this.itemPai == null) {
        for(i = 0; i < this.itensFilhos.length; i++) {
          this.itensFilhos[i].defineNovaPosicao();
        }
      } else {
        this.defineNovaPosicao();
      }

      return true;
    }

    redefinirLargura(largura : number) : boolean {
      let larguraAnterior = this.largura;
      this.largura = largura;

      // verificar se este item cabe no pai com a nova dimensao
      if(this.itemPai != null) {
        if(!this.itemPai.calcularPosicaoFilhos()) {
          this.largura = larguraAnterior;
          return false;
        }
      }

      // verificar se os filhos cabem com a nova largura
  	  if (!this.calcularPosicaoFilhos()) {
        this.largura = larguraAnterior;
        return false;
      }

      return true;
    }

  	redefinirAltura(altura : number) : boolean {
      let alturaAnterior = this.altura;
      this.altura = altura;

      // verificar se este item cabe no pai com a nova dimensao
      if(this.itemPai != null) {
        if(!this.itemPai.calcularPosicaoFilhos()) {
          this.altura = alturaAnterior;
          return false;
        }
      }

      // verificar se os filhos cabem com a nova altura
      if(this.itensFilhos.length > 0) {
        if(!this.calcularPosicaoFilhos()) {
          this.altura = alturaAnterior;
          return false;
        }
      }

      // no nosso projeto, o (0,0,0) é na base do armário e não no seu centro, logo o armário pai tem que ter um offset base
      if(this.itemPai == null) {
        this.posicaoY = altura / 2;
        let i;
        for(i = 0; i < this.itensFilhos.length; i++) {
          this.itensFilhos[i].defineNovaPosicao();
        }
      } else {
        this.defineNovaPosicao();
      }

      return true;
  	}

    // define os offsets das 3 dimensões, tendo em conta os valores temporários armazenados, as dimensões do próprio item e do itemPai
    defineNovaPosicao() : void {
      if(this.itemPai == null) return; // para evitar problemas com null pointers
      this.posicaoX = this.posicaoTempX + this.itemPai.posicaoX - this.itemPai.largura/2 + this.largura/2;
      this.posicaoY = this.posicaoTempY + this.itemPai.posicaoY - this.itemPai.altura/2 + this.altura/2;
      this.posicaoZ = this.posicaoTempZ + this.itemPai.posicaoZ - this.itemPai.profundidade/2 + this.profundidade/2 + this.espessura() + 1;
      let i;
      if(typeof this.itensFilhos !== 'undefined')
      for (i = 0; i < this.itensFilhos.length; i++) {
        this.itensFilhos[i].defineNovaPosicao();
      }
    }

    adicionarItemFilho(itemFilho : Item) : boolean {
      if(this.tipo != 0) return false; // não adiciona se o item pai não for armário
      if(itemFilho.largura >= this.largura) return false;
  	  if(itemFilho.altura >= this.altura) return false;
      if(itemFilho.profundidade >= this.profundidade-this.offsetProfundidade()) return false;

      // reorganiza o conteúdo do armário, de modo a encaixar o novo filho
      this.itensFilhos.push(itemFilho);
      if(!this.calcularPosicaoFilhos()) {
        this.itensFilhos.splice(this.itensFilhos.length-1, 1);
        return false;
      }

      // atribui a referência deste item como itemPai
      itemFilho.itemPai = this;

      return true;
    }

    calcularPosicaoFilhos() : boolean {
      if(this.itensFilhos.length == 0) return true; // não faz nada se não houver filhos
      // constroi uma matriz de booleans a false, do tamanho do item

      let i, j, matriz = [];
      for(i = 0; i < this.altura; i++) {
        matriz[i] = [];
        for(j = 0; j < this.largura; j++) {
          if(i < this.espessura() +1 || j < this.espessura() +1 || i >= this.altura - this.espessura() -1 || j >= this.largura - this.espessura()-1) { // para evitar colisões entre meshs das paredes
            matriz[i][j] = true;
          } else {
            matriz[i][j] = false;
          }
        }
  	  }

      // clona a lista de items filho para uma lista à parte, para poder ser manipulada
      let itensFilhosClone = [];
      for(i = 0; i < this.itensFilhos.length; i++) {
        itensFilhosClone.push(this.itensFilhos[i]);
      }

      while(itensFilhosClone.length > 0){

        // procura o filho com maior área - TODO: arranjar maneira de verificar variações nesta ordem - se falhar à primeira, testa com outras ordens
        let maxArea = 0, indexMaxArea, currArea;
        for(i = 0; i < itensFilhosClone.length; i++) {
          currArea = itensFilhosClone[i].altura * itensFilhosClone[i].largura;
          if(currArea > maxArea) {
            maxArea = currArea;
            indexMaxArea = i;
          }
        }

        // se o item não encaixa, retorna sem fazer alterações
        if(!this.encaixa(itensFilhosClone[indexMaxArea], matriz)) return false;
        itensFilhosClone.splice(indexMaxArea, 1);
      }

      // se todos os items filho encaixarem, define-lhes a nova posiçao
      for(i = 0; i < this.itensFilhos.length; i++) {
        this.itensFilhos[i].defineNovaPosicao();
      }

      return true;
    }

    // verifica se existe espaço suficiente para encaixar o item, tendo em conta a sua largura e altura
    espacoLivre(matriz, i : number, j : number, altura : number, largura : number) : boolean {
  	  let x, y;
      for(x = i; x < i + altura; x++) {
        for(y = j; y < j + largura; y++) {
          if (matriz[x][y]) return false;
        }
  	  }
      return true;
    }

    // 'encaixa' o item, ao meter a true o espaço correspondente
    encaixaAux(matriz, i : number, j : number, altura : number, largura : number) : void {
      let x, y;
      for (x = i; x < i + altura; x++) {
        for (y = j; y < j + largura; y++) {
          matriz[x][y] = true;
        }
      }
    }

    encaixa(item : Item, matriz) : boolean {
      let i, j;
      for (i = 0; i < matriz.length; i++) {
        for (j = 0; j < matriz[0].length; j++) {
          if (!matriz[i][j]) { // se o lugar estiver desocupado
            if (j + item.largura < matriz[0].length && i + item.altura < matriz.length) { // se ainda couber, numericamente, no espaço restante
              if(this.espacoLivre(matriz, i, j, item.altura, item.largura)) {
                item.posicaoTempX = j; // guarda os valores no atributo temporário da classe
                item.posicaoTempY = i;
                this.encaixaAux(matriz, i, j, item.altura, item.largura);
                return true;
              }
            }
          }
        }
      }

      return false;
    }


    completarItemPai() : void {
      let i;
      console.log("completarItemPai - after");
      console.log(this);

      for(i = 0; i < this.itensFilhos.length; i++) {
        this.itensFilhos[i].itemPai = this;
        this.itensFilhos[i].completarItemPai();
      }
    }



    // ----- Cálculo do preço de um item ----- //


}
