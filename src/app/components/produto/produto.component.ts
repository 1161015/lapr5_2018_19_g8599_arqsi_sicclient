import { Component, OnInit } from '@angular/core';
import { Categoria } from '../../models/categoria';
import { Material } from '../../models/material';
import { Produto } from '../../models/produto';
import { Catalogo } from '../../models/catalogo';
import { Colecao } from '../../models/colecao';
import { ProdutoService } from 'src/app/services/produto.service';
import { MaterialService } from 'src/app/services/material.service';
import { CategoriaService } from 'src/app/services/categoria.service';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { NgModel } from '@angular/forms';
import { ColecaoService } from 'src/app/services/colecao.service';
import { MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {

  titulo: string = "Gestão de Produtos";
  /* parametros pesquisa de produto*/
  produtoPesquisar: Produto;
  nomePesquisado: string = '';
  categoriaPesquisado: string = '';
  materiaisPesquisado: string = '';
  agregadosObrPesquisado: string = '';
  agregadosOpPesquisado: string = '';
  minPesquisado: string = '';
  maxPesquisado: string = '';
  restricaoPesquisado: string = '';
  alturaPesquisado: string = '';
  larguraPesquisado: string = '';
  profundidadePesquisado: string = '';

  /*parâmetros listagem de produto*/
  produtoListar: Produto = null;
  materiaisProduto: Material[] = [];
  alturasProduto: number[] = [];
  alturasMinProduto: number[] = [];
  alturasMaxProduto: number[] = [];
  largurasMinProduto: number[] = [];
  largurasMaxProduto: number[] = [];
  largurasProduto: number[] = [];
  profundidadesProduto: number[] = [];
  profundidadesMinProduto: number[] = [];
  profundidadesMaxProduto: number[] = [];
  produtosAgregados: string[] = [];
  produtoCatalogos: string[] = [];
  imagemTexturaListar: string[] = [];
  texturasListar: any[] = [];
  nomeTexturaListar: string[] = [];

  /*-------------------------------------*/

  /* parametros eliminar produto */
  produtoEliminar: Produto;
  /*-------------------------------------*/


  /* parametros edição de produto*/
  produtoEditar: Produto;
  /*-------------------------------------*/

  /* parametros partilhados por criação e edição de produto*/
  categoriaProduto: Categoria = null;
  colecaoProduto: Colecao = null;
  materialProduto: Material[] = [];
  listaFilhos: Produto[] = [];
  restricao: string;
  alturaDimensaoContinuaMin: number[] = [];
  alturaDimensaoContinuaMax: number[] = [];
  alturaDimensaoDiscreta: number[];
  contMinAlt: number;
  contMaxAlt: number;
  larguraDimensaoDiscreta: number[];
  larguraDimensaoContinuaMin: number[] = [];
  larguraDimensaoContinuaMax: number[] = [];
  contMinLarg: number;
  contMaxLarg: number;
  profundidadeDimensaoDiscreta: number[];
  profundidadeDimensaoContinuaMin: number[] = [];
  profundidadeDimensaoContinuaMax: number[] = [];
  contMinProf: number;
  contMaxProf: number;
  nomeProduto: string;
  percentagemMinima: number;
  percentagemMaxima: number;
  stringValoresAlt: string;
  stringValoresLarg: string;
  stringValoresProf: string;
  podeTerFilhos: boolean = false;

  /**
   * tooltips dimensões
   */
  larguraMinima : string  = "Largura mínima possível: 50";
  larguraMaxima : string = "Largura máxima possível: 300";
  alturaMinima : string = "Altura mínima possível: 50";
  alturaMaxima : string = "Altura máxima possível: 220";
  profundidadeMinima : string = "Profundidade mínima possível: 50";
  profundidadeMaxima : string = "Profundidade máxima possível: 200";
  profundidadeDiscreta : string = "Só são aceites valores entre 50 e 200";
  alturaDiscreta : string = "Só são aceites valores entre 50 e 220";
  larguraDiscreta : string = "Só são aceites valores entre 50 e 300";
  /*-------------------------------------*/

  /* listas de parametros usadas na criação e edição de produto */
  catalogosSelecionados: Catalogo[];
  listaCatalogos: Catalogo[];
  listaColecoes: Colecao[];
  listaCategorias: Categoria[];
  listaMateriais: Material[];
  listaProdutos: Produto[];
  listaOpcoes: string[] = ["Sim", "Não"];
  /*-------------------------------------*/

  /*COMPONENTES VISUAIS */

  /* Criação Produto */
  produtoHidden01: boolean = false;
  alturaContinuaHidden: boolean = false;
  alturaDiscretaHidden: boolean = false;
  larguraContinuaHidden: boolean = false;
  larguraDiscretaHidden: boolean = false;
  profundidadeContinuaHidden: boolean = false;
  profundidadeDiscretaHidden: boolean = false;
  /*-------------------------------------*/

  /* Edição Produto */

  produtoHidden05: boolean = false;
  alturaContinuaEditHidden: boolean = false;
  alturaDiscretaEditHidden: boolean = false;
  larguraContinuaEditHidden: boolean = false;
  larguraDiscretaEditHidden: boolean = false;
  profundidadeContinuaEditHidden: boolean = false;
  profundidadeDiscretaEditHidden: boolean = false;
  catalogosSelecionadosEditar: Catalogo[];
  listaColecoesEditar: Colecao[] = [];
  colecaoProdutoEditar: Colecao;
  /*-------------------------------------*/

  /* Eliminar Produto */
  produtoHidden04: boolean = false;
  /*-------------------------------------*/

  /* Listar Produtos */
  produtoHidden02: boolean = false;
  /*-------------------------------------*/

  /* Pesquisa Produto */
  produtoHidden03: boolean = false;
  /*-------------------------------------*/


  constructor(private produtoService: ProdutoService, private materialService: MaterialService, private categoriaService: CategoriaService
    , private catalogoService: CatalogoService, private colecaoService: ColecaoService) { }

  ngOnInit() {
    this.buscarProdutos();
    this.buscarMateriais();
    this.buscarCategorias();
    this.buscarCatalogos();
    document.getElementById("titulo").innerHTML = this.titulo;
    this.texturasListar.push({
      nome: 'Wenge 1',
      urlImagem: '../../../assets/materiais/madeira1.png'
    });
    this.texturasListar.push({
      nome: 'Wenge 2',
      urlImagem: '../../../assets/materiais/madeira2.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 1',
      urlImagem: '../../../assets/materiais/madeira3.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 2',
      urlImagem: '../../../assets/materiais/madeira4.jpg'
    });
    this.texturasListar.push({
      nome: 'Madeira 5',
      urlImagem: '../../../assets/materiais/madeira5.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 1',
      urlImagem: '../../../assets/materiais/vidro1.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 2',
      urlImagem: '../../../assets/materiais/vidro2.jpg'
    });
  }

  listarColecoesDisponiveis() {
    this.listaColecoes = [];
    if (this.catalogosSelecionados.length > 0) {
      this.catalogosSelecionados.forEach(catalogoAtual => {
        this.colecaoService.buscarColecoesDeCatalogo(catalogoAtual.catalogoId).subscribe(
          colecoes => {
            colecoes.forEach(colecaoAtual => {
              this.listaColecoes.push(colecaoAtual);
            });
          }
        )
      });
    }
  }

  verificarPodeFilhos() {

    this.categoriaService.podeTerFilhos(this.categoriaProduto.id).subscribe(
      retorno => {
        this.podeTerFilhos = retorno;
        if (!retorno) {
          this.listaFilhos = [];
        } else { // se for um armário atualizar as tooltips
          this.larguraMinima  = "Largura mínima possível: 60";
          this.larguraMaxima  = "Largura máxima possível: 300";
          this.alturaMinima  = "Altura mínima possível: 60";
          this.alturaMaxima  = "Altura máxima possível: 220";
          this.profundidadeMinima = "Profundidade mínima possível: 60";
          this.profundidadeMaxima  = "Profundidade máxima possível: 200";
          this.profundidadeDiscreta  = "Só são aceites valores entre 60 e 200";
          this.alturaDiscreta  = "Só são aceites valores entre 60 e 220";
          this.larguraDiscreta = "Só são aceites valores entre 60 e 300";
        }
      }
    );
  }

  listarColecoesCatalogosEditar() {
    this.listaColecoesEditar = [];
    this.catalogosSelecionadosEditar.forEach(catalogoAtual => {
      this.colecaoService.buscarColecoesDeCatalogo(catalogoAtual.catalogoId).subscribe(
        colecoes => {
          colecoes.forEach(colecaoAtual => {
            this.listaColecoesEditar.push(colecaoAtual);
          });
        }
      )
    });
  }

  buscarCatalogos() {
    this.catalogoService.buscarCatalogos().subscribe(catalogos => {
      this.listaCatalogos = catalogos;
    });
  }

  selecionarTodosCatalogos(checkAll, select: NgModel, values) {
    if (checkAll) {
      select.update.emit(values);
    } else {
      select.update.emit([]);
    }
  }

  /* metodos relacionados com edição de produto */
  atualizarDados() {
    //console.log(this.produtoEditar.id);
    this.nomeProduto = this.produtoEditar.nome.conteudo;
    let cat: Categoria;
    for (let i = 0; i < this.listaCategorias.length; i++) {
      if (this.listaCategorias[i].id == this.produtoEditar.categoria.id) {
        cat = this.listaCategorias[i];
        break;
      }
    }
    this.categoriaProduto = cat;
    this.listaColecoesEditar = [];
    if (this.produtoEditar.colecao != null) {
      this.colecaoService.buscarColecaoPorId(this.produtoEditar.colecao.id).subscribe(
        colecaoRecebida => {
          this.listaColecoesEditar.push(colecaoRecebida);
          this.colecaoProdutoEditar = colecaoRecebida;
        }
      );
    }
    this.catalogosSelecionadosEditar = [];
    this.catalogoService.buscarCatalogosProduto(this.produtoEditar.produtoId).subscribe(
      catalogos => {
        this.catalogosSelecionadosEditar = catalogos;
      }
    );
    let materiais: Material[] = [];
    for (let i = 0; i < this.produtoEditar.materiais.length; i++) {
      let idMaterial = this.produtoEditar.materiais[i].idMaterial;
      for (let j = 0; j < this.listaMateriais.length; j++) {
        let idAux = this.listaMateriais[j].id;
        if (idMaterial == idAux) {
          materiais.push(this.listaMateriais[j]);
        }
      }
    }
    this.materialProduto = materiais;
    this.categoriaService.podeTerFilhos(this.produtoEditar.categoria.id).subscribe(
      podeEditar => {
        this.podeTerFilhos = podeEditar;
        if (podeEditar) {
          this.produtoService.buscarProdutosFilho(this.produtoEditar.produtoId).subscribe(
            produtosFilhos => {
              this.larguraMinima  = "Largura mínima possível: 60";
              this.larguraMaxima  = "Largura máxima possível: 300";
              this.alturaMinima  = "Altura mínima possível: 60";
              this.alturaMaxima  = "Altura máxima possível: 220";
              this.profundidadeMinima = "Profundidade mínima possível: 60";
              this.profundidadeMaxima  = "Profundidade máxima possível: 200";
              this.profundidadeDiscreta  = "Só são aceites valores entre 60 e 200";
              this.alturaDiscreta  = "Só são aceites valores entre 60 e 220";
              this.larguraDiscreta  = "Só são aceites valores entre 60 e 300";
              this.listaFilhos = produtosFilhos;
            }
          );
        } else {
          this.larguraMinima    = "Largura mínima possível: 50";
          this.larguraMaxima   = "Largura máxima possível: 300";
          this.alturaMinima   = "Altura mínima possível: 50";
          this.alturaMaxima   = "Altura máxima possível: 220";
          this.profundidadeMinima   = "Profundidade mínima possível: 50";
          this.profundidadeMaxima   = "Profundidade máxima possível: 200";
          this.profundidadeDiscreta   = "Só são aceites valores entre 50 e 200";
          this.alturaDiscreta  = "Só são aceites valores entre 50 e 220";
          this.larguraDiscreta  = "Só são aceites valores entre 50 e 300";
          this.listaFilhos = [];
        }
      }
    );



  }

  atualizarAltura(tipo: number) {
    let str: string = '';
    if (tipo == 1) {
      let valoresMin: number[] = [];
      let valoresMax: number[] = [];
      if (this.produtoEditar.dimensoes.altura != null) {
        if (this.produtoEditar.dimensoes.altura[0].dimensaoMin != null) {

          for (let i = 0; i < this.produtoEditar.dimensoes.altura.length; i++) {
            valoresMin[i] = (this.produtoEditar.dimensoes.altura[i].dimensaoMin);
            valoresMax[i] = (this.produtoEditar.dimensoes.altura[i].dimensaoMax);
            str += "[" + valoresMin[i] + " - " + valoresMax[i] + "]";
          }

        }
      }
      this.alturaDimensaoContinuaMin = valoresMin;
      this.alturaDimensaoContinuaMax = valoresMax;
    } else {
      let valores: number[] = [];
      if (this.produtoEditar.dimensoes.altura != null) {
        if (this.produtoEditar.dimensoes.altura[0].dimensao != null) {
          for (let i = 0; i < this.produtoEditar.dimensoes.altura.length; i++) {
            valores.push(this.produtoEditar.dimensoes.altura[i].dimensao);
            str += this.produtoEditar.dimensoes.altura[i].dimensao + '; ';
          }
        }
      }
      this.alturaDimensaoDiscreta = valores;
    }
    this.stringValoresAlt = str;
  }

  atualizarLargura(tipo: number) {
    let str: string = '';
    if (tipo == 1) {
      let valoresMin: number[] = [];
      let valoresMax: number[] = [];
      if (this.produtoEditar.dimensoes.largura != null) {
        if (this.produtoEditar.dimensoes.largura[0].dimensaoMin != null) {
          for (let i = 0; i < this.produtoEditar.dimensoes.largura.length; i++) {
            valoresMin[i] = (this.produtoEditar.dimensoes.largura[i].dimensaoMin);
            valoresMax[i] = (this.produtoEditar.dimensoes.largura[i].dimensaoMax);
            str += "[" + valoresMin[i] + " - " + valoresMax[i] + "]";
          }
        }
      }
      this.larguraDimensaoContinuaMin = valoresMin;
      this.larguraDimensaoContinuaMax = valoresMax;
    } else {
      let valores: number[] = [];
      if (this.produtoEditar.dimensoes.largura != null) {
        if (this.produtoEditar.dimensoes.largura[0].dimensao != null) {
          for (let i = 0; i < this.produtoEditar.dimensoes.largura.length; i++) {
            valores.push(this.produtoEditar.dimensoes.largura[i].dimensao);
            str += this.produtoEditar.dimensoes.largura[i].dimensao + '; ';
          }
        }
      }
      this.larguraDimensaoDiscreta = valores;
    }
    this.stringValoresLarg = str;
  }

  atualizarProfundidade(tipo: number) {
    let str: string = '';
    if (tipo == 1) {
      let valoresMin: number[] = [];
      let valoresMax: number[] = [];
      if (this.produtoEditar.dimensoes.profundidade != null) {
        if (this.produtoEditar.dimensoes.profundidade[0].dimensaoMin != null) {
          for (let i = 0; i < this.produtoEditar.dimensoes.profundidade.length; i++) {
            valoresMin[i] = (this.produtoEditar.dimensoes.profundidade[i].dimensaoMin);
            valoresMax[i] = (this.produtoEditar.dimensoes.profundidade[i].dimensaoMax);
            str += "[" + valoresMin[i] + " - " + valoresMax[i] + "]";
          }
        }
      }
      this.profundidadeDimensaoContinuaMin = valoresMin;
      this.profundidadeDimensaoContinuaMax = valoresMax;
    } else {
      let valores: number[] = [];
      if (this.produtoEditar.dimensoes.profundidade != null) {
        if (this.produtoEditar.dimensoes.profundidade[0].dimensao != null) {
          for (let i = 0; i < this.produtoEditar.dimensoes.profundidade.length; i++) {
            valores.push(this.produtoEditar.dimensoes.profundidade[i].dimensao);
            str += this.produtoEditar.dimensoes.profundidade[i].dimensao + '; ';
          }
        }
      }
      this.profundidadeDimensaoDiscreta = valores;
    }
    this.stringValoresProf = str;
  }
  /*--------------------------------------------- */

  /*metodos de acesso a serviços */
  buscarProdutos() {
    this.produtoService.buscarProdutos().subscribe(produtos => {
      this.listaProdutos = produtos;
      console.log(this.listaProdutos);
    });
  }

  buscarCategorias() {
    this.categoriaService.buscarCategorias().subscribe(categorias => { this.listaCategorias = categorias; console.log(this.listaCategorias) });
  }

  buscarMateriais() {
    this.materialService.buscarMateriais().subscribe(materiais => { this.listaMateriais = materiais; });
  }
  /*---------------------------------------------- */

  /* metodos associados a manutençao de UI */
  limparDiscreta(tipo: number) {
    if (tipo == 1) {
      this.alturaDimensaoDiscreta = [];
      let string = '';
      document.getElementById("valoresDiscretaAltura").innerHTML = string;
    } else if (tipo == 2) {
      let string = '';
      document.getElementById("valoresDiscretaLargura").innerHTML = string;
      this.larguraDimensaoDiscreta = [];
    } else {
      let string = '';
      document.getElementById("valoresDiscretaProfundidade").innerHTML = string;
      this.profundidadeDimensaoDiscreta = [];
    }
  }

  limparContinua(tipo: number) {
    if (tipo == 1) {
      this.alturaDimensaoContinuaMin = [];
      this.alturaDimensaoContinuaMax = [];
      let string = '';
      document.getElementById("valoresContinuaAltura").innerHTML = string;
    } else if (tipo == 2) {
      let string = '';
      document.getElementById("valoresContinuaLargura").innerHTML = string;
      this.larguraDimensaoContinuaMin = [];
      this.larguraDimensaoContinuaMax = [];
    } else {
      let string = '';
      document.getElementById("valoresContinuaProfundidade").innerHTML = string;
      this.profundidadeDimensaoContinuaMin = [];
      this.profundidadeDimensaoContinuaMax = [];
    }

  }

  limparDadosCriarEditar() {
    // this.profundidadeMinima = "";
    // this.profundidadeMaxima = "";
    // this.alturaMinima = "";
    // this.alturaMaxima = "";
    // this.larguraMinima = "";
    // this.larguraMaxima = "";
    // this.profundidadeDiscreta = "";
    // this.alturaDiscreta = "";
    // this.larguraDiscreta = "";
    this.podeTerFilhos = false;
    this.categoriaProduto = null;
    this.materialProduto = [];
    this.listaFilhos = []
    this.restricao = null;
    this.alturaDimensaoDiscreta = [];
    this.contMinAlt = null;
    this.contMaxAlt = null;
    this.larguraDimensaoDiscreta = [];
    this.contMinLarg = null;
    this.contMaxLarg = null;
    this.profundidadeDimensaoDiscreta = [];
    this.contMinProf = null;
    this.contMaxProf = null;
    this.nomeProduto = null;
    this.percentagemMinima = null;
    this.percentagemMaxima = null;
    this.stringValoresAlt = null;
    this.stringValoresLarg = null;
    this.stringValoresProf = null;
    this.produtoEditar = null;
    this.alturaDimensaoContinuaMin = [];
    this.alturaDimensaoContinuaMax = [];
    this.larguraDimensaoContinuaMin = [];
    this.larguraDimensaoContinuaMax = [];
    this.profundidadeDimensaoContinuaMin = [];
    this.profundidadeDimensaoContinuaMax = [];
    this.catalogosSelecionadosEditar = [];
    this.colecaoProdutoEditar = null;
    this.produtoListar = null;
    this.catalogosSelecionados = [];
    this.colecaoProduto = null;
  }

  estadoCriarProduto() {
    this.produtoHidden01 = !this.produtoHidden01;
    this.produtoHidden02 = false;
    this.produtoHidden03 = false;
    this.produtoHidden04 = false;
    this.produtoHidden05 = false;
    this.alturaContinuaHidden = false;
    this.alturaDiscretaHidden = false;
    this.larguraContinuaHidden = false;
    this.larguraDiscretaHidden = false;
    this.profundidadeContinuaHidden = false;
    this.profundidadeDiscretaHidden = false;
    this.produtoPesquisar = null;
    this.produtoEliminar = null;



    this.alturaContinuaEditHidden = false;
    this.alturaDiscretaEditHidden = false;
    this.larguraContinuaEditHidden = false;
    this.larguraDiscretaEditHidden = false;
    this.profundidadeContinuaEditHidden = false;
    this.profundidadeDiscretaEditHidden = false;
    this.limparDimensoes();
    this.limparDadosCriarEditar()
  }

  estadoListarProdutos() {
    this.produtoHidden01 = false;
    this.produtoHidden02 = !this.produtoHidden02;
    this.produtoHidden03 = false;
    this.produtoHidden04 = false;
    this.produtoHidden05 = false;
    this.alturaContinuaHidden = false;
    this.alturaDiscretaHidden = false;
    this.larguraContinuaHidden = false;
    this.larguraDiscretaHidden = false;
    this.profundidadeContinuaHidden = false;
    this.profundidadeDiscretaHidden = false;
    this.produtoPesquisar = null;
    this.produtoEliminar = null;

    this.alturaContinuaEditHidden = false;
    this.alturaDiscretaEditHidden = false;
    this.larguraContinuaEditHidden = false;
    this.larguraDiscretaEditHidden = false;
    this.profundidadeContinuaEditHidden = false;
    this.profundidadeDiscretaEditHidden = false;
    this.limparDimensoes();
    this.limparDadosCriarEditar()

  }

  estadoProcurarProduto() {
    this.produtoHidden01 = false;
    this.produtoHidden02 = false;
    this.produtoHidden03 = !this.produtoHidden03;
    this.produtoHidden04 = false;
    this.produtoHidden05 = false;
    this.alturaContinuaHidden = false;
    this.alturaDiscretaHidden = false;
    this.larguraContinuaHidden = false;
    this.larguraDiscretaHidden = false;
    this.profundidadeContinuaHidden = false;
    this.profundidadeDiscretaHidden = false;
    this.produtoPesquisar = null;
    this.produtoEliminar = null;

    this.alturaContinuaEditHidden = false;
    this.alturaDiscretaEditHidden = false;
    this.larguraContinuaEditHidden = false;
    this.larguraDiscretaEditHidden = false;
    this.profundidadeContinuaEditHidden = false;
    this.profundidadeDiscretaEditHidden = false;
    this.limparDimensoes();
    this.limparDadosCriarEditar()
  }

  estadoEliminarProduto() {
    this.produtoHidden01 = false;
    this.produtoHidden02 = false;
    this.produtoHidden03 = false;
    this.produtoHidden04 = !this.produtoHidden04;
    this.produtoHidden05 = false;
    this.alturaContinuaHidden = false;
    this.alturaDiscretaHidden = false;
    this.larguraContinuaHidden = false;
    this.larguraDiscretaHidden = false;
    this.profundidadeContinuaHidden = false;
    this.profundidadeDiscretaHidden = false;
    this.produtoPesquisar = null;
    this.produtoEliminar = null;


    this.alturaContinuaEditHidden = false;
    this.alturaDiscretaEditHidden = false;
    this.larguraContinuaEditHidden = false;
    this.larguraDiscretaEditHidden = false;
    this.profundidadeContinuaEditHidden = false;
    this.profundidadeDiscretaEditHidden = false;
    this.limparDimensoes();
    this.limparDadosCriarEditar()
  }

  estadoEditarProduto() {
    this.produtoHidden01 = false;
    this.produtoHidden02 = false;
    this.produtoHidden03 = false;
    this.produtoHidden04 = false;
    this.produtoHidden05 = !this.produtoHidden05;
    this.alturaContinuaHidden = false;
    this.alturaDiscretaHidden = false;
    this.larguraContinuaHidden = false;
    this.larguraDiscretaHidden = false;
    this.profundidadeContinuaHidden = false;
    this.profundidadeDiscretaHidden = false;
    this.produtoPesquisar = null;
    this.produtoEliminar = null;

    this.alturaContinuaEditHidden = false;
    this.alturaContinuaEditHidden = false;
    this.alturaDiscretaEditHidden = false;
    this.larguraContinuaEditHidden = false;
    this.larguraDiscretaEditHidden = false;
    this.profundidadeContinuaEditHidden = false;
    this.profundidadeDiscretaEditHidden = false;
    this.limparDimensoes();
    this.limparDadosCriarEditar()
  }
  estadoDimensaoContinuaAltura() {
    this.alturaContinuaHidden = !this.alturaContinuaHidden;
    this.alturaDiscretaHidden = false;
    this.alturaDimensaoDiscreta = [];
    this.alturaDimensaoContinuaMin = [];
    this.alturaDimensaoContinuaMax = [];
  }

  estadoDimensaoDiscretaAltura() {
    this.alturaContinuaHidden = false;
    this.alturaDiscretaHidden = !this.alturaDiscretaHidden;
    this.alturaDimensaoDiscreta = [];

    this.alturaDimensaoContinuaMin = [];
    this.alturaDimensaoContinuaMax = [];
  }

  estadoDimensaoContinuaLargura() {
    this.larguraContinuaHidden = !this.larguraContinuaHidden;
    this.larguraDiscretaHidden = false;
    this.larguraDimensaoDiscreta = [];

    this.larguraDimensaoContinuaMin = [];
    this.larguraDimensaoContinuaMax = [];
  }

  estadoDimensaoDiscretaLargura() {
    this.larguraContinuaHidden = false;
    this.larguraDiscretaHidden = !this.larguraDiscretaHidden;
    this.larguraDimensaoDiscreta = [];
    this.larguraDimensaoContinuaMin = [];
    this.larguraDimensaoContinuaMax = [];
  }

  estadoDimensaoContinuaProfundidade() {
    this.profundidadeContinuaHidden = !this.profundidadeContinuaHidden;
    this.profundidadeDiscretaHidden = false;
    this.profundidadeDimensaoDiscreta = [];
    this.profundidadeDimensaoContinuaMin = [];
    this.profundidadeDimensaoContinuaMax = [];
  }

  estadoDimensaoDiscretaProfundidade() {
    this.profundidadeContinuaHidden = false;
    this.profundidadeDiscretaHidden = !this.profundidadeDiscretaHidden;
    this.profundidadeDimensaoDiscreta = [];
    this.profundidadeDimensaoContinuaMin = [];
    this.profundidadeDimensaoContinuaMax = [];
  }

  estadoDimensaoContinuaEditAltura() {
    this.alturaContinuaEditHidden = !this.alturaContinuaEditHidden;
    this.alturaDiscretaEditHidden = false;
    this.alturaDimensaoDiscreta = [];
    this.alturaDimensaoContinuaMin = [];
    this.alturaDimensaoContinuaMax = [];
    if (this.produtoEditar != null) {

      this.atualizarAltura(1);
    }
  }

  estadoDimensaoDiscretaEditAltura() {
    this.alturaContinuaEditHidden = false;
    this.alturaDiscretaEditHidden = !this.alturaDiscretaEditHidden;
    this.alturaDimensaoDiscreta = [];
    this.alturaDimensaoContinuaMin = [];
    this.alturaDimensaoContinuaMax = [];
    if (this.produtoEditar != null) {

      this.atualizarAltura(2);
    }
  }

  estadoDimensaoContinuaEditLargura() {
    this.larguraContinuaEditHidden = !this.larguraContinuaEditHidden;
    this.larguraDiscretaEditHidden = false;
    this.larguraDimensaoDiscreta = [];
    this.larguraDimensaoContinuaMin = [];
    this.larguraDimensaoContinuaMax = [];
    if (this.produtoEditar != null) {

      this.atualizarLargura(1);
    }
  }

  estadoDimensaoDiscretaEditLargura() {
    this.larguraContinuaEditHidden = false;
    this.larguraDiscretaEditHidden = !this.larguraDiscretaEditHidden;
    this.larguraDimensaoDiscreta = [];
    this.larguraDimensaoContinuaMin = [];
    this.larguraDimensaoContinuaMax = [];
    if (this.produtoEditar != null) {

      this.atualizarLargura(2);
    }
  }

  estadoDimensaoContinuaEditProfundidade() {
    this.profundidadeContinuaEditHidden = !this.profundidadeContinuaEditHidden;
    this.profundidadeDiscretaEditHidden = false;
    this.profundidadeDimensaoDiscreta = [];
    this.profundidadeDimensaoContinuaMin = [];
    this.profundidadeDimensaoContinuaMax = [];
    if (this.produtoEditar != null) {

      this.atualizarProfundidade(1);
    }
  }

  estadoDimensaoDiscretaEditProfundidade() {
    this.profundidadeContinuaEditHidden = false;
    this.profundidadeDiscretaEditHidden = !this.profundidadeDiscretaEditHidden;
    this.profundidadeDimensaoDiscreta = [];
    this.profundidadeDimensaoContinuaMin = [];
    this.profundidadeDimensaoContinuaMax = [];
    if (this.produtoEditar != null) {
      this.atualizarProfundidade(2);
    }


  }

  limparAltura() {
    this.contMinAlt = null;
    this.contMaxAlt = null;
    this.stringValoresAlt = null;
    this.alturaDimensaoDiscreta = [];
  }

  limparLargura() {
    this.contMinLarg = null;
    this.contMaxLarg = null;
    this.stringValoresLarg = null;
    this.larguraDimensaoDiscreta = [];
  }

  limparProfundidade() {
    this.contMinProf = null;
    this.contMaxProf = null;
    this.stringValoresProf = null;
    this.profundidadeDimensaoDiscreta = [];
  }

  limparDimensoes() {
    this.limparAltura();
    this.limparLargura();
    this.limparProfundidade();
  }
  /*---------------------------------------------- */

  /* Métodos de Criação/Edição de Produtos */
  // tipo: 1- Criar Produto Raiz; 2- Editar Produto Já Existente
  criarProduto(tipo: number) {
    let name = (<HTMLInputElement>document.getElementById("nomeProduto")).value;
    if (!name) {
      alert('Por favor insira um nome válido');
      return;
    }
    console.log(this.catalogosSelecionadosEditar);
    if (this.catalogosSelecionados == null && this.catalogosSelecionadosEditar == null) {
      alert("Selecione no minimo um Catalogo");
      return;
    }
    if (this.categoriaProduto == null) {
      alert("Selecione uma Categoria");
      return;
    }
    if (this.materialProduto.length == 0) {
      alert('Selecione no mínimo um Material');
      return;
    }
    if (this.verificarDimensoes()) {
      let nome;
      nome = { conteudo: name };

      let colecao;
      let produtoCatalogos = [];




      let categoria;
      categoria = { id: this.categoriaProduto.id }
      let idMateriais = [];
      for (let i = 0; i < this.materialProduto.length; i++) {
        idMateriais[i] = { idMaterial: this.materialProduto[i].id }
      }

      let todosFilhos = [];
      if (tipo == 1) {
        for (let i = 0; i < this.listaFilhos.length; i++) {
          todosFilhos[i] = { IdFilho: this.listaFilhos[i].produtoId }
        }
      } else if (tipo == 2) {
        for (let i = 0; i < this.listaFilhos.length; i++) {
          todosFilhos[i] = { IdPai: this.produtoEditar.produtoId, IdFilho: this.listaFilhos[i].produtoId }
        }
      }
      let dimensoes;
      let altura, largura, profundidade;
      //altura

      if (this.alturaDimensaoContinuaMin.length != 0) {
        let cont = [];
        for (let i = 0; i < this.alturaDimensaoContinuaMin.length; i++) {
          cont[i] = { dimensaoMin: this.alturaDimensaoContinuaMin[i], dimensaoMax: this.alturaDimensaoContinuaMax[i] }
        }
        console.log("ALTURA: ");
        console.log(cont);
        altura = cont;
      } else {
        let discreta = [];
        for (let i = 0; i < this.alturaDimensaoDiscreta.length; i++) {
          discreta[i] = { dimensao: this.alturaDimensaoDiscreta[i] }
        }
        altura = discreta;
      }
      //largura
      if (this.larguraDimensaoContinuaMin.length != 0) {
        let cont = [];
        for (let i = 0; i < this.larguraDimensaoContinuaMin.length; i++) {
          cont[i] = { dimensaoMin: this.larguraDimensaoContinuaMin[i], dimensaoMax: this.larguraDimensaoContinuaMax[i] }
        }
        largura = cont;
      } else {
        let discreta = [];
        for (let i = 0; i < this.larguraDimensaoDiscreta.length; i++) {
          discreta[i] = { dimensao: this.larguraDimensaoDiscreta[i] }
        }
        largura = discreta;
      }
      //profundidade
      if (this.profundidadeDimensaoContinuaMin.length != 0) {
        let cont = [];
        for (let i = 0; i < this.profundidadeDimensaoContinuaMin.length; i++) {
          cont[i] = { dimensaoMin: this.profundidadeDimensaoContinuaMin[i], dimensaoMax: this.profundidadeDimensaoContinuaMax[i] }
        }
        profundidade = cont;
      } else {
        let discreta = [];
        for (let i = 0; i < this.profundidadeDimensaoDiscreta.length; i++) {
          discreta[i] = { dimensao: this.profundidadeDimensaoDiscreta[i] }
        }
        profundidade = discreta;
      }

      console.log(this.alturaContinuaEditHidden);
      //altura editar
      // if (this.alturaContinuaEditHidden) {
      //   if (this.contMinAlt != 0) {
      //     altura = { min: this.contMinAlt, max: this.contMaxAlt }
      //   }
      // } else {
      //   let discreta = [];
      //   for (let i = 0; i < this.alturaDimensaoDiscreta.length; i++) {
      //     discreta[i] = { conteudo: this.alturaDimensaoDiscreta[i] }
      //   }
      //   altura = { discreta: discreta };
      //   console.log(altura);
      // }
      // //largura editar
      // if (this.larguraContinuaEditHidden) {
      //   if (this.contMinLarg != 0) {
      //     largura = { min: this.contMinLarg, max: this.contMaxLarg }
      //   }
      // } else {
      //   let discreta = [];
      //   for (let i = 0; i < this.larguraDimensaoDiscreta.length; i++) {
      //     discreta[i] = { conteudo: this.larguraDimensaoDiscreta[i] }
      //   }
      //   largura = { discreta: discreta };
      // }
      // if (this.profundidadeContinuaEditHidden) {
      //   if (this.contMinProf != 0) {
      //     profundidade = { min: this.contMinProf, max: this.contMaxProf }
      //   }
      // } else {
      //   let discreta = [];
      //   for (let i = 0; i < this.profundidadeDimensaoDiscreta.length; i++) {
      //     discreta[i] = { conteudo: this.profundidadeDimensaoDiscreta[i] }
      //   }
      //   profundidade = { discreta: discreta };
      // }

      if (tipo == 1) {
        if (this.colecaoProduto == null) {
          colecao = null;
        } else {
          colecao = {
            id: this.colecaoProduto.id
          }
        }
        for (let i = 0; i < this.catalogosSelecionados.length; i++) {
          produtoCatalogos[i] = { catalogoId: this.catalogosSelecionados[i].catalogoId };
        }
        this.produtoService.adicionarProduto({
          nome,
          colecao,
          categoria,
          materiais: idMateriais,
          produtosAgregados: todosFilhos,
          dimensoes: {
            altura,
            largura,
            profundidade
          },
          produtoCatalogos
        } as Produto).subscribe(produto => { this.buscarProdutos() });
      } else {
        if (this.colecaoProdutoEditar == null) {
          colecao = null;
        } else {
          colecao = {
            id: this.colecaoProdutoEditar.id
          }
        }
        for (let i = 0; i < this.catalogosSelecionadosEditar.length; i++) {
          produtoCatalogos[i] = { catalogoId: this.catalogosSelecionadosEditar[i].catalogoId };
        }
        this.produtoService.atualizarproduto({
          produtoId: this.produtoEditar.produtoId,
          nome,
          colecao,
          categoria,
          materiais: idMateriais,
          produtosAgregados: todosFilhos,
          dimensoes: {
            altura,
            largura,
            profundidade
          },
          produtoCatalogos
        } as Produto).subscribe(res => {
          if (typeof res !== 'undefined') {
            alert('Produto atualizado com sucesso!');
            this.buscarProdutos();
          }





        });
      }
      // nomeProduto => nome;
      // categoria => categoria;
      // listaFilhosObrigatorios;
      // listaFilhosOpcionais;
      // materiais => materialProduto[];
      // percentagens => percentagemMinima e percentagemMaxima;
      // materialAcabamento => restricao
      // altura
      // largura
      // profundidade
    }
  }



  verificarDimensoes() {
    if (this.verificarAltura() && this.verificarLargura() && this.verificarProfundidade()) {
      return true;
    }
    return false;
  }

  verificarAltura() {
    if (this.alturaDimensaoContinuaMin.length == 0 && this.alturaDimensaoDiscreta.length == 0) {
      alert('ALTURA: Valores de Dimensões inválidos');
      return false;
    }
    return true;
  }

  verificarLargura() {
    if (this.larguraDimensaoContinuaMin.length == 0 && this.larguraDimensaoDiscreta.length == 0) {
      alert('LARGURA: Valores de Dimensões inválidos');
      return false;
    }
    return true;
  }

  verificarProfundidade() {
    if (this.profundidadeDimensaoContinuaMin.length == 0 && this.profundidadeDimensaoDiscreta.length == 0) {
      alert('PROFUNDIDADE: Valores de Dimensões inválidos');
      return false;
    }
    return true;
  }

  // tipo: 1- Altura; 2- Largura; 3- Profundidade
  acrescentarValorDiscreta(tipo: number) {
    if (tipo == 1) {
      let value = (<HTMLInputElement>document.getElementById("discretaNovoAltura")).value;
      let valor = Number(value);
      if (!isNaN(valor)) {
        for (let i = 0; i < this.alturaDimensaoDiscreta.length; i++) {
          if (this.alturaDimensaoDiscreta[i] == valor) {
            return;
          }
        }
        this.alturaDimensaoDiscreta.push(valor);
        let string = '';
        for (let i = 0; i < this.alturaDimensaoDiscreta.length; i++) {
          string += this.alturaDimensaoDiscreta[i] + '; ';
        }
        document.getElementById("valoresDiscretaAltura").innerHTML = string;
      } else {
        alert(value + ' is not a number');
      }
    } else if (tipo == 2) {
      let value = (<HTMLInputElement>document.getElementById("discretaNovoLargura")).value;
      let valor = Number(value);
      if (!isNaN(valor)) {
        for (let i = 0; i < this.larguraDimensaoDiscreta.length; i++) {
          if (this.larguraDimensaoDiscreta[i] == valor) {
            return;
          }
        }
        this.larguraDimensaoDiscreta.push(valor);
        let string = '';
        for (let i = 0; i < this.larguraDimensaoDiscreta.length; i++) {
          string += this.larguraDimensaoDiscreta[i] + '; ';
        }
        document.getElementById("valoresDiscretaLargura").innerHTML = string;
      } else {
        alert(value + ' is not a number');
      }
    } else {
      let value = (<HTMLInputElement>document.getElementById("discretaNovoProfundidade")).value;
      let valor = Number(value);
      if (!isNaN(valor)) {
        for (let i = 0; i < this.profundidadeDimensaoDiscreta.length; i++) {
          if (this.profundidadeDimensaoDiscreta[i] == valor) {
            return;
          }
        }
        this.profundidadeDimensaoDiscreta.push(valor);
        let string = '';
        for (let i = 0; i < this.profundidadeDimensaoDiscreta.length; i++) {
          string += this.profundidadeDimensaoDiscreta[i] + '; ';
        }
        document.getElementById("valoresDiscretaProfundidade").innerHTML = string;
      } else {
        alert(value + ' is not a number');
      }
    }
  }



  //tipo: 1- Altura; 2- Largura; 3- Profundidade
  acrescentarValorContinua(tipo: number) {
    if (tipo == 1) {
      let value = (<HTMLInputElement>document.getElementById("continuaMinAltura")).value;
      let valorMin = Number(value);
      if (!isNaN(valorMin)) {
        value = (<HTMLInputElement>document.getElementById("continuaMaxAltura")).value;
        let valorMax = Number(value);
        if (!isNaN(valorMax)) {
          for (let i = 0; i < this.alturaDimensaoContinuaMin.length; i++) {
            if (this.alturaDimensaoContinuaMin[i] == valorMin) {
              return;
            }
          }
          for (let i = 0; i < this.alturaDimensaoContinuaMax.length; i++) {
            if (this.alturaDimensaoContinuaMax[i] == valorMax) {
              return;
            }
          }
          if (valorMin >= valorMax) return;
          this.alturaDimensaoContinuaMin.push(valorMin);
          this.alturaDimensaoContinuaMax.push(valorMax);
          let string = '';
          for (let i = 0; i < this.alturaDimensaoContinuaMin.length; i++) {
            string += "[" + this.alturaDimensaoContinuaMin[i] + " - " + this.alturaDimensaoContinuaMax[i] + "]" + '; ';
          }
          document.getElementById("valoresContinuaAltura").innerHTML = string;
        } else {
          alert(value + ' is not a number');
        }
      } else {
        alert(value + ' is not a number');
      }
    } else if (tipo == 2) {
      let value = (<HTMLInputElement>document.getElementById("continuaMinLargura")).value;
      let valorMin = Number(value);
      if (!isNaN(valorMin)) {
        value = (<HTMLInputElement>document.getElementById("continuaMaxLargura")).value;
        let valorMax = Number(value);
        if (!isNaN(valorMax)) {
          for (let i = 0; i < this.larguraDimensaoContinuaMin.length; i++) {
            if (this.larguraDimensaoContinuaMin[i] == valorMin) {
              return;
            }
          }
          for (let i = 0; i < this.larguraDimensaoContinuaMax.length; i++) {
            if (this.larguraDimensaoContinuaMax[i] == valorMax) {
              return;
            }
          }
          if (valorMin >= valorMax) return;
          this.larguraDimensaoContinuaMin.push(valorMin);
          this.larguraDimensaoContinuaMax.push(valorMax);
          let string = '';
          for (let i = 0; i < this.larguraDimensaoContinuaMin.length; i++) {
            string += "[" + this.larguraDimensaoContinuaMin[i] + " - " + this.larguraDimensaoContinuaMax[i] + "]" + '; ';
          }
          document.getElementById("valoresContinuaLargura").innerHTML = string;
        } else {
          alert(value + ' is not a number');
        }
      } else {
        alert(value + ' is not a number');
      }
    } else {
      let value = (<HTMLInputElement>document.getElementById("continuaMinProfundidade")).value;
      let valorMin = Number(value);
      if (!isNaN(valorMin)) {
        value = (<HTMLInputElement>document.getElementById("continuaMaxProfundidade")).value;
        let valorMax = Number(value);
        if (!isNaN(valorMax)) {
          for (let i = 0; i < this.profundidadeDimensaoContinuaMin.length; i++) {
            if (this.profundidadeDimensaoContinuaMin[i] == valorMin) {
              return;
            }
          }
          for (let i = 0; i < this.profundidadeDimensaoContinuaMax.length; i++) {
            if (this.profundidadeDimensaoContinuaMax[i] == valorMax) {
              return;
            }
          }
          if (valorMin >= valorMax) return;
          this.profundidadeDimensaoContinuaMin.push(valorMin);
          this.profundidadeDimensaoContinuaMax.push(valorMax);
          let string = '';
          for (let i = 0; i < this.profundidadeDimensaoContinuaMin.length; i++) {
            string += "[" + this.profundidadeDimensaoContinuaMin[i] + " - " + this.profundidadeDimensaoContinuaMax[i] + "]" + '; ';
          }
          document.getElementById("valoresContinuaProfundidade").innerHTML = string;
        } else {
          alert(value + ' is not a number');
        }
      } else {
        alert(value + ' is not a number');
      }
    }
  }
  /* ------------------------------------------------------ */
  /* Metodos de Eliminar Produto */
  eliminar() {
    if (this.produtoEliminar == null) {
      alert('Por favor selecione o produto que deseja eliminar.');
      return;
    }
    this.produtoService.eliminarProduto(this.produtoEliminar.produtoId).subscribe(
      _ => {
        this.buscarProdutos();
        this.produtoEliminar = null;
      }
    );

  }
  /* ------------------------------------------------------ */
  //FALTA ATUALIZAR
  /* Metodos de Procurar Produto */
  listarDados() {
    if (this.produtoPesquisar != null) {
      let string = '';
      document.getElementById("naoEncontrado").innerHTML = string;
      this.nomePesquisado = "Nome: " + this.produtoPesquisar.nome.conteudo;
      document.getElementById("nomePesquisado").innerHTML = this.nomePesquisado;
      let categoria: Categoria = null;
      for (let i = 0; i < this.listaCategorias.length; i++) {
        if (this.listaCategorias[i].id == this.produtoPesquisar.categoria.id) {
          categoria = this.listaCategorias[i];
          break;
        }
      }
      this.categoriaPesquisado = "Categoria: " + categoria.nome.conteudo;
      document.getElementById("categoriaPesquisado").innerHTML = this.categoriaPesquisado;
      let materiais: Material[] = [];
      for (let i = 0; i < this.produtoPesquisar.materiais.length; i++) {
        for (let j = 0; j < this.listaMateriais.length; j++) {
          if (this.produtoPesquisar.materiais[i].idMaterial == this.listaMateriais[j].id) {
            materiais.push(this.listaMateriais[j]);
          }
        }
      }
      let aux: string = '';
      for (let i = 0; i < materiais.length; i++) {
        if (i == materiais.length - 1) {
          aux += materiais[i].nome.conteudo;
        } else {
          aux += materiais[i].nome.conteudo + ", ";
        }
      }
      this.materiaisPesquisado = "Materiais: " + aux;
      document.getElementById("materiaisPesquisado").innerHTML = this.materiaisPesquisado;
      let obr: number[] = [];
      let op: number[] = [];
    }

  }


  listarProduto() {
    this.alturasProduto = []
    this.alturasMinProduto = [];
    this.alturasMaxProduto = [];
    this.largurasProduto = [];
    this.largurasMinProduto = [];
    this.largurasMaxProduto = [];
    this.profundidadesProduto = [];
    this.profundidadesMinProduto = [];
    this.profundidadesMaxProduto = [];
    this.produtosAgregados = [];
    this.produtoCatalogos = [];
    this.nomeTexturaListar = [];
    this.imagemTexturaListar = [];

    if (this.produtoListar != null) {
      this.materialService.buscarMateriaisDeProduto(this.produtoListar.produtoId).subscribe(materiaisP => {
        this.materiaisProduto = materiaisP;
        for (let i = 0; i < materiaisP.length; i++) {
          this.nomeTexturaListar.push(this.texturasListar[materiaisP[i].texturaImagem].nome);
          this.imagemTexturaListar.push(this.texturasListar[materiaisP[i].texturaImagem].urlImagem);
        }
      });
      this.produtoService.buscarProdutoPorId(this.produtoListar.produtoId).subscribe(produto => {
        for (let i = 0; i < produto.dimensoes.altura.length; i++) {
          if (produto.dimensoes.altura[i].dimensaoMin != null) {
            this.alturasMinProduto.push(produto.dimensoes.altura[i].dimensaoMin);
          }
          if (produto.dimensoes.altura[i].dimensaoMax != null) {
            this.alturasMaxProduto.push(produto.dimensoes.altura[i].dimensaoMax);
          }
          if (produto.dimensoes.altura[i].dimensao != null) {
            this.alturasProduto.push(produto.dimensoes.altura[i].dimensao);
          }
        }
        for (let i = 0; i < produto.dimensoes.largura.length; i++) {
          if (produto.dimensoes.largura[i].dimensaoMin != null) {
            this.largurasMinProduto.push(produto.dimensoes.largura[i].dimensaoMin);
          }
          if (produto.dimensoes.largura[i].dimensaoMax != null) {
            this.largurasMaxProduto.push(produto.dimensoes.largura[i].dimensaoMax);
          }
          if (produto.dimensoes.largura[i].dimensao != null) {
            this.largurasProduto.push(produto.dimensoes.largura[i].dimensao);
          }
        }
        for (let i = 0; i < produto.dimensoes.profundidade.length; i++) {
          if (produto.dimensoes.profundidade[i].dimensaoMin != null) {
            this.profundidadesMinProduto.push(produto.dimensoes.profundidade[i].dimensaoMin);
          }
          if (produto.dimensoes.profundidade[i].dimensaoMax != null) {
            this.profundidadesMaxProduto.push(produto.dimensoes.profundidade[i].dimensaoMax);
          }
          if (produto.dimensoes.profundidade[i].dimensao != null) {
            this.profundidadesProduto.push(produto.dimensoes.profundidade[i].dimensao);
          }
        }
        for (let i = 0; i < produto.produtosAgregados.length; i++) {
          this.produtoService.buscarProdutoPorId(produto.produtosAgregados[i].idFilho).subscribe(filho => {
            this.produtosAgregados.push(filho.nome.conteudo);
          })
        }
        for (let i = 0; i < produto.produtoCatalogos.length; i++) {
          this.catalogoService.buscarCatalogoPorId(produto.produtoCatalogos[i].catalogoId).subscribe(catalogo => {
            this.produtoCatalogos.push(catalogo.nome.conteudo);
          })
        }
      })
    }
  }


  /* ------------------------------------------------------ */

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  equalsCategoria(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.catalogoId === objTwo.catalogoId;
    }
  }

  equalsProduto(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.produtoId === objTwo.produtoId;
    }
  }

}

