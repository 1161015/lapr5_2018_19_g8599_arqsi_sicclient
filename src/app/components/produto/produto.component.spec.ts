import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProdutoComponent } from './produto.component';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule, MatTabsModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatCardModule } from '@angular/material/card';

describe('ProdutoComponent', () => {
  let component: ProdutoComponent;
  let fixture: ComponentFixture<ProdutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProdutoComponent],
      imports: [MatSelectModule, MatInputModule, FormsModule, RouterTestingModule,
        HttpClientTestingModule, MatTabsModule, MatCardModule, MatTooltipModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('#estadoCriarProduto() should toggle #produtoHidden01', () => {
    expect(component.produtoHidden01).toBe(false, 'off at first');
    component.estadoCriarProduto();
    expect(component.produtoHidden01).toBe(true, 'on after pressed');
    component.estadoCriarProduto();
    expect(component.produtoHidden01).toBe(false, 'off after pressed again');
  });

  it('#estadoListarProdutos() should toggle #produtoHidden02', () => {
    expect(component.produtoHidden02).toBe(false, 'off at first');
    component.estadoListarProdutos();
    expect(component.produtoHidden02).toBe(true, 'on after pressed');
    component.estadoListarProdutos();
    expect(component.produtoHidden02).toBe(false, 'off after pressed again');
  });

  it('#estadoProcurarProduto() should toggle #produtoHidden03', () => {
    expect(component.produtoHidden03).toBe(false, 'off at first');
    component.estadoProcurarProduto();
    expect(component.produtoHidden03).toBe(true, 'on after pressed');
    component.estadoProcurarProduto();
    expect(component.produtoHidden03).toBe(false, 'off after pressed again');
  });

  it('#estadoEliminarProduto() should toggle #produtoHidden04', () => {
    expect(component.produtoHidden04).toBe(false, 'off at first');
    component.estadoEliminarProduto();
    expect(component.produtoHidden04).toBe(true, 'on after pressed');
    component.estadoEliminarProduto();
    expect(component.produtoHidden04).toBe(false, 'off after pressed again');
  });

  it('#estadoEditarProduto() should toggle #produtoHidden05', () => {
    expect(component.produtoHidden05).toBe(false, 'off at first');
    component.estadoEditarProduto();
    expect(component.produtoHidden05).toBe(true, 'on after pressed');
    component.estadoEditarProduto();
    expect(component.produtoHidden05).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoContinuaAltura() should toggle #alturaContinuaHidden', () => {
    expect(component.alturaContinuaHidden).toBe(false, 'off at first');
    component.estadoDimensaoContinuaAltura();
    expect(component.alturaContinuaHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoContinuaAltura();
    expect(component.alturaContinuaHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoDiscretaAltura() should toggle #alturaDiscretaHidden', () => {
    expect(component.alturaDiscretaHidden).toBe(false, 'off at first');
    component.estadoDimensaoDiscretaAltura();
    expect(component.alturaDiscretaHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoDiscretaAltura();
    expect(component.alturaDiscretaHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoContinuaLargura() should toggle #larguraContinuaHidden', () => {
    expect(component.larguraContinuaHidden).toBe(false, 'off at first');
    component.estadoDimensaoContinuaLargura();
    expect(component.larguraContinuaHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoContinuaLargura();
    expect(component.larguraContinuaHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoDiscretaLargura() should toggle #larguraDiscretaHidden', () => {
    expect(component.larguraDiscretaHidden).toBe(false, 'off at first');
    component.estadoDimensaoDiscretaLargura();
    expect(component.larguraDiscretaHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoDiscretaLargura();
    expect(component.larguraDiscretaHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoContinuaProfundidade() should toggle #profundidadeContinuaHidden', () => {
    expect(component.profundidadeContinuaHidden).toBe(false, 'off at first');
    component.estadoDimensaoContinuaProfundidade();
    expect(component.profundidadeContinuaHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoContinuaProfundidade();
    expect(component.profundidadeContinuaHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoDiscretaProfundidade() should toggle #profundidadeDiscretaHidden', () => {
    expect(component.profundidadeDiscretaHidden).toBe(false, 'off at first');
    component.estadoDimensaoDiscretaProfundidade();
    expect(component.profundidadeDiscretaHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoDiscretaProfundidade();
    expect(component.profundidadeDiscretaHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoContinuaEditAltura() should toggle #alturaContinuaEditHidden', () => {
    expect(component.alturaContinuaEditHidden).toBe(false, 'off at first');
    component.estadoDimensaoContinuaEditAltura();
    expect(component.alturaContinuaEditHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoContinuaEditAltura();
    expect(component.alturaContinuaEditHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoDiscretaEditAltura() should toggle #alturaDiscretaEditHidden', () => {
    expect(component.alturaDiscretaEditHidden).toBe(false, 'off at first');
    component.estadoDimensaoDiscretaEditAltura();
    expect(component.alturaDiscretaEditHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoDiscretaEditAltura();
    expect(component.alturaDiscretaEditHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoContinuaEditLargura() should toggle #larguraContinuaEditHidden', () => {
    expect(component.larguraContinuaEditHidden).toBe(false, 'off at first');
    component.estadoDimensaoContinuaEditLargura();
    expect(component.larguraContinuaEditHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoContinuaEditLargura();
    expect(component.larguraContinuaEditHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoDiscretaEditLargura() should toggle #larguraDiscretaEditHidden', () => {
    expect(component.larguraDiscretaEditHidden).toBe(false, 'off at first');
    component.estadoDimensaoDiscretaEditLargura();
    expect(component.larguraDiscretaEditHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoDiscretaEditLargura();
    expect(component.larguraDiscretaEditHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoContinuaEditProfundidade() should toggle #profundidadeContinuaEditHidden', () => {
    expect(component.profundidadeContinuaEditHidden).toBe(false, 'off at first');
    component.estadoDimensaoContinuaEditProfundidade();
    expect(component.profundidadeContinuaEditHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoContinuaEditProfundidade();
    expect(component.profundidadeContinuaEditHidden).toBe(false, 'off after pressed again');
  });

  it('#estadoDimensaoDiscretaEditProfundidade() should toggle #profundidadeDiscretaEditHidden', () => {
    expect(component.profundidadeDiscretaEditHidden).toBe(false, 'off at first');
    component.estadoDimensaoDiscretaEditProfundidade();
    expect(component.profundidadeDiscretaEditHidden).toBe(true, 'on after pressed');
    component.estadoDimensaoDiscretaEditProfundidade();
    expect(component.profundidadeDiscretaEditHidden).toBe(false, 'off after pressed again');
  });

});
