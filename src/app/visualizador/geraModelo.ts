import * as THREE from 'three';
import * as TWEEN from "tween.js"
import { Item } from '../models/item';
import { Categoria } from '../models/categoria';
import { CategoriaService } from 'src/app/services/categoria.service';
import { Produto } from '../models/produto';
import { Acabamento } from '../models/acabamento';
import { Material } from '../models/material';
import { HistoricoPrecoService } from 'src/app/services/historico-preco.service';
import { ProdutoService } from 'src/app/services/produto.service';

import { Injectable, Injector } from '@angular/core';
import { VisualizadorService } from '../services/visualizador.service';

@Injectable({
  providedIn: 'root'
})

export class GeraModelo {
  private visualizadorService: VisualizadorService;
  listaTodosMateriais = [];
  listaTodosMeshes = [];
  listaTodosItens = [];

  public static precoTotal : number;


  vlistaTodosMeshesAUsar = [];

  constructor(private categoriaService: CategoriaService, private produtoService: ProdutoService, private historicoPrecoService : HistoricoPrecoService, injector: Injector) {
    setTimeout(() => this.visualizadorService = injector.get(VisualizadorService));
  }

  ALTURA_GAVETAS: number;
  ALTURA_PRATELEIRAS: number;

  texturaMaterial: any;

  desenharItem(item: Item, scene): void {
    
    item.resetPreco();
        // Categoria do Item
        this.categoriaService.buscarCategoriaPorId(item.produto.categoria.id).subscribe(
          categoria => {
            item.categoria = categoria;
            if (!this.determinarCategoria(item, scene, item.urlMaterial)){
              alert("Categoria do produto selecionado não se encontra modelada.");
              //this.removerTodasMeshes(item, scene);
              return; // se um item não pode ser desenhado, os filhos também não
            }
            GeraModelo.precoTotal += item.precoItem;
            let i;  // chamada recursiva aos filhos
            if (typeof item.itensFilhos !== 'undefined')
              for (i = 0; i < item.itensFilhos.length; i++) {
                this.desenharItem(item.itensFilhos[i] as Item, scene);
              }

          }

    );
  }

  // todas as categorias que JÁ tenham um modelo definido devem ser acrescentadas aqui
  // caso não encontre um modelo, retorna false, não desenhando o item
  determinarCategoria(item: Item, scene, urlImagem: string): boolean {
    if (item.categoria == null || typeof item.categoria == 'undefined') return false; // traduz-se na categoria que origina esta chamada ser já a raiz da respetiva árvore de categorias
    switch (item.categoria.nome.conteudo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "")) { // Substitui acentos pela letra normal, sem acento
      case "armario":
        item.tipo = 0;
        return this.criarArmario(item, scene, urlImagem);
      case "modulo gavetas":
      item.tipo = 1;
        return this.criarModuloGavetas(item, scene, urlImagem);
      case "modulo prateleiras":
      item.tipo = 2;
        return this.criarModuloPrateleiras(item, scene, urlImagem);
      case "modulo cabides":
      item.tipo = 3;
        return this.criarModuloCabides(item, scene, urlImagem);
      default:
        this.categoriaService.buscarCategoriaPorId(item.categoria.categoriaPai.id).subscribe(
          categoriaPai => {
            item.categoria = categoriaPai;
            return this.determinarCategoria(item, scene, urlImagem);
          }
        );
    }
    return true;
  }

  // Estruturação de itens

  criarArmario(item: Item, scene, urlImagem: string): boolean {
    this.configurarTextura(item, urlImagem);

    this.criarEstrutura(item, scene, this.texturaMaterial);
    this.criarParedeFundo(item, scene, this.texturaMaterial);
    this.criarPortas(item, scene, this.texturaMaterial);
    this.listaTodosItens.push(item);
    return true;
  }

  criarModuloGavetas(item: Item, scene, urlImagem: string): boolean {
    this.configurarTextura(item, urlImagem);

    this.criarEstrutura(item, scene, this.texturaMaterial);
    this.criarParedeFundo(item, scene, this.texturaMaterial);

    let numeroEspacos = 0;
    do {
      numeroEspacos++;
      this.ALTURA_GAVETAS = item.altura / numeroEspacos;
      if (this.ALTURA_GAVETAS < 15) return false; // Não é possível criar Items com gavetas menores que 15 de altura
    } while (this.ALTURA_GAVETAS > 25 || this.ALTURA_GAVETAS < 15); // determina o numero de gavetas do modulo de maneira a que a altura de cada gaveta tenha entre 15 e 25

    // começa a colocar gavetas de baixo para cima
    let i;
    for (i = 0; i < numeroEspacos; i++) {
      this.criarGaveta(item, (-item.altura / 2 + i * this.ALTURA_GAVETAS) + item.posicaoY + this.ALTURA_GAVETAS / 2, scene, this.texturaMaterial);
    }
    this.listaTodosItens.push(item);
    return true;
  }

  criarModuloPrateleiras(item: Item, scene, urlImagem: string): boolean {
    this.configurarTextura(item, urlImagem);

    this.criarEstrutura(item, scene, this.texturaMaterial);

    let numeroEspacos = 0;
    do {
      numeroEspacos++;
      this.ALTURA_PRATELEIRAS = item.altura / numeroEspacos;
      if (this.ALTURA_PRATELEIRAS < 15) return false; // Não é possível criar Items com gavetas menores que 15 de altura
    } while (this.ALTURA_PRATELEIRAS > 25 || this.ALTURA_PRATELEIRAS < 15); // determina o numero de gavetas do modulo de maneira a que a altura de cada gaveta tenha entre 15 e 25

    // começa a colocar prateleiras de baixo para cima
    let i;
    for (i = 1; i < numeroEspacos; i++) {
      this.criarPrateleira(item, (-item.altura / 2 + i * this.ALTURA_PRATELEIRAS) + item.posicaoY, scene, this.texturaMaterial);
    }
    this.listaTodosItens.push(item);
    return true;
  }

  criarModuloCabides(item: Item, scene, urlImagem: string): boolean {
    this.configurarTextura(item, urlImagem);

    this.criarEstrutura(item, scene, this.texturaMaterial);
    this.criarPenduraCruzetas(item, scene);

    this.listaTodosItens.push(item);
    return true;
  }

  // Modelagem de partes

  // Cria uma estrutura que servirá de frame ao item
  criarEstrutura(item: Item, scene, textura): void {
    // Paredes laterais, chão e teto
    let geomHori = this.criarParalelipipedo(item, item.largura - 2 * item.espessura(), item.espessura(), item.profundidade);
    let geomVert = this.criarParalelipipedo(item, item.espessura(), item.altura, item.profundidade);

    // Teto
    let meshTeto = new THREE.Mesh(geomHori, textura);
    meshTeto.position.set(item.posicaoX, item.altura / 2 + item.posicaoY - item.espessura() / 2, item.posicaoZ);
    this.adicionar(item, scene, meshTeto);

    // Chão
    let meshChao = new THREE.Mesh(geomHori, textura);
    meshChao.position.set(item.posicaoX, - item.altura / 2 + item.posicaoY + item.espessura() / 2, item.posicaoZ);
    this.adicionar(item, scene, meshChao);

    // Paredes
    let meshDir = new THREE.Mesh(geomVert, textura);
    meshDir.position.set(item.largura / 2 + item.posicaoX - item.espessura() / 2, item.posicaoY, item.posicaoZ);
    this.adicionar(item, scene, meshDir);

    let meshEsq = new THREE.Mesh(geomVert, textura);
    meshEsq.position.set(-item.largura / 2 + item.posicaoX + item.espessura() / 2, item.posicaoY, item.posicaoZ);
    this.adicionar(item, scene, meshEsq);
  }

  // Cria uma parede de fundo para ser usada na estrutura principal
  criarParedeFundo(item: Item, scene, textura): void {
    // Parede do fundo
    let geomFundo = this.criarParalelipipedo(item, item.largura - 2 * item.espessura(), item.altura - 2 * item.espessura(), item.espessura());
    let meshFundo = new THREE.Mesh(geomFundo, textura);
    meshFundo.position.set(item.posicaoX, item.posicaoY, -item.profundidade / 2 + item.espessura() / 2 + item.posicaoZ);
    this.adicionar(item, scene, meshFundo);
  }

  // Cria portas
  criarPortas(item: Item, scene, textura): void {
    let materialInox = new THREE.MeshPhongMaterial({
      color: 0xffffff,
      specular: 0xffffff,
      shininess: 30,
      map: new THREE.TextureLoader().load('assets/texturas/metal.jpg')
    });

    let geomPorta = this.criarParalelipipedo(item, item.largura / 2, item.altura, item.espessura()); // para haver uma pequena abertura
    let meshPorta1 = new THREE.Mesh(geomPorta, textura);
    let meshPorta2 = new THREE.Mesh(geomPorta, textura);

    meshPorta1.position.set(-item.largura / 4, 0, 0);
    meshPorta2.position.set(item.largura / 4, 0, 0);

    this.listaTodosMeshes.push(meshPorta1);
    this.listaTodosMeshes.push(meshPorta2);

    var pivot1 = new THREE.Object3D();
    pivot1.position.set(item.largura / 2 + item.posicaoX + 0.125, 0 + item.posicaoY, item.profundidade / 2 + item.posicaoZ + item.espessura() / 2);
    //pivot1.rotation.y = Math.PI / 2;

    var pivot2 = new THREE.Object3D();
    pivot2.position.set(-item.largura / 2 + item.posicaoX - 0.125, 0 + item.posicaoY, item.profundidade / 2 + item.posicaoZ + item.espessura() / 2);
    //criarPuxadores(item, scene);

    let geomPuxador1 = this.criarParalelipipedo(item, item.espessura(), item.altura / 2, item.espessura());
    let geomPuxador2 = this.criarParalelipipedo(item, item.espessura(), item.espessura(), item.espessura());

    let offset_puxadores = item.largura / 10 + item.espessura() * 2;

    let puxador11 = new THREE.Mesh(geomPuxador1, materialInox);
    let puxador12 = new THREE.Mesh(geomPuxador2, materialInox);
    let puxador13 = new THREE.Mesh(geomPuxador2, materialInox);
    puxador11.position.set(offset_puxadores - item.largura / 2, 0, item.espessura() * 2);
    puxador12.position.set(offset_puxadores - item.largura / 2, item.altura / 4 - item.espessura() / 2, item.espessura());
    puxador13.position.set(offset_puxadores - item.largura / 2, -item.altura / 4 + item.espessura() / 2, item.espessura());

    let puxador21 = new THREE.Mesh(geomPuxador1, materialInox);
    let puxador22 = new THREE.Mesh(geomPuxador2, materialInox);
    let puxador23 = new THREE.Mesh(geomPuxador2, materialInox);
    puxador21.position.set(-offset_puxadores + item.largura / 2, 0, item.espessura() * 2);
    puxador22.position.set(-offset_puxadores + item.largura / 2, item.altura / 4 - item.espessura() / 2, item.espessura());
    puxador23.position.set(-offset_puxadores + item.largura / 2, -item.altura / 4 + item.espessura() / 2, item.espessura());

    pivot1.add(meshPorta1);
    meshPorta1.name = 'portaF';
    pivot1.add(puxador11);
    pivot1.add(puxador12);
    pivot1.add(puxador13);
    pivot2.add(meshPorta2);
    meshPorta2.name = 'portaF';
    pivot2.add(puxador21);
    pivot2.add(puxador22);
    pivot2.add(puxador23);

    this.adicionar(item, scene, pivot1);
    this.adicionar(item, scene, pivot2);
  }

  criarPrateleira(item: Item, altura: number, scene, textura): void {
    let geomPrat = this.criarParalelipipedo(item, item.largura - item.espessura(), item.espessura(), item.profundidade - item.espessura()); // de modo a encaixar na caixa do módulo de prateleiras
    let meshPrat = new THREE.Mesh(geomPrat, textura);
    meshPrat.position.set(item.posicaoX, altura, item.posicaoZ); // esta altura é a altura de posição da prateleira e não do item em si, aka. "a que altura a que a prateleira está no módulo de prateleiras"
    this.adicionar(item, scene, meshPrat);
  }

  criarGaveta(item: Item, altura: number, scene, textura): void {
    let geomFace = this.criarParalelipipedo(item, item.largura, this.ALTURA_GAVETAS - item.espessura() / 2, item.espessura()); // a altura tem que ser a altura das gavetas - espessura
    let meshFace = new THREE.Mesh(geomFace, textura);
    meshFace.position.set(item.posicaoX, altura - item.espessura() / 2, item.profundidade / 2 + item.espessura() / 2 + item.posicaoZ); // esta altura é a altura de posição da gaveta e não do item em si, aka. "a que altura a gaveta está no módulo de gavetas"
    meshFace.name = 'gavetaF'; // não alterar, deverá mexer com a deteção dos cliques
    this.adicionar(item, scene, meshFace);

    let geomBase = this.criarParalelipipedo(item, item.largura - item.espessura() * 2, item.espessura(), item.profundidade - item.espessura());
    let meshBase = new THREE.Mesh(geomBase, textura);
    meshBase.position.set(item.posicaoX, altura - item.espessura() * 4, item.espessura() / 2 + item.posicaoZ);
    this.adicionar(item, scene, meshBase);

    let geomLado = this.criarParalelipipedo(item, item.espessura(), this.ALTURA_GAVETAS - item.espessura() * 3, item.profundidade - item.espessura());
    let meshEsq = new THREE.Mesh(geomLado, textura);
    let meshDir = new THREE.Mesh(geomLado, textura);
    meshEsq.position.set(item.posicaoX - item.largura / 2 + item.espessura() * 1.5, altura + item.espessura() / 2, item.espessura() / 2 + item.posicaoZ);
    meshDir.position.set(item.posicaoX + item.largura / 2 - item.espessura() * 1.5, altura + item.espessura() / 2, item.espessura() / 2 + item.posicaoZ);
    this.adicionar(item, scene, meshEsq);
    this.adicionar(item, scene, meshDir);

    let geomFundo = this.criarParalelipipedo(item, item.largura - item.espessura() * 4, this.ALTURA_GAVETAS - item.espessura() * 3, item.espessura());
    let meshFundo = new THREE.Mesh(geomFundo, textura);
    meshFundo.position.set(item.posicaoX, altura + item.espessura() / 2, - item.profundidade / 2 + item.espessura() * 1.5 + item.posicaoZ);
    this.adicionar(item, scene, meshFundo);

    this.criarMacaneta(item, altura, scene);
  }

  criarMacaneta(item: Item, altura: number, scene): void {
    let textura = new THREE.MeshPhongMaterial({
      color: 0xffffff,
      specular: 0xffffff,
      shininess: 30,
      map: new THREE.TextureLoader().load('assets/texturas/metal.jpg')
    });
    let geomMaca = this.criarParalelipipedo(item, item.espessura()*5, item.espessura(), item.espessura()*2);
    let meshMaca = new THREE.Mesh(geomMaca, textura);
    meshMaca.position.set(item.posicaoX, altura, item.profundidade / 2 + item.espessura() + item.posicaoZ);
    this.adicionar(item, scene, meshMaca);
  }

  criarPenduraCruzetas(item : Item, scene) : void {
    let textura = new THREE.MeshPhongMaterial({
      color: 0xffffff,
      specular: 0xffffff,
      shininess: 30,
      map: new THREE.TextureLoader().load('assets/texturas/metal.jpg')
    });
    let geomCruz = new THREE.CylinderGeometry(1, 1, item.largura - item.espessura()*2, 50);
    let meshCruz = new THREE.Mesh(geomCruz, textura);
    meshCruz.position.set(item.posicaoX, item.posicaoY + item.altura/2 - 15, item.posicaoZ);
    meshCruz.rotateZ(90 * Math.PI / 180);
    this.adicionar(item, scene, meshCruz);
  }

  // Outras funções

  adicionar(item: Item, scene, mesh): void {
    this.listaTodosMeshes.push(mesh);
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    scene.add(mesh);
    item.adicionarMesh(mesh);
  }

  configurarTextura(item: Item, urlImagem: string) {

    let valorOpacidade = item.acabamento.opacidade / 100;
    if (valorOpacidade < 0.3) {
      valorOpacidade = 0.3;
    }
    if (item.acabamento.fosco == true) {
      this.texturaMaterial = new THREE.MeshLambertMaterial({
        color: item.acabamento.cor,
        transparent: true,
        emissive: item.acabamento.cor,
        opacity: valorOpacidade,
        emissiveIntensity: item.acabamento.emissividade / 100,
        reflectivity: item.acabamento.brilho / 100,
        map: new THREE.TextureLoader().load(urlImagem)
      });
    } else {
      this.texturaMaterial = new THREE.MeshPhongMaterial({
        color: item.acabamento.cor,
        transparent: true,
        emissive: 0xFFFFFF,
        opacity: valorOpacidade,
        emissiveIntensity: item.acabamento.emissividade / 100,
        shininess: item.acabamento.brilho,
        map: new THREE.TextureLoader().load(urlImagem)
      });
    }
  }

  criarParalelipipedo(item : Item, largura : number, altura : number, profundidade : number) : any {
    let area = largura * altura * 2 + largura * profundidade * altura * 2 + altura * profundidade * 2;
    this.aplicarPrecoAcabamento(item, area);
    let volume = largura * altura * profundidade;
    this.aplicarPrecoMaterial(item, volume);
    return new THREE.BoxGeometry(largura, altura, profundidade);
  }

  aplicarPrecoAcabamento(item : Item, area : number) : void {
    if(typeof item.precoAcabamento == undefined) return;
    item.incrementarPreco(Math.floor(item.precoAcabamento.preco.valor * area / 10000));
  }

  aplicarPrecoMaterial(item : Item, volume : number) : void {
    if(typeof item.precoMaterial == undefined) return;
    item.incrementarPreco(Math.floor(item.precoMaterial.preco.valor * volume / 1000000));
  }

  abrirPortas(item, index, onCreate) {
    if(item.tipo == 0)
    this.abrirPorta(item, index, onCreate);
    if(item.tipo == 1)
    this.abrirGavetas(item, 1, false);
    let i;
    for (i = 0; i < item.itensFilhos.length; i++) {
      this.abrirPortas(item.itensFilhos[i], index - 1, onCreate);
    }
  }


  abrirPorta(item, index, onCreate) {
    let x = 1000, y = 1200;
    if (onCreate) {
      x = 0;
      y = 0;
    }

    let tween1 = new TWEEN.Tween(item.meshs[item.meshs.length - 1].rotation);
    tween1.easing(TWEEN.Easing.Circular.In);
    tween1.to({ y: (-Math.PI / 2) - 0.2 * index }, x); // 1000 milisegundos
    tween1.start();

    let tween2 = new TWEEN.Tween(item.meshs[item.meshs.length - 2].rotation);
    tween2.easing(TWEEN.Easing.Circular.In);
    tween2.to({ y: (Math.PI / 2) + 0.2 * index }, y); // 1200 milisegundos
    tween2.start();

    this.animate();
  }


  fecharPortas(item) {
    if(item.tipo == 0)
    this.fecharPorta(item);
    if(item.tipo == 1)
    this.fecharGavetas(item);
    let i;
    if(typeof item.itensFilhos !== 'undefined')
    for (i = 0; i < item.itensFilhos.length; i++) {
      this.fecharPortas(item.itensFilhos[i]);
    }
  }

 abrirGavetas(item, index, onCreate) {
   console.log(item);
    let tween1, tween2, tween3, tween4, tween5, tween6;
    let time = 1000;
    if (onCreate) time = 0;
    let i = 5, count = 0; // n mexer
    while (item.meshs[i] != null) {
      tween1 = new TWEEN.Tween(item.meshs[i].position);
      tween1.easing(TWEEN.Easing.Circular.In);
      tween1.to({ z: item.profundidade / ((index * count) + item.espessura()*3) + item.profundidade / 2 + item.espessura()/2 + item.posicaoZ }, time);
      tween1.start();
  
      tween2 = new TWEEN.Tween(item.meshs[i + 1].position);
      tween2.easing(TWEEN.Easing.Circular.In);
      tween2.to({ z: item.profundidade / ((index * count) + item.espessura()*3) + item.espessura()/2 + item.posicaoZ }, time);
      tween2.start();
  
      tween3 = new TWEEN.Tween(item.meshs[i + 2].position);
      tween3.easing(TWEEN.Easing.Circular.In);
      tween3.to({ z: item.profundidade / ((index * count) + item.espessura()*3) + item.espessura()/2 + item.posicaoZ }, time);
      tween3.start();
  
      tween4 = new TWEEN.Tween(item.meshs[i + 3].position);
      tween4.easing(TWEEN.Easing.Circular.In);
      tween4.to({ z: item.profundidade / ((index * count) + item.espessura()*3) + item.espessura()/2 + item.posicaoZ }, time);
      tween4.start();
  
      tween5 = new TWEEN.Tween(item.meshs[i + 4].position);
      tween5.easing(TWEEN.Easing.Circular.In);
      tween5.to({z : item.profundidade / ((index * count)+item.espessura()*3) - item.profundidade / 2 + item.espessura()*1.5 + item.posicaoZ}, time);
      tween5.start();
  
      tween6 = new TWEEN.Tween(item.meshs[i + 5].position);
      tween6.easing(TWEEN.Easing.Circular.In);
      tween6.to({z : item.profundidade / ((index * count)+item.espessura()*3) + item.profundidade / 2 + item.espessura() + item.posicaoZ}, time);
      tween6.start();
  
      this.animate();
      i += 6;
      count+=3;
    }
  }

  fecharGavetas(item) {
    let tween1, tween2, tween3, tween4, tween5, tween6;
    let i = 5; // n mexer
    let time = 500;
    while (item.meshs[i] != null) {
      tween1 = new TWEEN.Tween(item.meshs[i].position);
      tween1.easing(TWEEN.Easing.Circular.In);
      tween1.to({ z: item.profundidade / 2 + item.espessura()/2 + item.posicaoZ }, time);
      tween1.start();
  
      tween2 = new TWEEN.Tween(item.meshs[i + 1].position);
      tween2.easing(TWEEN.Easing.Circular.In);
      tween2.to({ z: item.espessura()/2 + item.posicaoZ }, time);
      tween2.start();
  
      tween3 = new TWEEN.Tween(item.meshs[i + 2].position);
      tween3.easing(TWEEN.Easing.Circular.In);
      tween3.to({ z: item.espessura()/2 + item.posicaoZ }, time);
      tween3.start();
  
      tween4 = new TWEEN.Tween(item.meshs[i + 3].position);
      tween4.easing(TWEEN.Easing.Circular.In);
      tween4.to({ z: item.espessura()/2 + item.posicaoZ }, time);
      tween4.start();
  
      tween5 = new TWEEN.Tween(item.meshs[i + 4].position);
      tween5.easing(TWEEN.Easing.Circular.In);
      tween5.to({ z: - item.profundidade / 2 + item.espessura()*1.5 + item.posicaoZ }, time);
      tween5.start();
  
      tween6 = new TWEEN.Tween(item.meshs[i + 5].position);
      tween6.easing(TWEEN.Easing.Circular.In);
      tween6.to({ z: item.profundidade / 2 + item.espessura() + item.posicaoZ }, time);
      tween6.start();
  
      this.animate();
      i += 6;
    }
  }

  animate() {
    requestAnimationFrame(() => {
      this.animate();
    });
    TWEEN.update();
  }


  fecharPorta(item) {
    let tween1 = new TWEEN.Tween(item.meshs[item.meshs.length - 1].rotation);
    tween1.easing(TWEEN.Easing.Circular.In);
    tween1.to({ y: 0 }, 1400); // 1200 milisegundos
    tween1.start();

    let tween2 = new TWEEN.Tween(item.meshs[item.meshs.length - 2].rotation);
    tween2.easing(TWEEN.Easing.Circular.In);
    tween2.to({ y: 0 }, 1200); // 1000 milisegundos
    tween2.start();

    this.animate();
  }

  buscarItemPorMesh(mesh) {
    let i;
    let j;

    for (i = 0; i < this.listaTodosItens.length; i++) {
      let itemAtual = this.listaTodosItens[i];
      for (j = 0; j < itemAtual.meshs.length; j++) {
        if (itemAtual.meshs[j].children.length > 0) { // no caso do pivot das portas
          let k;
          for (k = 0; k < itemAtual.meshs[j].children.length; k++) {
            if (mesh.name == itemAtual.meshs[j].children[k].name) return itemAtual;
          }
        }
        let meshAtual = itemAtual.meshs[j];
        if (mesh == meshAtual) return itemAtual;
      }
    }
    return null;
  }

  removerTodosItens(){
    this.listaTodosItens = [];
  }

  removerTodasMeshes2(){
    this.listaTodosMeshes = [];
  }

  removerTodasMeshes(item, scene) {
    let i;
    let j;
    console.log(scene.children.length);

    for (i = 0; i < item.meshs.length; i++) {
      scene.remove(item.meshs[i]);

    }
    console.log("ola");
    if (typeof item.itensFilhos !== 'undefined')
      for (j = 0; j < item.itensFilhos.length; j++) {
        this.removerTodasMeshes(item.itensFilhos[j], scene);
      }
  }

  verificarEvento(meshRecebida, itemRecebido) {
    let index = 0, itemDummy = itemRecebido;
    if (meshRecebida.name == 'portaA') {
      this.fecharPortas(itemRecebido);
      meshRecebida.name = 'portaF';
    } else if (meshRecebida.name == 'portaF') {
      this.abrirPortas(itemRecebido, index, false);
      meshRecebida.name = 'portaA';
    } else if (meshRecebida.name == 'gavetaA') {
      this.fecharGavetas(itemRecebido);
      meshRecebida.name = 'gavetaF';
    } else if (meshRecebida.name == 'gavetaF') { 
      this.abrirGavetas(itemRecebido, index, false);
      meshRecebida.name = 'gavetaA';
    }
  }
}
