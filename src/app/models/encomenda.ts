import { Item } from "./item";

export class Encomenda {
    idEncomenda: number;
    Itens: Item [];
    itens: Item [];
    dataEncomenda:string; 
    _id:string;
    idCidade: number;
    cidade: string;
    IdCliente: number;
    EstadoEncomenda: string;
    Morada: string;
    Preco: number;
    preco: number;

    numeroItensEncomenda : number; // só para mostrar na UI
    dataEncomendaUI: string; // só para mostrar na UI
    todosItens : Item []; // só para mostrar na UI
    precoMostrarTotal : number;
}