import { Component, OnInit } from '@angular/core';
import { Acabamento } from 'src/app/models/acabamento';
import { Material } from 'src/app/models/material';
import { AcabamentoService } from 'src/app/services/acabamento.service';
import { HistoricoPrecoService } from 'src/app/services/historico-preco.service';
import { MaterialService } from 'src/app/services/material.service';
import { NgModel } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material';
import { HistoricoPreco } from 'src/app/models/historicoPreco';

@Component({
  selector: 'app-historicopreco',
  templateUrl: './historicopreco.component.html',
  styleUrls: ['./historicopreco.component.css']
})
export class HistoricoprecoComponent implements OnInit {

  /**
   * listagem de historicos
   */
  materialListar: Material;
  acabamentoListar: Acabamento;
  colunasVisiveis = ['Material', 'Acabamento', 'Preço', 'Data'];
  dadosHistorico: any[];

  //fim
  historicoHidden01: boolean = false;
  historicoHidden02: boolean = false;
  historicoHidden03: boolean = false;
  precoMaterialHidden: boolean = true;
  listaMateriais: Material[] = [];
  listaAcabamentos: Acabamento[] = [];
  listaAcabamentosMaterial: Acabamento[] = [];
  listaHistoricos: HistoricoPreco[] = [];
  listaHistoricosFuturos: HistoricoPreco[] = [];
  materialSelecionado: Material = null;
  precoMaterial: number = null;
  precoAcabamento: number[] = [];
  dataPicker: Date = null;
  minDate: Date = new Date();
  acabamentosSelecionados: Acabamento[] = [];
  historicoEliminar: HistoricoPreco;
  valorRadioButton: string;



  constructor(private acabamentoService: AcabamentoService, private materialService: MaterialService,
    private historicoService: HistoricoPrecoService) { }

  ngOnInit() {
    this.buscarMateriais();
    this.buscarAcabamentos();
    this.buscarHistoricos();
  }

  buscarHistoricos() {
    this.historicoService.buscarHistoricos().subscribe(historicos => { this.listaHistoricos = historicos });
  }

  buscarMateriais() {
    this.materialService.buscarMateriais().subscribe(materiais => { this.listaMateriais = materiais; });
  }
  buscarAcabamentos() {
    this.acabamentoService.buscarAcabamentos().subscribe(acabamentos => { this.listaAcabamentos = acabamentos; });
  }

  listarAcabamentos() {
    console.log(this.materialListar.id)
    console.log(this.listaHistoricos)
    this.acabamentoService.buscarAcabamentoDeMaterial(this.materialListar.id).subscribe(
      acabamentos => {
        this.listaAcabamentosMaterial = acabamentos;
      }
    );
    this.historicoService.buscarHistorioMaterial(this.materialListar.id).subscribe(
      historico => {
        this.dadosHistorico = [];
        historico.forEach(historicoAtual => {
          let dataEmString = historicoAtual.preco.dataInicio.toString();
          let dataDividida = dataEmString.split('T');
          let nomeMaterial;
          this.listaMateriais.forEach(materialAtual => {
            if (materialAtual.id == historicoAtual.id_Material) {
              nomeMaterial = materialAtual.nome.conteudo;
            }
          })
          let linhaDados = {
            Material: nomeMaterial,
            Acabamento: 'Não contabilizado',
            Preço: historicoAtual.preco.valor + '€',
            Data: dataDividida[0],
          }
          this.dadosHistorico.push(linhaDados);
        })
      }
    );
  }

  listarInformacaoPreco() {
    if (this.acabamentoListar == null) {
      this.listarAcabamentos();
      return;
    }
    this.historicoService.buscarHistoricoMaterialAcabamento(this.materialListar.id, this.acabamentoListar.id).subscribe(
      historico => {
        this.dadosHistorico = [];
        historico.forEach(historicoAtual => {
          let dataEmString = historicoAtual.preco.dataInicio.toString();
          let dataDividida = dataEmString.split('T');
          let nomeMaterial;
          this.listaMateriais.forEach(materialAtual => {
            if (materialAtual.id == historicoAtual.id_Material) {
              nomeMaterial = materialAtual.nome.conteudo;
            }
          });
          let nomeAcabamento;
          this.listaAcabamentos.forEach(acabamentoAtual => {
            if (acabamentoAtual.id == historicoAtual.id_Acabamento) {
              nomeAcabamento = acabamentoAtual.nome.conteudo;
            }
          });
          let linhaDados = {
            Material: nomeMaterial,
            Acabamento: nomeAcabamento,
            Preço: 'Acréscimo de ' + historicoAtual.preco.valor + '€',
            Data: dataDividida[0],
          }
          this.dadosHistorico.push(linhaDados);
        })
      }
    )
  }

  listarAcabamentosMaterial() {
    if (this.dataPicker == null) {
      alert('Selecione uma data');
      return;
    }
    let month;
    let day;
    let mes;
    if (this.dataPicker.getMonth() < 10) {
      mes = this.dataPicker.getMonth() + 1
      month = '0' + mes
    } else {
      month = this.dataPicker.getMonth() + 1
    }
    if (this.dataPicker.getDate() < 10) {
      day = '0' + this.dataPicker.getDate()
    } else {
      day = this.dataPicker.getDate()
    }
    let data = this.dataPicker.getFullYear() + '-' + month + '-' + day + 'T00:00:00'
    this.historicoService.buscarHistoricos().subscribe(historicos => {
      for (let i = 0; i < historicos.length; i++) {
        if (historicos[i].id_Material == this.materialSelecionado.id
          && historicos[i].id_Acabamento == null && data == historicos[i].preco.dataInicio.toString()) {
          this.precoMaterial = historicos[i].preco.valor;
        }
      }
    });
    this.listaAcabamentosMaterial = [];
    this.materialService.buscarMaterialPorId(this.materialSelecionado.id).subscribe(material => {
      for (let i = 0; i < material.acabamentos.length; i++) {
        for (let j = 0; j < this.listaAcabamentos.length; j++) {
          if (material.acabamentos[i].idAcabamento == this.listaAcabamentos[j].id) {
            this.listaAcabamentosMaterial.push(this.listaAcabamentos[j]);
          }
        }
      }
    })
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.precoMaterial = null
    this.dataPicker = event.value;
    if (this.materialSelecionado != null) {
      let month
      let day
      let mes
      if (this.dataPicker.getMonth() < 10) {
        mes = this.dataPicker.getMonth() + 1
        month = '0' + mes
      } else {
        month = this.dataPicker.getMonth() + 1
      }
      if (this.dataPicker.getDate() < 10) {
        day = '0' + this.dataPicker.getDate()
      } else {
        day = this.dataPicker.getDate()
      }
      let data = this.dataPicker.getFullYear() + '-' + month + '-' + day + 'T00:00:00'
      for (let i = 0; i < this.listaHistoricos.length; i++) {
        if (this.listaHistoricos[i].id_Material == this.materialSelecionado.id
          && this.listaHistoricos[i].id_Acabamento == null && data == this.listaHistoricos[i].preco.dataInicio.toString()) {
          this.precoMaterial = this.listaHistoricos[i].preco.valor;
        }
      }
    }
  }

  adicionarHistorico() {
    if (this.dataPicker == null) {
      alert("Insira uma data!");
      return;
    }
    if (!this.materialSelecionado || this.materialSelecionado == null) {
      alert("Selecione um material!");
      return;
    }
    if (!this.precoMaterial || this.precoMaterial <= 0) {
      alert("O material tem de ter preço válido!");
      return;
    }
    if (this.acabamentosSelecionados.length > 0) {
      for (let i = 0; i < this.acabamentosSelecionados.length; i++) {
        if (this.precoAcabamento[i] == null || !this.precoAcabamento[i] || this.precoAcabamento[i]<=0) {
          alert("O acabamento tem de ter preço válido!");
          return;
        }
      }
    }

    let historicoAdicionar1 = new HistoricoPreco();
    historicoAdicionar1.id_Material = this.materialSelecionado.id;
    historicoAdicionar1.id_Acabamento = null;
    historicoAdicionar1.preco = { valor: this.precoMaterial, dataInicio: this.dataPicker };
    console.log(historicoAdicionar1)
    this.historicoService.adicionarHistorico(historicoAdicionar1).subscribe(_ => {
      this.buscarHistoricos();
    });

    if (this.acabamentosSelecionados != null && this.acabamentosSelecionados.length > 0) {
      let historicoAdicionar = new HistoricoPreco();

      for (let i = 0; i < this.acabamentosSelecionados.length; i++) {
        historicoAdicionar.id_Material = this.materialSelecionado.id;
        historicoAdicionar.id_Acabamento = this.acabamentosSelecionados[i].id;
        historicoAdicionar.preco = { valor: this.precoAcabamento[i], dataInicio: this.dataPicker };

        this.historicoService.adicionarHistorico(historicoAdicionar).subscribe(_ => {
          this.buscarHistoricos();
        });
      }
    }
  }

  listarHistoricosFuturos() {
    let comparador = this.valorRadioButton;
    this.historicoService.buscarHistoricosFuturos(comparador).subscribe(historicosFuturos => {
      historicosFuturos.forEach(historicoFuturoAtual => {
        this.materialService.buscarMaterialPorId(historicoFuturoAtual.id_Material).subscribe(
          materialRecebido => {
            if (materialRecebido == null) {
              historicoFuturoAtual.nomeMaterial = "Material já foi apagado."
            } else {
              historicoFuturoAtual.nomeMaterial = materialRecebido.nome.conteudo;
            }
          }
        );
        let dataEmString = historicoFuturoAtual.preco.dataInicio.toString();
        let dataDividida = dataEmString.split('T');
        historicoFuturoAtual.dataMostrar = dataDividida[0];
        if (historicoFuturoAtual.id_Acabamento != null) {
          this.acabamentoService.buscarAcabamentoPorId(historicoFuturoAtual.id_Acabamento).subscribe(
            acabamento => {
              if (acabamento == null) {
                historicoFuturoAtual.nomeAcabamento = "Acabamento já foi apagado.";
              } else {
                historicoFuturoAtual.nomeAcabamento = acabamento.nome.conteudo;
              }
            }
          );
        }
      });
      this.listaHistoricosFuturos = historicosFuturos;
    });
  }

  eliminarHistorico() {
    if (this.historicoEliminar == null) {
      alert('Por favor selecione o histórico que deseja remover.');
      return;
    }
    this.historicoService.eliminarHistorico(this.historicoEliminar.id).subscribe(
      _ => {
        this.buscarHistoricos();
        this.historicoEliminar = null;
        this.valorRadioButton = null;
        this.listaHistoricosFuturos = [];
      }
    );
  }

  selecionarTodosAcabamentos(checkAll, select: NgModel, values) {
    if (checkAll) {
      select.update.emit(values);
    } else {
      select.update.emit([]);
    }
  }

  limparDados() {
    this.valorRadioButton = null;
    this.listaHistoricosFuturos = [];
    this.materialListar = null;
    this.acabamentoListar = null;
    this.dadosHistorico = [];
    this.materialSelecionado = null;
    this.precoMaterial = null;
    this.precoAcabamento = [];
    this.dataPicker = null;
    this.acabamentosSelecionados = [];
    this.historicoEliminar = null;
    this.listaAcabamentosMaterial = [];
  }


  estadoDefinirHistorico() {
    this.limparDados();
    this.historicoHidden01 = !this.historicoHidden01;
    this.historicoHidden02 = false;
    this.historicoHidden03 = false;
  }

  estadoListarHistorico() {
    this.limparDados();
    this.historicoHidden01 = false;
    this.historicoHidden02 = !this.historicoHidden02;
    this.historicoHidden03 = false;
  }

  estadoEliminarHistorico() {
    this.limparDados();
    this.historicoHidden01 = false;
    this.historicoHidden02 = false;
    this.historicoHidden03 = !this.historicoHidden03;
  }

}