export class Restricao {
    id : number;
    idPai: number;
    idFilho: number;
    percentagemAlturaMinima: number;
    percentagemAlturaMaxima: number;
    percentagemLarguraMinima: number;
    percentagemLarguraMaxima: number;
    percentagemProfundidadeMinima: number;
    percentagemProfundidadeMaxima: number;
    idMaterialPai: number;
    idMaterialFilho: number;
    obrigatoriedade: boolean;
    nomeProdutoPai: string;
    nomeProdutoFilho: string;
    nomeMaterialPai: string;
    nomeMaterialFilho: string;
    percentagemAlturaMinimaMostrar: string;
    percentagemAlturaMaximaMostrar: string;
    percentagemLarguraMinimaMostrar: string;
    percentagemLarguraMaximaMostrar: string;
    percentagemProfundidadeMinimaMostrar: string;
    percentagemProfundidadeMaximaMostrar: string;
}