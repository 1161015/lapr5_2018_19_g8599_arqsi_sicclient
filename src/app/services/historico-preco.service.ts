import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Material } from '../models/material';
import { HistoricoPreco } from '../models/historicoPreco';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HistoricoPrecoService {

  private historicoURL = 'https://sic-productions.azurewebsites.net/api/historicoPreco';
  // private historicoURL = 'https://localhost:44352/api/historicoPreco';

  constructor(private http: HttpClient) { }

  private log(message: string): void {
    console.log(message);
  }

  buscarHistoricos(): Observable<HistoricoPreco[]> {
    return this.http.get<HistoricoPreco[]>(this.historicoURL).pipe(
      tap(_ => this.log('Históricos carregados com sucesso')),
      catchError(this.handleError('buscar históricos', [])
      )
    );
  }

  buscarHistoricosFuturos(comparador: string): Observable<HistoricoPreco[]> {
    let url = this.historicoURL + '/futuro/' + comparador;
    return this.http.get<HistoricoPreco[]>(url).pipe(
      tap(_ => { }),
      catchError(this.handleError('buscar históricos futuros', [])
      ));
  }

  buscarPrecoAtualMaterial(idMaterial: number): Observable<HistoricoPreco> {
    let url = this.historicoURL + '/material/atual/' + idMaterial;
    return this.http.get<HistoricoPreco>(url).pipe(
      tap(_ => { }),
      catchError(this.handleError<HistoricoPreco>('erro ao buscar preco atual do material'))
    );
  }

  buscarAcrescimoAtualMaterialAcabamento(idMaterial: number, idAcabamento: number): Observable<HistoricoPreco> {
    let url = this.historicoURL + '/acabamento/atual/' + idAcabamento + '/' + idMaterial;
    return this.http.get<HistoricoPreco>(url).pipe(
      tap(_ => { }),
      catchError(this.handleError<HistoricoPreco>('erro ao buscar acrescimo atual do acabamento'))
    );
  }

  buscarHistorioMaterial(idMaterial: number): Observable<HistoricoPreco[]> {
    let url = this.historicoURL + '/material/' + idMaterial;
    return this.http.get<HistoricoPreco[]>(url).pipe(
      tap(_ => { }), catchError(this.handleError('buscar historico só de material', []))
    );
  }

  buscarHistoricoMaterialAcabamento(idMaterial: number, idAcabamento: number): Observable<HistoricoPreco[]> {
    let url = this.historicoURL + '/acabamento/' + idAcabamento + '/' + idMaterial;
    return this.http.get<HistoricoPreco[]>(url).pipe(
      tap(_ => { }), catchError(this.handleError('Buscar historico de material junto com acabamento', []))
    );
  }

  buscarHistoricoPorId(id: number): Observable<HistoricoPreco> {
    let url = this.historicoURL + '/' + id;
    return this.http.get<HistoricoPreco>(url).pipe(
      tap(_ => this.log(`Histórico carregado`)),
      catchError(this.handleError<HistoricoPreco>(`Erro ao buscar histórico com o id=${id}`))
    )
  }

  adicionarHistorico(historico: HistoricoPreco): Observable<HistoricoPreco> {
    return this.http.post<HistoricoPreco>(this.historicoURL, historico, httpOptions).pipe(
      tap((historicoPreco: HistoricoPreco) => alert('Histórico adicionado')),
      catchError(this.handleError<HistoricoPreco>('adicionar histórico'))
    );
  }

  atualizarHistorico(historico: HistoricoPreco): Observable<HistoricoPreco> {
    let url = this.historicoURL + '/' + historico.id;
    return this.http.put<HistoricoPreco>(url, historico, httpOptions).pipe(
      tap((historicoPreco: HistoricoPreco) => alert(`Historico atualizado`)),
      catchError(this.handleError<HistoricoPreco>('atualizar historico'))
    );
  }

  eliminarHistorico(id: number): Observable<HistoricoPreco> {
    let url = this.historicoURL + '/' + id;
    return this.http.delete<HistoricoPreco>(url, httpOptions).pipe(
      tap(_ => alert(`Histórico-Preço eliminado`)),
      catchError(this.handleError<HistoricoPreco>('Apagar Histórico-Preço'))
    );
  }


  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
