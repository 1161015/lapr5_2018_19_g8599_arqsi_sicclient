import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ItemComponent } from './item.component';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatCardModule, MatTooltipModule, MatSliderModule } from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { MaterialModule } from 'src/app/material.module';


describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemComponent ],
      imports:[MatSliderModule, MaterialModule, MatSelectModule, MatInputModule, FormsModule, RouterTestingModule, HttpClientTestingModule, MatCardModule, MatTooltipModule, ColorPickerModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#estadoCriarItem() should toggle #mostrarCriarItem', () =>{
    expect(component.mostrarCriarItem).toBe(false, "off at first");
    component.estadoCriarItem();
    expect(component.mostrarCriarItem).toBe(true, "on after pressed");
    component.estadoCriarItem();
    expect(component.mostrarCriarItem).toBe(false, "off after pressed again");
  });

  it('#estadoListarItens() should toggle #mostrarListarItens', () =>{
    expect(component.mostrarListarItens).toBe(false, "off at first");
    component.estadoListarItem();
    expect(component.mostrarListarItens).toBe(true, "on after pressed");
    component.estadoListarItem();
    expect(component.mostrarListarItens).toBe(false, "off after pressed again");
  });

  it('#estadoProcurarItem() should toggle #mostrarProcurarItem', () =>{
    expect(component.mostrarProcurarItem).toBe(false, "off at first");
    component.estadoProcurarItem();
    expect(component.mostrarProcurarItem).toBe(true, "on after pressed");
    component.estadoProcurarItem();
    expect(component.mostrarProcurarItem).toBe(false, "off after pressed again");
  });

  it('#estadoApagarItem() should toggle #mostrarEliminarItem', () =>{
    expect(component.mostrarApagarItem).toBe(false, "off at first");
    component.estadoEliminarItem();
    expect(component.mostrarApagarItem).toBe(true, "on after pressed");
    component.estadoEliminarItem();
    expect(component.mostrarApagarItem).toBe(false, "off after pressed again");
  });

  it('#estadoEditarItem() should toggle #mostrarEditarItem', () =>{
    expect(component.mostrarEditarItem).toBe(false, "off at first");
    component.estadoEditarItem();
    expect(component.mostrarEditarItem).toBe(true, "on after pressed");
    component.estadoEditarItem();
    expect(component.mostrarEditarItem).toBe(false, "off after pressed again");
  });
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
