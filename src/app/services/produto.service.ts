import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Produto } from '../models/produto';
import { CategoriaService } from './categoria.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

   private produtoURL = 'https://sic-productions.azurewebsites.net/api/produto'
  //private produtoURL = 'https://localhost:44352/api/produto'

  constructor(private http: HttpClient, private categoriaService: CategoriaService) { }

  private log(message: string): void {
    console.log(message);
  }

  produtosObrigatorios(idPai: number): Observable<Produto[]> {
    let url = this.produtoURL + '/' + idPai + '/filhos/obrigatorios';
    return this.http.get<Produto[]>(url).pipe(
      tap(_ => { }),
      catchError(this.handleError('buscar produtos obrigatórios', []))
    );
  }

  buscarProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.produtoURL).pipe(
      tap(pr => { }),
      catchError(this.handleError('buscar produtos', []))
    );
  }


  buscarProdutosPorColecao(idColecao: number): Observable<Produto[]> {
    let urlFinal = this.produtoURL + '/verProdutos/colecao/' + idColecao;
    return this.http.get<Produto[]>(urlFinal).pipe(
      tap(produto => { }),
      catchError(this.handleError(`Erro ao buscar produtos da coleção`, [])));
  }

  buscarProdutosPorCatalogo(idCatalogo: number): Observable<Produto[]> {
    let urlFinal = this.produtoURL + '/verProdutos/catalogo/' + idCatalogo;
    return this.http.get<Produto[]>(urlFinal).pipe(
      tap(produto => { }),
      catchError(this.handleError(`Erro ao buscar produtos do catálogo`, [])));
  }

  buscarProdutoPorId(idProduto: number): Observable<Produto> {
    let urlFinal = this.produtoURL + '/' + idProduto;
    return this.http.get<Produto>(urlFinal).pipe(
      tap(produto => { }),
      catchError(this.handleError<Produto>(`Erro ao buscar o produto`)));
  }

  adicionarProduto(produto: Produto): Observable<Produto> {
    return this.http.post<Produto>(this.produtoURL, produto, httpOptions).pipe(
      tap((produto: Produto) => alert(`Produto adicionado`)),
      catchError(this.handleError<Produto>('adicionar produto'))
    );
  }



  atualizarproduto(produto: Produto): Observable<Produto> {
    console.log(produto);
    let url = this.produtoURL + '/' + produto.produtoId;
    return this.http.put(url, produto, httpOptions).pipe(
      tap(_ => {}),
      catchError(this.handleError<any>('atualizar produto'))
    );
  }

  eliminarProduto(id: number): Observable<Produto> {
    let url = this.produtoURL + '/' + id;
    return this.http.delete<Produto>(url, httpOptions).pipe(
      tap(_ => alert(`Produto Apagado`)),
      catchError(this.handleError<Produto>('Apagar Produto'))
    );
  }

  buscarProdutosFilho(idProduto: number): Observable<Produto[]> {
    let urlFinal = this.produtoURL + '/' + idProduto + '/partes';
    return this.http.get<Produto[]>(urlFinal).pipe(
      tap(_ => {}),
      catchError(this.handleError('buscar produtos', []))
    );
  }



  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }



}
