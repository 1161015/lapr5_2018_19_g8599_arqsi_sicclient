import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncomendaComponent } from './encomenda.component';
import { MatSelectModule, MatStepperModule, MatTableModule, MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

describe('EncomendaComponent', () => {
  let component: EncomendaComponent;
  let fixture: ComponentFixture<EncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EncomendaComponent],
      imports: [FormsModule, MatSelectModule, MatStepperModule, MatTabsModule, HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('#estadoCriarEncomenda() should toggle #mostrarCriarEncomenda', () => {
    expect(component.mostrarCriarEncomenda).toBe(false, 'off at first');
    component.estadoCriarEncomenda();
    expect(component.mostrarCriarEncomenda).toBe(true, 'on after pressed');
    component.estadoCriarEncomenda();
    expect(component.mostrarCriarEncomenda).toBe(false, 'off after pressed again');
  });

  it('#estadoListarEncomendas() should toggle #mostrarListarEncomenda', () => {
    expect(component.mostrarListarEncomenda).toBe(false, 'off at first');
    component.estadoListarEncomendas();
    expect(component.mostrarListarEncomenda).toBe(true, 'on after pressed');
    component.estadoListarEncomendas();
    expect(component.mostrarListarEncomenda).toBe(false, 'off after pressed again');
  });*/
});
