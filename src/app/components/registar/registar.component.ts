import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilizador } from '../../models/utilizador';
import { RegistarService } from '../../services/registar.service';
import { Cidade } from 'src/app/models/cidade';
import { EncomendaService } from '../../services/encomenda.service';
import { ItemComponent } from '../item/item.component';


@Component({
  selector: 'app-registar',
  templateUrl: './registar.component.html',
  styleUrls: ['./registar.component.css']
})


export class RegistarComponent implements OnInit {

  hide1 = false;
  hide2 = false;
  termos = false;
  tratamento = false;
  registar = true;
  aceitarTermos: boolean = false;
  aceitarTratamento: boolean = false;
  mostrarBotaoVoltarRegisto: boolean = true;
  mostrarBotaoRegisto: boolean = true;
  mostrarSpinnerRegistar: boolean = false;

  cidades: Cidade[] = [];
  cidadeSelecionada: Cidade = null;

  constructor(private _router: Router, private registarService: RegistarService, private encomendaService: EncomendaService) {
  }

  ngOnInit() {
    this.buscarCidades();
    ItemComponent.podeCriarScene = true;
  }

  buscarCidades(): any {
    this.encomendaService.buscarCidades().subscribe(cidades => {
      this.cidades = cidades;
    });
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  mostrarTermos() {
    this.termos = true;
    this.tratamento = false;
    this.registar = false;
  }

  mostrarTratamento() {
    this.termos = false;
    this.tratamento = true;
    this.registar = false;
  }

  voltarTermos(){
    this.termos = false;
    this.tratamento = false;
    this.registar = true;
  }

  voltarTratamento(){
    this.termos = false;
    this.tratamento = false;
    this.registar = true;
  }

  abrirPaginaInicial(){
    this._router.navigate(['/']);
  }

  registoUtilizador(usernameInserido: string, emailInserido: string, passwordInserida: string, passwordInserida2: string, nomeInserido: string) {
    var verificaUsername = /^[a-z0-9]{3,}$/i.test(usernameInserido);
    var verificaNome = /^[a-záàâãéèêíïóôõöúçñ ]+$/i.test(nomeInserido);
    var verificaEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailInserido);
    var verificarPassword = /[#$%^&*/\[\]\\()":{}|<>]/.test(passwordInserida);
    var verificarPassword2 = /(.){8,}/.test(passwordInserida);
    var verificarPassword3 = /[0-9]{1,}/.test(passwordInserida);
    var verificarPassword4 = /[A-Z]{1,}/.test(passwordInserida);
    var verificarPassword5 = /[a-z]{1,}/.test(passwordInserida);
    var verificarPassword6 = /[,.!?@]{1}/.test(passwordInserida);
    var passwordsIguais;
    if (passwordInserida == passwordInserida2) {
      passwordsIguais = true;
    } else {
      passwordsIguais = false;
    }
    if (!verificaUsername) {
      alert("Por favor, insira um Username válido.")
      return;
    }
    if (!verificaNome) {
      alert("Por favor, insira um Nome válido.")
      return;
    }

    if (this.cidadeSelecionada == null) {
      alert("Por favor, selecione uma Cidade.")
      return;
    }
    if (!verificaEmail) {
      alert("Por favor, insira um Email válido.");
      return;
    }
    if (!verificarPassword2) {
      alert("Por favor, insira uma Password com pelo menos 8 caracteres.");
      return;
    }
    if (verificarPassword) {
      alert("Por favor, insira uma Password válida.");
      return;
    }
    if (!verificarPassword3) {
      alert("Por favor, insira uma Password com pelo menos 1 algarismo.");
      return;
    }
    if (!verificarPassword4) {
      alert("Por favor, insira uma Password com pelo menos 1 letra maiúscula.");
      return;
    }
    if (!verificarPassword5) {
      alert("Por favor, insira uma Password com pelo menos 1 letra minúscula.");
      return;
    }
    if (!verificarPassword6) {
      alert("Por favor, insira uma Password com pelo menos 1 caracter especial.");
      return;
    }
    if (!passwordsIguais) {
      alert("Por favor, as Passwords inseridas precisam de ser iguais.");
      return;
    }
    if (!this.aceitarTermos) {
      alert("Para a criar uma conta de utilizador, precisa de aceitar os Termos e Condições do site.");
      return;
    }

    this.mostrarBotaoVoltarRegisto = false;
    this.mostrarBotaoRegisto = false;
    this.mostrarSpinnerRegistar = true;

    let novoUtilizador = new Utilizador();
    novoUtilizador.nome = {
      nome: nomeInserido
    };

    novoUtilizador.email = {
      email: emailInserido
    };
    novoUtilizador.morada = {
      morada: this.cidadeSelecionada.Nome
    };
    novoUtilizador.password = {
      password: passwordInserida
    };
    novoUtilizador.username = {
      username: usernameInserido
    };
    novoUtilizador.termos = this.aceitarTermos;

    novoUtilizador.tratamento = this.aceitarTratamento;

    console.log(novoUtilizador);
    this.registarService.registarUtilizador(novoUtilizador).subscribe(_ => {
      this.mostrarBotaoVoltarRegisto = true;
      this.mostrarBotaoRegisto = true;
      this.mostrarSpinnerRegistar = false;
      alert('Foi criada uma conta, para o utilizador "' + usernameInserido + '" de nome "' + nomeInserido + '".' + ' Com o email "' + emailInserido + '".');
      this._router.navigate(['/login']);
    }, error => {
      alert('Username ou Email já se encontram em uso!');
      this.mostrarBotaoVoltarRegisto = true;
      this.mostrarBotaoRegisto = true;
      this.mostrarSpinnerRegistar = false;
    });

  }
}
