import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Acabamento } from 'src/app/models/acabamento';
import { AcabamentoService } from 'src/app/services/acabamento.service';
import { ProdutoService } from 'src/app/services/produto.service';
import { MaterialService } from 'src/app/services/material.service';
import { Material } from 'src/app/models/material';
import { Item } from 'src/app/models/item';
import { Produto } from 'src/app/models/produto';
import { ItemService } from 'src/app/services/item.service';
import { Catalogo } from 'src/app/models/catalogo';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { ColecaoService } from 'src/app/services/colecao.service';
import { Colecao } from 'src/app/models/colecao';
import { VisualizadorService } from 'src/app/services/visualizador.service';
import { CategoriaService } from 'src/app/services/categoria.service';
import { GeraModelo } from 'src/app/visualizador/geraModelo';
//import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {

  private canEleId = 'renderCanvas';

  //Editar Luzes do Visualizador
  colorLuzCriar: string;
  valorBrilhoLuzCriar: number;
  luzLigada: boolean = false;
  luzDesligada: boolean = true;

  //Editar Tamanhos do Visualizador
  larguraItem: number;
  alturaItem: number;
  profundidadeItem: number;

  //Editar Material do Visualizador
  texturasListar: any[] = [];
  imagemTexturaListar: string;

  larguraCriar: boolean = true;
  alturaCriar: boolean = true;
  profundidadeCriar: boolean = true;
  hideElement: boolean = false;
  mostrarSpinnerCriarItem: boolean = false;
  ligarvisualizacaoCriar: boolean = true;
  desligarvisualizacaoCriar: boolean = true;
  ligarCriar: boolean = true;

  mostrarCriarItem: boolean = false;
  mostrarApagarItem: boolean = false;
  mostrarListarItens: boolean = false;
  mostrarEditarItem: boolean = false;
  mostrarProcurarItem: boolean = false;
  mostrarVisualizador: boolean = false;
  previsualizacao: boolean = false;
  esconderDiv: boolean = true;

  listaItensNaoEncomendados: Item[] = [];
  itemApagar: Item;
  itemEditar: Item;
  itemProduto: Produto;
  materialItem: Material;
  acabamentoMaterial: Acabamento;
  listaProdutosFilho: Produto[] = []; // lista auxiliar
  listaItensFilhoAux: Item[] = []; // lista auxiliar
  itemsFilho: Item[] = [];
  itemsFilhoEnviar: Item[] = [];
  itemProcurar: Item;
  tooltipLargura: string;
  tooltipLarguraEditar: string;
  tooltipAltura: string;
  tooltipAlturaEditar: string;
  tooltipProfundidade: string;
  tooltipProfundidadeEditar: string;
  tooltipItensFilhosObrigatorios: string;
  tooltipItensFilhosObrigatoriosEditar: string;
  tooltipItensCriar: string;
  tooltipItensEditar: string;
  itensFilhos: string[];
  itemListar: Item = null;
  nomeTexturaListar: string = null;

  catalogoSelecionadoCriar: Catalogo;
  listaCatalogos: Catalogo[];

  colecaoSelecionadaCriar: Colecao;
  listaColecoes: Colecao[];

  //Inputs do Editar item
  materialItemEditar: Material;
  acabamentoMaterialEditar: Acabamento;
  larguraItemEditar: Number;
  alturaItemEditar: Number;
  profundidadeItemEditar: Number;

  larguraEditar: boolean = true;
  alturaEditar: boolean = true;
  profundidadeEditar: boolean = true;
  hideElementEditar: boolean = false;
  mostrarSpinnerEditarItem: boolean = false;
  ligarvisualizacaoEditar: boolean = true;
  desligarvisualizacaoEditar: boolean = true;
  ligarEditar: boolean = true;

  podeDesenhar: boolean = true;

  preco: number;

  /* Listas de Parâmetros usadas na criação e edição de Items */
  listaItens: Item[] = [];
  listaMateriais: Material[] = [];
  listaAcabamentos: Acabamento[] = [];
  listaItemsFilho: Item[] = [];
  listaProdutos: Produto[] = [];
  podeTerFilhos: boolean = false;
  /* -------------------------------------------------------- */

  public static podeCriarScene = true;

  constructor(private itemService: ItemService, private materialService: MaterialService, private acabamentoService: AcabamentoService, private produtoService: ProdutoService,
    private catalogoService: CatalogoService, private colecaoService: ColecaoService, private visualizadorService: VisualizadorService, private item: Item, private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.buscarItens();
    this.buscarCatalogos();
    this.buscarItensNaoEncomendados();
    this.texturasListar.push({
      nome: 'Wenge 1',
      urlImagem: '../../../assets/materiais/madeira1.png'
    });
    this.texturasListar.push({
      nome: 'Wenge 2',
      urlImagem: '../../../assets/materiais/madeira2.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 1',
      urlImagem: '../../../assets/materiais/madeira3.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 2',
      urlImagem: '../../../assets/materiais/madeira4.jpg'
    });
    this.texturasListar.push({
      nome: 'Madeira 5',
      urlImagem: '../../../assets/materiais/madeira5.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 1',
      urlImagem: '../../../assets/materiais/vidro1.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 2',
      urlImagem: '../../../assets/materiais/vidro2.jpg'
    });
  }

  ngAfterViewInit(): void {
    if (ItemComponent.podeCriarScene) {
      this.visualizadorService.createScene(this.canEleId);
      this.visualizadorService.animate();
      ItemComponent.podeCriarScene = false;
    }
  }

  //Metodos de Controlo das Luzes
  ligarLuz() {
    this.luzLigada = true;
    this.luzDesligada = false;
    this.visualizadorService.ligarLuz();
  }

  desligarLuz() {
    this.luzLigada = false;
    this.luzDesligada = true;
    this.visualizadorService.desligarLuz();
  }

  atualizarCoresCriar() {
    this.visualizadorService.alterarCor(this.colorLuzCriar);
  }

  atualizarBrilhoCriar() {
    this.visualizadorService.alterarIntensidade(this.valorBrilhoLuzCriar);
  }

  //Metodos de Controlo de Tamanhos de Item
  atualizarCubo() {
    let testeAlturaCont;
    let testeAlturaDisc;
    let testeLarguraCont;
    let testeLarguraDisc;
    let testeProfundidadeCont;
    let testeProfundidadeDisc;


    if (this.materialItem == null) {
      this.visualizadorService.apagaCubo();
      return;
    }
    if (this.acabamentoMaterial == null) {
      this.visualizadorService.apagaCubo();
      return;
    }
    if (this.alturaItem == null || this.alturaItem == 0) {
      this.visualizadorService.apagaCubo();
      return;
    }
    if (this.larguraItem == null || this.larguraItem == 0) {
      this.visualizadorService.apagaCubo();
      return;
    }
    if (this.profundidadeItem == null || this.profundidadeItem == 0) {
      this.visualizadorService.apagaCubo();
      return;
    }

    // há sempre pelo menos um valor portanto é seguro ir à posição 0
    //Altura
    if (this.itemProduto.dimensoes.altura[0].dimensao == null) { //se for contínua
      this.itemProduto.dimensoes.altura.forEach(alturaAtual => {
        if (this.alturaItem >= alturaAtual.dimensaoMin && this.alturaItem <= alturaAtual.dimensaoMax) {
          testeAlturaCont = true;
        }
      });
      if (!testeAlturaCont) {
        this.visualizadorService.apagaCubo();
        return;
      }
    } else { //se for discreta
      this.itemProduto.dimensoes.altura.forEach(alturaAtual => {
        if (this.alturaItem == alturaAtual.dimensao) {
          testeAlturaDisc = true;
        }
      });
      if (!testeAlturaDisc) {
        this.visualizadorService.apagaCubo();
        return;
      }
    }
    //Largura
    if (this.itemProduto.dimensoes.largura[0].dimensao == null) { //se for contínua
      this.itemProduto.dimensoes.largura.forEach(larguraAtual => {
        if (this.larguraItem >= larguraAtual.dimensaoMin && this.larguraItem <= larguraAtual.dimensaoMax) {
          testeLarguraCont = true;
        }
      });
      if (!testeLarguraCont) {
        this.visualizadorService.apagaCubo();
        return;
      }
    } else { //se for discreta
      this.itemProduto.dimensoes.largura.forEach(larguraAtual => {
        if (this.larguraItem == larguraAtual.dimensao) {
          testeLarguraDisc = true;
        }
      });
      if (!testeLarguraDisc) {
        this.visualizadorService.apagaCubo();
        return;
      }
    }
    //Profundidade
    if (this.itemProduto.dimensoes.profundidade[0].dimensao == null) { //se for contínua
      this.itemProduto.dimensoes.largura.forEach(profundidadeAtual => {
        if (this.profundidadeItem >= profundidadeAtual.dimensaoMin && this.profundidadeItem <= profundidadeAtual.dimensaoMax) {
          testeProfundidadeCont = true;
        }
      });
      if (!testeProfundidadeCont) {
        this.visualizadorService.apagaCubo();
        return;
      }
    } else { //se for discreta
      this.itemProduto.dimensoes.profundidade.forEach(profundidadeAtual => {
        if (this.profundidadeItem == profundidadeAtual.dimensao) {
          testeProfundidadeDisc = true;
        }
      });
      if (!testeProfundidadeDisc) {
        this.visualizadorService.apagaCubo();
        return;
      }
    }

    this.larguraCriar = false;
    this.alturaCriar = false;
    this.profundidadeCriar = false;
    this.hideElement = true;
    this.ligarvisualizacaoCriar = false;
    this.desligarvisualizacaoCriar = false;
    this.ligarCriar = false;
    this.mostrarSpinnerCriarItem = true;

    this.imagemTexturaListar = this.texturasListar[this.materialItem.texturaImagem].urlImagem;
    let novaAltura;
    let novaLargura;
    let novaProfundidade;
    if (this.alturaItem > 220) {
      novaAltura = 220;
    } else {
      novaAltura = this.alturaItem;
    }
    if (this.larguraItem > 300) {
      novaLargura = 300;
    } else {
      novaLargura = this.larguraItem;
    }
    if (this.profundidadeItem > 200) {
      novaProfundidade = 200;
    } else {
      novaProfundidade = this.profundidadeItem;
    }

    this.visualizadorService.apagaCubo();
    this.itemsFilhoEnviar = this.itemsFilho;
    let itFAux = [], i, cont = 0;
    for (i = 0; i < this.itemsFilhoEnviar.length; i++) {
      cont = cont + 1;
      itFAux.push(this.converteJsonEmItem(this.itemsFilhoEnviar[i], cont));
    }
    if (this.itemsFilhoEnviar.length == 0) {
      this.larguraCriar = true;
      this.alturaCriar = true;
      this.profundidadeCriar = true;
      this.hideElement = false;
      this.ligarvisualizacaoCriar = true;
      this.desligarvisualizacaoCriar = true;
      this.ligarCriar = true;
      this.mostrarSpinnerCriarItem = false;
    }
    this.visualizadorService.alterarCubo(novaAltura, novaLargura, novaProfundidade, this.imagemTexturaListar, this.materialItem, this.acabamentoMaterial, this.itemProduto, itFAux);

  }

  converteJsonEmItem(item, cont): Item {
    let it = new Item(), i;
    it.altura = item.altura;
    it.largura = item.largura;
    it.profundidade = item.profundidade;
    it.idProduto = item.idProduto;
    it.idMaterial = item.idMaterial;
    it.idAcabamento = item.idAcabamento;
    it.produto = item.produto;
    it.categoria = item.categoria;
    it.material = item.material;
    it.acabamento = item.acabamento;
    it.urlMaterial = this.texturasListar[it.material.texturaImagem].urlImagem;
    it.posicaoY = it.altura / 2;
    it.precoAcabamento = item.precoAcabamento;
    it.precoMaterial = item.precoMaterial;
    it.itensFilhos = [];
    if (typeof item.itensFilhos !== 'undefined')
      for (i = 0; i < item.itensFilhos.length; i++)
        it.itensFilhos.push(this.converteJsonEmItem(item.itensFilhos[i], cont));

    return it;
  }


  atualizarCuboEditar() {
    let testeAlturaCont;
    let testeAlturaDisc;
    let testeLarguraCont;
    let testeLarguraDisc;
    let testeProfundidadeCont;
    let testeProfundidadeDisc;
    this.podeDesenhar = true;

    if (this.materialItemEditar == null) {
      this.visualizadorService.apagaCubo();
      return;
    }

    if (this.acabamentoMaterialEditar == null) {
      this.visualizadorService.apagaCubo();
      return;
    }
    if (this.alturaItemEditar == null || this.alturaItemEditar == 0) {
      this.visualizadorService.apagaCubo();
      return;
    }

    if (this.larguraItemEditar == null || this.larguraItemEditar == 0) {
      this.visualizadorService.apagaCubo();
      return;
    }

    if (this.profundidadeItemEditar == null || this.profundidadeItemEditar == 0) {
      this.visualizadorService.apagaCubo();
      return;
    }

    this.produtoService.buscarProdutoPorId(this.itemEditar.idProduto).subscribe(produtoDoItem => {
      // há sempre pelo menos um valor portanto é seguro ir à posição 0
      //Altura
      if (produtoDoItem.dimensoes.altura[0].dimensao == null) { //se for contínua
        produtoDoItem.dimensoes.altura.forEach(alturaAtual => {
          if (this.alturaItemEditar >= alturaAtual.dimensaoMin && this.alturaItemEditar <= alturaAtual.dimensaoMax) {
            testeAlturaCont = true;
          }
        });
        if (!testeAlturaCont) {
          this.visualizadorService.apagaCubo();
          this.podeDesenhar = false;
          return;
        }
      } else { //se for discreta
        produtoDoItem.dimensoes.altura.forEach(alturaAtual => {
          if (this.alturaItemEditar == alturaAtual.dimensao) {
            testeAlturaDisc = true;
          }
        });
        if (!testeAlturaDisc) {
          this.visualizadorService.apagaCubo();
          this.podeDesenhar = false;
          return;
        }
      }
      //Largura
      if (produtoDoItem.dimensoes.largura[0].dimensao == null) { //se for contínua
        produtoDoItem.dimensoes.largura.forEach(larguraAtual => {
          if (this.larguraItemEditar >= larguraAtual.dimensaoMin && this.larguraItemEditar <= larguraAtual.dimensaoMax) {
            testeLarguraCont = true;
          }
        });
        if (!testeLarguraCont) {
          this.visualizadorService.apagaCubo();
          this.podeDesenhar = false;
          return;
        }
      } else { //se for discreta
        produtoDoItem.dimensoes.largura.forEach(larguraAtual => {
          if (this.larguraItemEditar == larguraAtual.dimensao) {
            testeLarguraDisc = true;
          }
        });
        if (!testeLarguraDisc) {
          this.visualizadorService.apagaCubo();
          this.podeDesenhar = false;
          return;
        }
      }
      //Profundidade
      if (produtoDoItem.dimensoes.profundidade[0].dimensao == null) { //se for contínua
        produtoDoItem.dimensoes.profundidade.forEach(profundidadeAtual => {
          if (this.profundidadeItemEditar >= profundidadeAtual.dimensaoMin && this.profundidadeItemEditar <= profundidadeAtual.dimensaoMax) {
            testeProfundidadeCont = true;
          }
        });
        if (!testeProfundidadeCont) {
          this.visualizadorService.apagaCubo();
          this.podeDesenhar = false;
          return;
        }
      } else { //se for discreta
        produtoDoItem.dimensoes.profundidade.forEach(profundidadeAtual => {
          if (this.profundidadeItemEditar == profundidadeAtual.dimensao) {
            testeProfundidadeDisc = true;
          }
        });
        if (!testeProfundidadeDisc) {
          this.visualizadorService.apagaCubo();
          this.podeDesenhar = false;
          return;
        }
      }

      if (!this.podeDesenhar) {
        this.visualizadorService.apagaCubo();
        return;
      }



      this.imagemTexturaListar = this.texturasListar[this.materialItemEditar.texturaImagem].urlImagem;
      let novaAltura;
      let novaLargura;
      let novaProfundidade;
      if (this.alturaItemEditar > 220) {
        novaAltura = 220;
      } else {
        novaAltura = this.alturaItemEditar;
      }
      if (this.larguraItemEditar > 300) {
        novaLargura = 300;
      } else {
        novaLargura = this.larguraItemEditar;
      }
      if (this.profundidadeItemEditar > 200) {
        novaProfundidade = 200;
      } else {
        novaProfundidade = this.profundidadeItemEditar;
      }
      console.log('testando');
      console.log(this.itemEditar);
      this.visualizadorService.apagaCubo();
      this.itemsFilhoEnviar = this.itemsFilho;
      let itFAux = [], i, cont = 0;
      for (i = 0; i < this.itemsFilhoEnviar.length; i++) {
        cont = cont + 1;
        let j;
        for (j = 0; j < this.listaItens.length; j++) {
          if (this.listaItens[j]._id == this.itemsFilhoEnviar[i]._id) {
            itFAux.push(this.converteJsonEmItem(this.listaItens[j], cont));
            console.log(this.listaItens[j]);
            break;
          }
        }
      }
      if (this.itemsFilhoEnviar.length == 0) {

      }
      
      this.visualizadorService.alterarCubo(novaAltura, novaLargura, novaProfundidade, this.imagemTexturaListar, this.materialItemEditar, this.acabamentoMaterialEditar, this.itemEditar.produto, itFAux);
    });
    return;
  }

  converteJsonEmItemEditar(item, cont): Item {
    let it = new Item(), i;
    it.altura = item.altura;
    it.largura = item.largura;
    it.profundidade = item.profundidade;
    it.idProduto = item.idProduto;
    it.idMaterial = item.idMaterial;
    it.idAcabamento = item.idAcabamento;
    it.posicaoY = it.altura / 2;
    this.materialService.buscarMaterialPorId(item.idMaterial).subscribe(
      materialRecebido => {
        it.material = materialRecebido;
        it.urlMaterial = this.texturasListar[materialRecebido.texturaImagem].urlImagem;
        this.acabamentoService.buscarAcabamentoPorId(it.idAcabamento).subscribe(
          acabamentoRecebido => {
            it.acabamento = acabamentoRecebido;

          }
        );
      }
    );
    it.itensFilhos = [];
    if (typeof item.itensFilhos !== 'undefined')
      for (i = 0; i < item.itensFilhos.length; i++)
        it.itensFilhos.push(this.converteJsonEmItem(item.itensFilhos[i], cont));

    return it;
  }

  atualizarCuboListar() {
    this.visualizadorService.apagaCubo();
    let itFAux = [], i, cont = 0;
    for (i = 0; i < this.itemListar.itensFilhos.length; i++) {
      cont = cont + 1;
      let j;
      for (j = 0; j < this.listaItens.length; j++) {
        if (this.listaItens[j]._id == this.itemListar.itensFilhos[i]._id) {
          itFAux.push(this.converteJsonEmItem(this.listaItens[j], cont));
          break;
        }
      }
    }
    this.itemListar.urlMaterial = this.texturasListar[this.itemListar.material.texturaImagem].urlImagem;
    this.visualizadorService.alterarCubo(this.itemListar.altura, this.itemListar.largura, this.itemListar.profundidade, this.itemListar.urlMaterial, this.itemListar.material, this.itemListar.acabamento, this.itemListar.produto, itFAux);

  }

  atualizarCuboApagar() {
    this.visualizadorService.apagaCubo();
    let itFAux = [], i, cont = 0;
    for (i = 0; i < this.itemApagar.itensFilhos.length; i++) {
      cont = cont + 1;
      let j;
      for (j = 0; j < this.listaItens.length; j++) {
        if (this.listaItens[j]._id == this.itemApagar.itensFilhos[i]._id) {
          itFAux.push(this.converteJsonEmItem(this.listaItens[j], cont));
          break;
        }
      }
    }
    this.itemApagar.urlMaterial = this.texturasListar[this.itemApagar.material.texturaImagem].urlImagem;
    this.visualizadorService.alterarCubo(this.itemApagar.altura, this.itemApagar.largura, this.itemApagar.profundidade, this.itemApagar.urlMaterial, this.itemApagar.material, this.itemApagar.acabamento, this.itemApagar.produto, itFAux);

  }

  converteJsonEmItemListagem(item, cont): Item {
    let it = new Item(), i;
    it.altura = item.altura;
    it.largura = item.largura;
    it.profundidade = item.profundidade;
    it.idProduto = item.idProduto;
    it.idMaterial = item.idMaterial;
    it.idAcabamento = item.idAcabamento;
    it.posicaoY = it.altura / 2;
    this.materialService.buscarMaterialPorId(item.idMaterial).subscribe(
      materialRecebido => {
        it.material = materialRecebido;
        it.urlMaterial = this.texturasListar[materialRecebido.texturaImagem].urlImagem;
        this.acabamentoService.buscarAcabamentoPorId(it.idAcabamento).subscribe(
          acabamentoRecebido => {
            it.acabamento = acabamentoRecebido;
          }
        );
      }
    );
    it.itensFilhos = [];
    if (typeof item.itensFilhos !== 'undefined')
      for (i = 0; i < item.itensFilhos.length; i++)
        it.itensFilhos.push(this.converteJsonEmItem(item.itensFilhos[i], cont));

    return it;
  }

  buscarItensNaoEncomendados() {
    this.itemService.buscarItensNaoEncomendados().subscribe(itens => {
      this.listaItensNaoEncomendados = itens;
    });
  }

  buscarCatalogos() {
    this.catalogoService.buscarCatalogos().subscribe(
      catalogos => {
        this.listaCatalogos = catalogos;
      }
    )
  }

  buscarItens() {
    this.itemService.buscarItens().subscribe(itens => {

      this.listaItens = itens;
      console.log(this.listaItens);
    });
  }

  visualizarItemCriar() {
    this.mostrarVisualizador = true;
    this.previsualizacao = true;
    this.esconderDiv = false;
  }

  pararVisualizarItemCriar() {
    this.mostrarVisualizador = false;
    this.previsualizacao = false;
    this.esconderDiv = true;
  }

  limparDados() {
    this.podeTerFilhos = false;
    this.listaItemsFilho = [];
    this.itemApagar = null;
    this.itemEditar = null;
    this.itemProduto = null;
    this.materialItem = null;
    this.acabamentoMaterial = null;
    this.listaProdutosFilho = []; // lista auxiliar
    this.listaItensFilhoAux = []; // lista auxiliar
    this.itemsFilho = [];
    this.itemProcurar = null;
    this.tooltipLargura = "";
    this.tooltipLarguraEditar = "";
    this.tooltipAltura = "";
    this.tooltipAlturaEditar = "";
    this.tooltipProfundidade = "";
    this.tooltipProfundidadeEditar = "";
    this.tooltipItensFilhosObrigatorios = "";
    this.tooltipItensFilhosObrigatoriosEditar = "";
    this.tooltipItensCriar = "";
    this.tooltipItensEditar = "";
    this.larguraItem = null;
    this.alturaItem = null;
    this.profundidadeItem = null;
    this.catalogoSelecionadoCriar = null;
    this.larguraItemEditar = null
    this.alturaItemEditar = null
    this.profundidadeItemEditar = null


    this.colecaoSelecionadaCriar = null;
    this.listaMateriais = [];
    this.listaAcabamentos = [];

    this.materialItemEditar = null;
    this.acabamentoMaterialEditar = null;

    this.colorLuzCriar = "#FFFFFF";
    this.valorBrilhoLuzCriar = 0;
    this.luzLigada = false;
    this.luzDesligada = true;
    this.ngAfterViewInit();
  }



  mostrarProdutosCatalogo() {
    if (this.catalogoSelecionadoCriar == null) {
      this.produtoService.buscarProdutos().subscribe(
        todosProdutos => {
          this.listaProdutos = todosProdutos;
        }
      );
    } else {
      this.produtoService.buscarProdutosPorCatalogo(this.catalogoSelecionadoCriar.catalogoId).subscribe(
        produtosCatalogo => {
          this.listaProdutos = produtosCatalogo;
        }
      );
      this.colecaoService.buscarColecoesDeCatalogo(this.catalogoSelecionadoCriar.catalogoId).subscribe(
        colecoesCatalogo => {
          this.listaColecoes = colecoesCatalogo;
        }
      );
    }
  }

  mostrarProdutosColecao() {
    if (this.colecaoSelecionadaCriar == null) {
      this.mostrarProdutosCatalogo();
      return;
    }
    this.produtoService.buscarProdutosPorColecao(this.colecaoSelecionadaCriar.id).subscribe(
      produtosColecao => {
        this.listaProdutos = produtosColecao;
      }
    );
  }

  atualizarDadosItem() {
    this.produtoService.buscarProdutoPorId(this.itemEditar.idProduto).subscribe(
      produtoRecebido => {
        this.categoriaService.podeTerFilhos(produtoRecebido.categoria.id).subscribe(
          podeTerFilhos => {
            this.podeTerFilhos = podeTerFilhos;
            if (!podeTerFilhos) {
              this.listaItemsFilho = [];
            }

            if (this.itemEditar == null) return;
            this.atualizarToolTipsEditar();
            this.materialService.buscarMateriaisDeProduto(this.itemEditar.idProduto).subscribe(
              materiaisProduto => {
                this.listaMateriais = materiaisProduto;
                this.materialService.buscarMaterialPorId(this.itemEditar.idMaterial).subscribe(
                  materialRecebido => {

                    this.materialItemEditar = materialRecebido;

                    this.acabamentoService.buscarAcabamentoDeMaterial(this.itemEditar.idMaterial).subscribe(
                      acabamentosMaterial => {
                        this.listaAcabamentos = acabamentosMaterial;
                        this.acabamentoService.buscarAcabamentoPorId(this.itemEditar.idAcabamento).subscribe(
                          acabamentoRecebido => {
                            this.acabamentoMaterialEditar = acabamentoRecebido;

                            (<HTMLInputElement>document.getElementById("alturaItem")).value = String(this.itemEditar.altura);
                            this.alturaItemEditar = this.itemEditar.altura;
                            (<HTMLInputElement>document.getElementById("larguraItem")).value = String(this.itemEditar.largura);
                            this.larguraItemEditar = this.itemEditar.largura;
                            (<HTMLInputElement>document.getElementById("profundidadeItem")).value = String(this.itemEditar.profundidade);
                            this.profundidadeItemEditar = this.itemEditar.profundidade;
                            this.itemsFilho = this.itemEditar.itensFilhos;

                            this.listaItemsFilho = [];
                            this.produtoService.buscarProdutosFilho(this.itemEditar.idProduto).subscribe(
                              produtosFilhos => {
                                produtosFilhos.forEach(produtoAtual => {
                                  this.itemService.itensProduto(produtoAtual.produtoId).subscribe(
                                    itensDaqueleProduto => {
                                      itensDaqueleProduto.forEach(itemAtual => {
                                        // let naoEncomendado = false; //isto era no caso de apenas mostrar itens que não estejam em encomendas
                                        // for (let i = 0; i < this.listaItensNaoEncomendados.length; i++) {
                                        //   if (this.listaItensNaoEncomendados[i].idItem == itemAtual.idItem) {
                                        //     naoEncomendado = true;
                                        //     break;
                                        //   }
                                        // }
                                        // if (naoEncomendado) {
                                        //   this.listaItemsFilho.push(itemAtual);
                                        // }
                                        this.listaItemsFilho.push(itemAtual);
                                        //his.atualizarCuboEditar()
                                      });
                                    }
                                  );
                                });
                              }
                            );
                            this.atualizarCuboEditar()
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
    //setTimeout(this.atualizarCuboEditar(), 5000);
    //setTimeout(function(){ this.atualizarCuboEditar(); }, 1000);
    //Observable.interval(1000).subscribe(() => );
  }


  atualizarAcabamentosMaterialEditar() {
    this.acabamentoMaterialEditar = null;
    this.acabamentoService.buscarAcabamentoDeMaterial(this.materialItemEditar.id).subscribe(
      acabamentosMaterial => {
        this.listaAcabamentos = acabamentosMaterial;
      }
    );
  }

  atualizarToolTipsEditar() {

    this.tooltipItensEditar = "";
    this.produtoService.buscarProdutosFilho(this.itemEditar.idProduto).subscribe(
      produtosFilhos => {
        if (produtosFilhos.length == 0) {
          this.tooltipItensEditar = "Este item não tem produtos filhos (agregados)."
        } else {
          this.tooltipItensEditar += 'Este item aceita como filho os seguintes itens: '
          for (let i = 0; i < produtosFilhos.length; i++) {
            if (i == produtosFilhos.length - 1) {
              this.tooltipItensEditar += produtosFilhos[i].nome.conteudo;
            } else {
              this.tooltipItensEditar += produtosFilhos[i].nome.conteudo + ', ';
            }
          }
        }
      }
    );
    this.produtoService.buscarProdutoPorId(this.itemEditar.idProduto).subscribe(produtoDoItem => {
      // há sempre pelo menos um valor portanto é seguro ir à posição 0
      this.tooltipAlturaEditar = "";
      this.tooltipLarguraEditar = "";
      this.tooltipProfundidadeEditar = "";
      if (produtoDoItem.dimensoes.altura[0].dimensao == null) { //se for contínua
        this.tooltipAlturaEditar += "Apenas são aceites valores contínuos no intervalo de: "
        produtoDoItem.dimensoes.altura.forEach(alturaAtual => {
          this.tooltipAlturaEditar += '[' + alturaAtual.dimensaoMin + '-' + alturaAtual.dimensaoMax + '] ';
        });
      } else { //se for discreta
        this.tooltipAlturaEditar += 'Apenas são aceites os seguintes valores discretos: ';
        produtoDoItem.dimensoes.altura.forEach(alturaAtual => {
          this.tooltipAlturaEditar += alturaAtual.dimensao + ' ';
        });
      }
      if (produtoDoItem.dimensoes.largura[0].dimensao == null) { //se for contínua
        this.tooltipLarguraEditar += "Apenas são aceites valores contínuos no intervalo de: "
        produtoDoItem.dimensoes.largura.forEach(alturaAtual => {
          this.tooltipLarguraEditar += '[' + alturaAtual.dimensaoMin + '-' + alturaAtual.dimensaoMax + '] ';
        });
      } else { //se for discreta
        this.tooltipLarguraEditar += 'Apenas são aceites os seguintes valores discretos: ';
        produtoDoItem.dimensoes.largura.forEach(alturaAtual => {
          this.tooltipLarguraEditar += alturaAtual.dimensao + ' ';
        });
      }
      if (produtoDoItem.dimensoes.profundidade[0].dimensao == null) { //se for contínua
        this.tooltipProfundidadeEditar += "Apenas são aceites valores contínuos no intervalo de: "
        produtoDoItem.dimensoes.profundidade.forEach(alturaAtual => {
          this.tooltipProfundidadeEditar += '[' + alturaAtual.dimensaoMin + '-' + alturaAtual.dimensaoMax + '] ';
        });
      } else { //se for discreta
        this.tooltipProfundidadeEditar += 'Apenas são aceites os seguintes valores discretos: ';
        produtoDoItem.dimensoes.profundidade.forEach(alturaAtual => {
          this.tooltipProfundidadeEditar += alturaAtual.dimensao + ' ';
        });
      }
    });
    this.produtoService.produtosObrigatorios(this.itemEditar.idProduto).subscribe(
      produtosRecebidos => {
        if (produtosRecebidos.length == 0) {
          this.tooltipItensFilhosObrigatoriosEditar = 'Este item não tem filhos obrigatórios.';
        } else {
          this.tooltipItensFilhosObrigatoriosEditar = 'Este item tem como obrigatórios os seguintes itens:'
          for (let i = 0; i < produtosRecebidos.length; i++) {
            if (i == produtosRecebidos.length - 1) {
              this.tooltipItensFilhosObrigatoriosEditar += produtosRecebidos[i].nome.conteudo;
            } else {
              this.tooltipItensFilhosObrigatoriosEditar += produtosRecebidos[i].nome.conteudo + ', ';
            }
          }
        }
      }
    );
  }

  listarItem() {
    this.itensFilhos = [];
    this.materialService.buscarMaterialPorId(this.itemListar.idMaterial).subscribe(material => {
      this.nomeTexturaListar = this.texturasListar[material.texturaImagem].nome;
      this.imagemTexturaListar = this.texturasListar[material.texturaImagem].urlImagem;
      console.log(this.nomeTexturaListar)
      console.log(this.imagemTexturaListar)
    })
    for (let i = 0; i < this.itemListar.itensFilhos.length; i++) {
      this.produtoService.buscarProdutoPorId(this.itemListar.itensFilhos[i].idProduto).subscribe(produto => {
        this.itensFilhos.push(produto.nome.conteudo);
      })
    }
  }

  atualizarItensFilhoEMateriais() {
    this.categoriaService.podeTerFilhos(this.itemProduto.categoria.id).subscribe(
      podeTerFilhos => {
        console.log(podeTerFilhos);
        this.podeTerFilhos = podeTerFilhos;
        if (!podeTerFilhos) {
          this.listaItemsFilho = [];
        }
      }
    )
    this.itemsFilho = [];
    this.materialItem = null;
    this.acabamentoMaterial = null;
    this.atualizarMateriaisProduto();
    this.atualizarItensFilho();
    this.atualizarToolTips();
  }

  atualizarToolTips() {
    this.tooltipItensCriar = "";
    this.produtoService.buscarProdutosFilho(this.itemProduto.produtoId).subscribe(
      produtosFilhos => {
        if (produtosFilhos.length == 0) {
          this.tooltipItensCriar = "Este produto não tem produtos filhos (agregados)."
        } else {
          this.tooltipItensCriar += 'Este produto aceita como filho os seguintes produtos: '
          for (let i = 0; i < produtosFilhos.length; i++) {
            if (i == produtosFilhos.length - 1) {
              this.tooltipItensCriar += produtosFilhos[i].nome.conteudo;
            } else {
              this.tooltipItensCriar += produtosFilhos[i].nome.conteudo + ', ';
            }
          }
        }
      }
    );
    // há sempre pelo menos um valor portanto é seguro ir à posição 0
    this.tooltipAltura = "";
    this.tooltipLargura = "";
    this.tooltipProfundidade = "";
    if (this.itemProduto.dimensoes.altura[0].dimensao == null) { //se for contínua
      this.tooltipAltura += "Apenas são aceites valores contínuos no intervalo de: "
      this.itemProduto.dimensoes.altura.forEach(alturaAtual => {
        this.tooltipAltura += '[' + alturaAtual.dimensaoMin + '-' + alturaAtual.dimensaoMax + '] ';
      });
    } else { //se for discreta
      this.tooltipAltura += 'Apenas são aceites os seguintes valores discretos: ';
      this.itemProduto.dimensoes.altura.forEach(alturaAtual => {
        this.tooltipAltura += alturaAtual.dimensao + ' ';
      });
    }
    if (this.itemProduto.dimensoes.largura[0].dimensao == null) { //se for contínua
      this.tooltipLargura += "Apenas são aceites valores contínuos no intervalo de: "
      this.itemProduto.dimensoes.largura.forEach(alturaAtual => {
        this.tooltipLargura += '[' + alturaAtual.dimensaoMin + '-' + alturaAtual.dimensaoMax + '] ';
      });
    } else { //se for discreta
      this.tooltipLargura += 'Apenas são aceites os seguintes valores discretos: ';
      this.itemProduto.dimensoes.largura.forEach(alturaAtual => {
        this.tooltipLargura += alturaAtual.dimensao + ' ';
      });
    }
    if (this.itemProduto.dimensoes.profundidade[0].dimensao == null) { //se for contínua
      this.tooltipProfundidade += "Apenas são aceites valores contínuos no intervalo de: "
      this.itemProduto.dimensoes.profundidade.forEach(alturaAtual => {
        this.tooltipProfundidade += '[' + alturaAtual.dimensaoMin + '-' + alturaAtual.dimensaoMax + '] ';
      });
    } else { //se for discreta
      this.tooltipProfundidade += 'Apenas são aceites os seguintes valores discretos: ';
      this.itemProduto.dimensoes.profundidade.forEach(alturaAtual => {
        this.tooltipProfundidade += alturaAtual.dimensao + ' ';
      });
    }

    this.produtoService.produtosObrigatorios(this.itemProduto.produtoId).subscribe(
      produtosRecebidos => {
        if (produtosRecebidos.length == 0) {
          this.tooltipItensFilhosObrigatorios = 'Este produto não tem filhos obrigatórios.';
        } else {
          this.tooltipItensFilhosObrigatorios = 'Este produto tem como obrigatórios os seguintes produtos:'
          for (let i = 0; i < produtosRecebidos.length; i++) {
            if (i == produtosRecebidos.length - 1) {
              this.tooltipItensFilhosObrigatorios += produtosRecebidos[i].nome.conteudo;
            } else {
              this.tooltipItensFilhosObrigatorios += produtosRecebidos[i].nome.conteudo + ', ';
            }
          }
        }
      }
    );

  }

  atualizarMateriaisProduto() {
    let listaMateriaisAux = []
    this.materialService.buscarMateriais().subscribe(materiais => {
      listaMateriaisAux = materiais;

      this.listaMateriais = [];
      let count = 0;
      for (let i = 0; i < listaMateriaisAux.length; i++) {
        for (let j = 0; j < this.itemProduto.materiais.length; j++) {
          if (this.itemProduto.materiais[j].idMaterial == listaMateriaisAux[i].id) {
            this.listaMateriais[count] = listaMateriaisAux[i];
            count++;
          }
        }
      }

      this.listaAcabamentos = [];
    });
  }

  atualizarAcabamentosMaterial() {
    let listaAcabamentosAux = [];
    this.acabamentoMaterial = null;
    this.acabamentoService.buscarAcabamentos().subscribe(acabamentos => {
      listaAcabamentosAux = acabamentos;

      this.listaAcabamentos = [];
      let count = 0;

      for (let i = 0; i < listaAcabamentosAux.length; i++) {
        if (this.materialItem.acabamentos != null) {
          for (let j = 0; j < this.materialItem.acabamentos.length; j++) {
            if (listaAcabamentosAux[i].id == this.materialItem.acabamentos[j].idAcabamento) {
              this.listaAcabamentos[count] = listaAcabamentosAux[i];
              count++;
            }
          }
        } else {

        }

      }
    });
  }

  atualizarItensFilho() {
    if (this.itemProduto == null) {
      alert('Por favor selecione um produto');
      return;
    }

    let string = '';
    this.produtoService.buscarProdutosFilho(this.itemProduto.produtoId).subscribe(produtos => {
      this.listaProdutosFilho = produtos;  // produtos Filho de um produto
      this.itemService.buscarItens().subscribe(itens => {
        this.listaItensFilhoAux = itens;   // todos os itens

        this.listaItemsFilho = [];
        let count = 0;
        // compara as duas listas, a ver quais são os itens que têm o id de um produto da lista de produtos Filho
        for (let i = 0; i < this.listaItensFilhoAux.length; i++) {
          for (let j = 0; j < this.listaProdutosFilho.length; j++) {
            if (this.listaItensFilhoAux[i].idProduto == this.listaProdutosFilho[j].produtoId) {

              this.listaItemsFilho.push(this.listaItensFilhoAux[i]);
              count++;
            }
          }
        }

        for (let i = 0; i < this.listaProdutosFilho.length; i++) {
          if (i == this.listaProdutosFilho.length - 1) {
            string += this.listaProdutosFilho[i].nome.conteudo;
          } else {
            string += this.listaProdutosFilho[i].nome.conteudo + ", ";
          }

        }

        // document.getElementById("produtosObrigatorios").innerHTML = string;
      });
    });
  }

  eliminarItem() {
    if (this.itemApagar == null) {
      alert('Por favor selecione um item');
      return;
    }
    this.itemService.eliminarItem(this.itemApagar.idItem).subscribe(_ => {
      this.buscarItens();
      this.buscarItensNaoEncomendados();
      this.itemApagar = null;
    });
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne._id === objTwo._id;
    }
  }

  equalsMaterial(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  equalsItem(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.idItem === objTwo.idItem;
    }
  }

  estadoCriarItem() {
    this.mostrarCriarItem = !this.mostrarCriarItem;
    this.mostrarApagarItem = false;
    this.mostrarListarItens = false;
    this.mostrarEditarItem = false;
    this.mostrarProcurarItem = false;
    this.pararVisualizarItemCriar();
    this.limparDados();
  }

  estadoListarItem() {
    this.limparDados();
    this.mostrarListarItens = !this.mostrarListarItens;
    this.mostrarApagarItem = false;
    this.mostrarCriarItem = false;
    this.mostrarEditarItem = false;
    this.mostrarProcurarItem = false;
    this.pararVisualizarItemCriar();
  }

  estadoProcurarItem() {
    this.limparDados();
    this.mostrarProcurarItem = !this.mostrarProcurarItem;
    this.mostrarApagarItem = false;
    this.mostrarCriarItem = false;
    this.mostrarEditarItem = false;
    this.mostrarListarItens = false;
    this.pararVisualizarItemCriar();
  }

  estadoEliminarItem() {
    this.limparDados();
    this.mostrarApagarItem = !this.mostrarApagarItem;
    this.mostrarProcurarItem = false;
    this.mostrarCriarItem = false;
    this.mostrarEditarItem = false;
    this.mostrarListarItens = false;
    this.pararVisualizarItemCriar();
  }

  estadoEditarItem() {
    this.limparDados();
    this.mostrarEditarItem = !this.mostrarEditarItem;
    this.mostrarApagarItem = false;
    this.mostrarProcurarItem = false;
    this.mostrarCriarItem = false;
    this.mostrarListarItens = false; ~
      this.pararVisualizarItemCriar();
  }

  criarItem() {


    this.tratarItem(1);
  }

  editarItem() {
    this.tratarItem(2);
  }

  tratarItem(opcao) {

    let altura = (<HTMLInputElement>document.getElementById("alturaItem")).value;
    let largura = (<HTMLInputElement>document.getElementById("larguraItem")).value;
    let profundidade = (<HTMLInputElement>document.getElementById("profundidadeItem")).value;

    if (isNaN(Number(altura)) || isNaN(Number(largura)) || isNaN(Number(profundidade))) {
      alert('Dimensões inseridas são inválidas!');
      return;
    }

    // if (!this.verificarDimensoes(altura, largura, profundidade)) {
    //   alert('Dimensões inseridas não correspondem com as especificações do produto escolhido!');
    //   return;
    // }



    if (this.materialItem == null && this.materialItemEditar == null) {
      alert('Material em falta!');
      return;
    }

    if (this.acabamentoMaterial == null && this.acabamentoMaterialEditar == null) {
      alert('Acabamento em falta!');
      return;
    }

    let listaFilhosFinal = [];
    for (let i = 0; i < this.itemsFilho.length; i++) {
      listaFilhosFinal[i] = this.itemsFilho[i].idItem;
    }

    if (opcao == 1) {
      if (confirm('O preço deste item ao ser encomendado será de ' + GeraModelo.precoTotal + '€ pretende efetuar a criação?')) {
        this.itemService.adicionarItem({
          id_produto: this.itemProduto.produtoId,
          id_material: this.materialItem.id,
          id_acabamento: this.acabamentoMaterial.id,
          Altura: Number(altura),
          ItensFilhos: listaFilhosFinal,
          Largura: Number(largura),
          Profundidade: Number(profundidade),
          Preco: GeraModelo.precoTotal
        } as Item).subscribe(_ => {
          this.itemProduto = null;
          this.listaAcabamentos = [];
          this.listaMateriais = [];
          this.buscarItens();
          this.buscarItensNaoEncomendados();
        });
      } else {
        this.acabamentoMaterial = null;
        this.materialItem = null;
        this.alturaItem = null;
        this.profundidadeItem = null;
        this.larguraItem = null;
        this.atualizarCubo();
      }
    } else {
      if (confirm('O preço deste item ao ser encomendado será de ' + GeraModelo.precoTotal + '€ pretende efetuar a edição?')) {
        this.itemService.atualizarItem({
          _id: this.itemEditar.idItem,
          id_produto: this.itemEditar.idProduto,
          id_material: this.materialItemEditar.id,
          id_acabamento: this.acabamentoMaterialEditar.id,
          Altura: Number(altura),
          ItensFilhos: listaFilhosFinal,
          Largura: Number(largura),
          Profundidade: Number(profundidade),
          Preco: GeraModelo.precoTotal
        } as Item).subscribe(_ => {
          this.itemEditar = null;
          this.materialItemEditar = null;
          this.acabamentoMaterialEditar = null;
          this.listaAcabamentos = [];
          this.listaMateriais = [];
          this.buscarItens();
          this.buscarItensNaoEncomendados();
        });
      } else {
        this.acabamentoMaterialEditar = null;
        this.materialItemEditar = null;
        this.alturaItemEditar = null;
        this.profundidadeItemEditar = null;
        this.larguraItemEditar = null;
        this.atualizarCubo();
      }

    }
    this.itemsFilho = [];
    this.materialItem = null;
    this.acabamentoMaterial = null;
    (<HTMLInputElement>document.getElementById("alturaItem")).value = '';
    (<HTMLInputElement>document.getElementById("larguraItem")).value = '';
    (<HTMLInputElement>document.getElementById("profundidadeItem")).value = '';
  }
  //PRECISA SER ATUALIZADO
  // verificarDimensoes(altura, largura, profundidade) {
  //   if (this.itemProduto == null) return false;
  //   if (this.itemProduto.dimensoes == null) return false;

  //   if (this.itemProduto.dimensoes.altura == null) return false;
  //   if (this.itemProduto.dimensoes.largura == null) return false;
  //   if (this.itemProduto.dimensoes.profundidade == null) return false;
  //   for (let i = 0; i < this.itemProduto.dimensoes.altura.length; i++) {
  //     if (this.itemProduto.dimensoes.altura[i].dimensao != null) {
  //       if (this.itemProduto.dimensoes.altura[i].dimensao> 0) {
  //         if (this.itemProduto.dimensoes.altura.indexOf(altura) == -1) return false;
  //       }
  //     } else {
  //       if (this.itemProduto.dimensoes.altura.min == null) return false;
  //       if (this.itemProduto.dimensoes.altura.max == null) return false;
  //       if (altura < this.itemProduto.dimensoes.altura.min) return false;
  //       if (altura > this.itemProduto.dimensoes.altura.max) return false;
  //     }
  //   }

  //   if (this.itemProduto.dimensoes.largura.discreta != null) {
  //     if (this.itemProduto.dimensoes.largura.discreta.length > 0) {
  //       if (this.itemProduto.dimensoes.largura.discreta.indexOf(altura) == -1) return false;
  //     }
  //   } else {
  //     if (this.itemProduto.dimensoes.largura.min == null) return false;
  //     if (this.itemProduto.dimensoes.largura.max == null) return false;
  //     if (largura < this.itemProduto.dimensoes.largura.min) return false;
  //     if (largura > this.itemProduto.dimensoes.largura.max) return false;
  //   }

  //   if (this.itemProduto.dimensoes.profundidade.discreta != null) {
  //     if (this.itemProduto.dimensoes.profundidade.discreta.length > 0) {
  //       if (this.itemProduto.dimensoes.profundidade.discreta.indexOf(altura) == -1) return false;
  //     }
  //   } else {
  //     if (this.itemProduto.dimensoes.profundidade.min == null) return false;
  //     if (this.itemProduto.dimensoes.profundidade.max == null) return false;
  //     if (profundidade < this.itemProduto.dimensoes.profundidade.min) return false;
  //     if (profundidade > this.itemProduto.dimensoes.profundidade.max) return false;
  //   }

  //   return true;
  // }

  procurarInfoItem() {
    if (this.itemProcurar == null) {
      alert('Por favor selecione um item válido!');
      return;
    }

    document.getElementById("nomePesquisado").innerHTML = this.itemProcurar.nomeProduto;
    document.getElementById("materiaisPesquisado").innerHTML = this.itemProcurar.nomeMaterial;
    document.getElementById("acabamentosPesquisado").innerHTML = this.itemProcurar.nomeAcabamento;

    document.getElementById("alturaPesquisado").innerHTML = String(this.itemProcurar.altura);
    document.getElementById("larguraPesquisado").innerHTML = String(this.itemProcurar.largura);
    document.getElementById("profundidadePesquisado").innerHTML = String(this.itemProcurar.profundidade);

    let itensF = '';
    for (let i = 0; i < this.itemProcurar.itensFilhos.length; i++) {
      for (let j = 0; j < this.listaItens.length; j++) {
        if (this.itemProcurar.itensFilhos[i].idItem == this.listaItens[j].idItem) {
          itensF += this.listaItens[j].nomeProduto;
          if (i < length - 1) {
            itensF += ', ';
          }
        }
      }
    }
    document.getElementById("agregadosObrPesquisado").innerHTML = itensF;
  }
}
