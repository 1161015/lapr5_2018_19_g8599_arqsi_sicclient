import { TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';

import { EncomendaService } from './services/encomenda.service';

describe('EncomendaService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
  ],
  }));

  // it('should be created', () => {
  //   const service: EncomendaService = TestBed.get(EncomendaService);
  //   expect(service).toBeTruthy();
  // });
});
