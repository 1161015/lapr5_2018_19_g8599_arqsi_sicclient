import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialComponent } from './material.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule, MatTabsModule, MatIconModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { MatRadioModule } from '@angular/material/radio';
import { ColorPickerModule } from 'ngx-color-picker';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('MaterialComponent', () => {
  let component: MaterialComponent;
  let fixture: ComponentFixture<MaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MaterialComponent],
      imports: [MatFormFieldModule, MatIconModule, FormsModule, MatSelectModule, MatInputModule, MatCardModule, MatTabsModule, MatSliderModule, MatRadioModule, ColorPickerModule, HttpClientTestingModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#estadoCriarMaterial() should toggle #materialHidden01', () => {
    expect(component.materialHidden01).toBe(false, "off at first");
    component.estadoCriarMaterial();
    expect(component.materialHidden01).toBe(true, "on after pressed");
    component.estadoCriarMaterial();
    expect(component.materialHidden01).toBe(false, "off after pressed again");
  });

  it('#estadoListarMateriais() should toggle #materialHidden02', () => {
    expect(component.materialHidden02).toBe(false, "off at first");
    component.estadoListarMateriais();
    expect(component.materialHidden02).toBe(true, "on after pressed");
    component.estadoListarMateriais();
    expect(component.materialHidden02).toBe(false, "off after pressed again");
  });

  it('#estadoEliminarMaterial() should toggle #materialHidden04', () => {
    expect(component.materialHidden04).toBe(false, "off at first");
    component.estadoEliminarMaterial();
    expect(component.materialHidden04).toBe(true, "on after pressed");
    component.estadoEliminarMaterial();
    expect(component.materialHidden04).toBe(false, "off after pressed again");
  });

  it('#estadoEditarItem() should toggle #materialHidden05', () => {
    expect(component.materialHidden05).toBe(false, "off at first");
    component.estadoEditarMaterial();
    expect(component.materialHidden05).toBe(true, "on after pressed");
    component.estadoEditarMaterial();
    expect(component.materialHidden05).toBe(false, "off after pressed again");
  });
});
