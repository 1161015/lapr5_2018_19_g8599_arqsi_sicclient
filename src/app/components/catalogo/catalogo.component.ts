import { Component, OnInit } from '@angular/core';
import { Catalogo } from 'src/app/models/catalogo';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Produto } from 'src/app/models/produto';
import { ProdutoService } from 'src/app/services/produto.service';
import { CategoriaService } from 'src/app/services/categoria.service';
import { Categoria } from 'src/app/models/categoria';
import { Material } from 'src/app/models/material';
import { MaterialService } from 'src/app/services/material.service';
import { MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  catalogos: Catalogo[] = [];

  listaProdutos: Produto[] = [];
  listaCategorias: Categoria[] = [];
  listaMateriais: Material[] = [];
  produtoCatalogo: Produto[] = [];
  catalogoEliminar: Catalogo;
  catalogoEditar: Catalogo;
  produtoCatalogoEditar: Produto[] = [];
  imagemTexturaListar: string[] = [];
  texturasListar: any[] = [];
  nomeTexturaListar: string[] = [];

  catalogoListar: Catalogo;
  nomePesquisado: string = '';
  categoriaPesquisado: string = '';
  materiaisPesquisado: string = '';
  agregadosObrPesquisado: string = '';
  agregadosOpPesquisado: string = '';
  minPesquisado: string = '';
  maxPesquisado: string = '';
  restricaoPesquisado: string = '';
  alturaPesquisado: string = '';
  larguraPesquisado: string = '';
  profundidadePesquisado: string = '';
  catalogoEditarNome: string = '';

  /**
   * variáveis usadas para as listagens
   */
  produtosListar: Produto[];
  produtoListar: Produto = null;
  materiaisProduto: Material[] = [];
  alturasProduto: number[] = [];
  alturasMinProduto: number[] = [];
  alturasMaxProduto: number[] = [];
  largurasMinProduto: number[] = [];
  largurasMaxProduto: number[] = [];
  largurasProduto: number[] = [];
  profundidadesProduto: number[] = [];
  profundidadesMinProduto: number[] = [];
  profundidadesMaxProduto: number[] = [];
  produtosAgregados: string[] = [];

  catalogoHidden01: boolean = false;
  catalogoHidden02: boolean = false;
  catalogoHidden03: boolean = false;
  catalogoHidden04: boolean = false;
  catalogoHidden05: boolean = false;
  catalogoHidden06: boolean = true;


  constructor(private catalogoService: CatalogoService, private produtoService: ProdutoService,
    private categoriaService: CategoriaService, private materialService: MaterialService) { }

  ngOnInit() {
    this.buscarProdutos();
    this.buscarCatalogos();
    this.buscarCategorias();
    this.buscarMateriais();
    this.texturasListar.push({
      nome: 'Wenge 1',
      urlImagem: '../../../assets/materiais/madeira1.png'
    });
    this.texturasListar.push({
      nome: 'Wenge 2',
      urlImagem: '../../../assets/materiais/madeira2.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 1',
      urlImagem: '../../../assets/materiais/madeira3.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 2',
      urlImagem: '../../../assets/materiais/madeira4.jpg'
    });
    this.texturasListar.push({
      nome: 'Madeira 5',
      urlImagem: '../../../assets/materiais/madeira5.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 1',
      urlImagem: '../../../assets/materiais/vidro1.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 2',
      urlImagem: '../../../assets/materiais/vidro2.jpg'
    });
  }

  buscarCategorias(): any {
    this.categoriaService.buscarCategorias().subscribe(categorias => {
      this.listaCategorias = categorias;
    });
  }

  buscarMateriais(): any {
    this.materialService.buscarMateriais().subscribe(materiais => {
      this.listaMateriais = materiais;
    });
  }

  buscarProdutos(): any {
    this.produtoService.buscarProdutos().subscribe(produtos => {
      this.listaProdutos = produtos;
    });
  }

  buscarCatalogos() {
    this.catalogoService.buscarCatalogos().subscribe(catalogos => {
      this.catalogos = catalogos;
    });
  }

  criarCatalogo(nomeCatalogo: string): void {
    nomeCatalogo = nomeCatalogo.trim();
    if (!nomeCatalogo || nomeCatalogo.length < 2) {
      alert("Certifique-se que introduziu um nome válido (3 caractéres mínimo).");
      return;
    }
    let idProdutos = [];
    if (this.produtoCatalogo.length != 0) {
      for (let i = 0; i < this.produtoCatalogo.length; i++) {
        idProdutos[i] = { produtoId: this.produtoCatalogo[i].produtoId }
      }
    }
    this.catalogoService.criarCatalogo({
      nome: {
        conteudo: nomeCatalogo
      },
      produtoCatalogos: idProdutos
    } as Catalogo).subscribe(catalogo => { this.buscarCatalogos(); });
  }

  atualizarCatalogo(): void {
    if (this.catalogoEditarNome == null) {
      alert('Introduza um nome não vazio');
      return;
    }
    this.catalogoEditarNome = this.catalogoEditarNome.trim();
    if (!this.catalogoEditarNome || this.catalogoEditarNome.length < 2) {
      alert("Certifique-se que introduziu um nome válido (3 caractéres mínimo).");
      return;
    }
    let ProdutoId = [];
    if (this.produtoCatalogoEditar.length != 0) {
      for (let i = 0; i < this.produtoCatalogoEditar.length; i++) {
        ProdutoId[i] = { produtoId: this.produtoCatalogoEditar[i].produtoId }
      }
    }
    this.catalogoService.atualizarCatalogo({
      catalogoId: this.catalogoEditar.catalogoId,
      nome: {
        conteudo: this.catalogoEditarNome
      },
      produtoCatalogos: ProdutoId
    } as Catalogo).subscribe(_ => { this.buscarCatalogos() });
  }

  eliminarCatalogo() {
    if (this.catalogoEliminar == null) {
      alert('Selecione o catálogo que pretende eliminar');
      return;
    }
    this.catalogoService.eliminarCatalogo(this.catalogoEliminar.catalogoId).subscribe(
      _ => {
        this.buscarCatalogos();
        this.catalogoEliminar = null;
      }
    );

  }

  //Listar os Produtos do Catálogo
  listarProdutos() {
    if (this.catalogoListar != null) {
      this.produtoService.buscarProdutosPorCatalogo(this.catalogoListar.catalogoId).subscribe(
        produtosCatalogo => {
          this.produtosListar = produtosCatalogo;
        }
      )
    }
  }

  listarProduto(event: MatTabChangeEvent) {
    this.alturasProduto = []
    this.alturasMinProduto = [];
    this.alturasMaxProduto = [];
    this.largurasProduto = [];
    this.largurasMinProduto = [];
    this.largurasMaxProduto = [];
    this.profundidadesProduto = [];
    this.profundidadesMinProduto = [];
    this.profundidadesMaxProduto = [];
    this.produtosAgregados = [];
    this.nomeTexturaListar = [];
    this.imagemTexturaListar = [];

    for (let i = 0; i < this.catalogoListar.produtoCatalogos.length; i++) {
      if (i == event.index) {
        this.materialService.buscarMateriaisDeProduto(this.catalogoListar.produtoCatalogos[i].produtoId).subscribe(materiaisP => {
          this.materiaisProduto = materiaisP;
          for (let i = 0; i < materiaisP.length; i++) {
            this.nomeTexturaListar.push(this.texturasListar[materiaisP[i].texturaImagem].nome);
            this.imagemTexturaListar.push(this.texturasListar[materiaisP[i].texturaImagem].urlImagem);
          }
        });
        this.produtoService.buscarProdutoPorId(this.catalogoListar.produtoCatalogos[i].produtoId).subscribe(produto => {
          for (let i = 0; i < produto.dimensoes.altura.length; i++) {
            if (produto.dimensoes.altura[i].dimensaoMin != null) {
              this.alturasMinProduto.push(produto.dimensoes.altura[i].dimensaoMin);
            }
            if (produto.dimensoes.altura[i].dimensaoMax != null) {
              this.alturasMaxProduto.push(produto.dimensoes.altura[i].dimensaoMax);
            }
            if (produto.dimensoes.altura[i].dimensao != null) {
              this.alturasProduto.push(produto.dimensoes.altura[i].dimensao);
            }
          }
          for (let i = 0; i < produto.dimensoes.largura.length; i++) {
            if (produto.dimensoes.largura[i].dimensaoMin != null) {
              this.largurasMinProduto.push(produto.dimensoes.largura[i].dimensaoMin);
            }
            if (produto.dimensoes.largura[i].dimensaoMax != null) {
              this.largurasMaxProduto.push(produto.dimensoes.largura[i].dimensaoMax);
            }
            if (produto.dimensoes.largura[i].dimensao != null) {
              this.largurasProduto.push(produto.dimensoes.largura[i].dimensao);
            }
          }
          for (let i = 0; i < produto.dimensoes.profundidade.length; i++) {
            if (produto.dimensoes.profundidade[i].dimensaoMin != null) {
              this.profundidadesMinProduto.push(produto.dimensoes.profundidade[i].dimensaoMin);
            }
            if (produto.dimensoes.profundidade[i].dimensaoMax != null) {
              this.profundidadesMaxProduto.push(produto.dimensoes.profundidade[i].dimensaoMax);
            }
            if (produto.dimensoes.profundidade[i].dimensao != null) {
              this.profundidadesProduto.push(produto.dimensoes.profundidade[i].dimensao);
            }
          }
          for (let i = 0; i < produto.produtosAgregados.length; i++) {
            this.produtoService.buscarProdutoPorId(produto.produtosAgregados[i].idFilho).subscribe(filho => {
              this.produtosAgregados.push(filho.nome.conteudo);
            })
          }
        })
      }
    }
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.produtoId === objTwo.produtoId;
    }
  }

  atualizarDados() {
    this.catalogoEditarNome = this.catalogoEditar.nome.conteudo;
    this.produtoService.buscarProdutosPorCatalogo(this.catalogoEditar.catalogoId).subscribe(
      produtosCatalogo => {
        this.produtoCatalogoEditar = produtosCatalogo;
      }
    );
  }


  estadoCriarCatalogo() {
    this.catalogoHidden01 = !this.catalogoHidden01;
    this.catalogoHidden02 = false;
    this.catalogoHidden03 = false;
    this.catalogoHidden04 = false;
    this.catalogoHidden05 = false;

  }

  estadoListarCatalogos() {
    this.catalogoHidden01 = false;
    this.catalogoHidden02 = !this.catalogoHidden02;
    this.catalogoHidden03 = false;
    this.catalogoHidden04 = false;
    this.catalogoHidden05 = false;

  }
  estadoProcurarCatalogo() {
    this.catalogoHidden01 = false;
    this.catalogoHidden02 = false;
    this.catalogoHidden03 = !this.catalogoHidden03;
    this.catalogoHidden04 = false;
    this.catalogoHidden05 = false;
  }

  estadoEliminarCatalogo() {
    this.catalogoHidden01 = false;
    this.catalogoHidden02 = false;
    this.catalogoHidden03 = false;
    this.catalogoHidden04 = !this.catalogoHidden04;
    this.catalogoHidden05 = false;
  }

  estadoEditarCatalogo() {
    this.catalogoHidden01 = false;
    this.catalogoHidden02 = false;
    this.catalogoHidden03 = false;
    this.catalogoHidden04 = false;
    this.catalogoHidden05 = !this.catalogoHidden05;
  }

}
