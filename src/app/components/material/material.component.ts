import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Acabamento } from 'src/app/models/acabamento';
import { AcabamentoService } from 'src/app/services/acabamento.service';
import { MaterialService } from 'src/app/services/material.service';
import { Material } from 'src/app/models/material';
import { HistoricoPrecoService } from 'src/app/services/historico-preco.service';
import { HistoricoPreco } from 'src/app/models/historicoPreco';
import { MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  listaAcabamentos: Acabamento[] = [];
  listaMateriais: Material[] = [];
  listaHistoricos: HistoricoPreco[] = [];
  materialHidden01: boolean = false;
  materialHidden02: boolean = false;
  materialHidden03: boolean = false;
  materialHidden04: boolean = false;
  materialHidden05: boolean = false;
  acabamentosSelecionados: Acabamento[] = [];
  acabamentosSelecionadosEditar: Acabamento[] = [];
  materialApagar: Material;
  materialEditar: Material;
  editarNomeMaterial: string;
  nomeEditar: string;
  nomePesquisado: string = '';
  corPesquisada: string = '';
  opacidadePesquisada: string = '';
  foscoPesquisado: string = '';
  emissividadePesquisada: string = '';
  //precoEditar: number;
  ficheiro: File;
  ficheiroEditar: File;
  precoAcabamento: number[] = [];
  //precoAcabamentoEditar: number[] = [];
  precoMaterial: number = null;
  materialListar: Material = null;
  valorBrilhoListar: number = 0;
  valorOpacidadeListar: number = 0;
  valorEmissividadeListar: number = 0;
  valorRadioButtonListar: string = '';
  precoMaterialPesquisado: string = '';
  precoAcabamentoPesquisado: string = '';
  acabamentosListar: Acabamento[] = [];
  colorListar: string[] = [];
  valorRadioButtonLista: string[] = [];
  listaAcrescimos: string[] = [];
  texturasListar: any[] = [];
  indiceTexturaCriar: number = 0; // assumir que é sempre a primeira textura que quer.
  indiceTexturaEditar: number = null;
  nomeTexturaListar: string;
  imagemTexturaListar: string;
  indiceTabSelecionar: number;

  precoMaterialEditar: number;
  precoAcabamentoEditar: number[] = [];

  constructor(private acabamentoService: AcabamentoService, private materialService: MaterialService,
    private historicoService: HistoricoPrecoService) { }

  ngOnInit() {
    this.buscarMateriais();
    this.buscarAcabamentos();
    this.texturasListar.push({
      nome: 'Wenge 1',
      urlImagem: '../../../assets/materiais/madeira1.png'
    });
    this.texturasListar.push({
      nome: 'Wenge 2',
      urlImagem: '../../../assets/materiais/madeira2.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 1',
      urlImagem: '../../../assets/materiais/madeira3.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 2',
      urlImagem: '../../../assets/materiais/madeira4.jpg'
    });
    this.texturasListar.push({
      nome: 'Madeira 5',
      urlImagem: '../../../assets/materiais/madeira5.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 1',
      urlImagem: '../../../assets/materiais/vidro1.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 2',
      urlImagem: '../../../assets/materiais/vidro2.jpg'
    });
  }



  guardarIndiceImagem(event: MatTabChangeEvent) {
    this.indiceTexturaCriar = event.index;
  }

  guardarIndiceImagemEditar(event: MatTabChangeEvent) {
    this.indiceTexturaEditar = event.index;
  }



  buscarMateriais() {
    this.materialService.buscarMateriais().subscribe(materiais => { this.listaMateriais = materiais; });
  }
  buscarAcabamentos() {
    this.acabamentoService.buscarAcabamentos().subscribe(acabamentos => { this.listaAcabamentos = acabamentos; });
  }

  atualizarPrecosAcabamentos() {
    if (this.materialEditar != null) {
      this.precoAcabamentoEditar = [];
      this.acabamentosSelecionadosEditar.forEach(acabamentoAtual => {
        this.historicoService.buscarAcrescimoAtualMaterialAcabamento(this.materialEditar.id,
          acabamentoAtual.id).subscribe(
            historicoPreco => {
              let i: number;
              for (i = 0; i < this.acabamentosSelecionadosEditar.length; i++) {
                if (historicoPreco != null) {
                  if (this.acabamentosSelecionadosEditar[i].id == historicoPreco.id_Acabamento) {
                    this.precoAcabamentoEditar[i] = historicoPreco.preco.valor;
                  }
                }
              }
            }
          );
      });
    }
  }

  atualizarDados() { // para atualizar a UI quando se aperta num material
    this.nomeEditar = this.materialEditar.nome.conteudo;
    this.indiceTexturaEditar = this.materialEditar.texturaImagem;
    this.indiceTabSelecionar = this.materialEditar.texturaImagem;
    console.log(this.indiceTabSelecionar);
    this.editarNomeMaterial = this.materialEditar.nome.conteudo;
    let acabamentos: Acabamento[] = [];
    if (this.materialEditar.acabamentos != null) {
      for (let i = 0; i < this.materialEditar.acabamentos.length; i++) {
        let idAcabamento = this.materialEditar.acabamentos[i].idAcabamento;
        for (let j = 0; j < this.listaAcabamentos.length; j++) {
          let idAcabamento2 = this.listaAcabamentos[j].id;
          if (idAcabamento == idAcabamento2) {
            acabamentos.push(this.listaAcabamentos[j]);
          }
        }
      }
    }

    this.historicoService.buscarPrecoAtualMaterial(this.materialEditar.id).subscribe(
      historicoPreco => {
        this.precoMaterialEditar = historicoPreco.preco.valor;
      }
    );

    this.precoAcabamentoEditar = [];
    this.acabamentosSelecionadosEditar = acabamentos;
    this.acabamentosSelecionadosEditar.forEach(acabamentoAtual => {
      this.historicoService.buscarAcrescimoAtualMaterialAcabamento(this.materialEditar.id, acabamentoAtual.id).subscribe(
        historicoPreco => {
          let i: number;
          for (i = 0; i < this.acabamentosSelecionadosEditar.length; i++) {
            if (historicoPreco != null) {
              if (this.acabamentosSelecionadosEditar[i].id == historicoPreco.id_Acabamento) {
                this.precoAcabamentoEditar[i] = historicoPreco.preco.valor;
              }
            }
          }
        }
      );
    });
    this.ficheiroEditar = this.ficheiro;
  }

  adicionarMaterial(nomeAcabamento: string) {
    nomeAcabamento = nomeAcabamento.trim();
    if (!nomeAcabamento || nomeAcabamento.length < 2) {
      alert("O nome tem de conter pelo menos três caracteres!");
      return;
    }
    if (this.acabamentosSelecionados.length == 0) {
      alert('Selecione pelo menos um acabamentos');
      return;
    }
    if (this.indiceTexturaCriar == null) {
      alert('É necessário selecionar pelo menos uma textura');
      return;
    }
    if (!this.precoMaterial || this.precoMaterial <= 0) {
      alert("O material tem de ter preço válido.");
      return;
    }
    for (let i = 0; i < this.acabamentosSelecionados.length; i++) {
      if (this.precoAcabamento[i] == null || !this.precoAcabamento[i] || this.precoAcabamento[i] <= 0) {
        alert("O acabamento tem de ter preço válido ");
        return;
      }
    }

    // if (this.ficheiro == null) {
    //   alert("Insira uma textura");
    //   return;
    // }
    let idsAcabamentos = [];
    for (let i = 0; i < this.acabamentosSelecionados.length; i++) {
      idsAcabamentos[i] = { idAcabamento: this.acabamentosSelecionados[i].id }
    }
    let dataAtual = new Date();
    let dia = dataAtual.getDate();
    let mes = dataAtual.getUTCMonth();
    let ano = dataAtual.getUTCFullYear();
    let dataCriacao = new Date(ano, mes, dia, 0, 0, 0, 0);
    this.materialService.adicionarMaterial({
      nome: {
        conteudo: nomeAcabamento
      },
      acabamentos: idsAcabamentos,
      texturaImagem: this.indiceTexturaCriar
    } as Material).subscribe(material => {
      this.buscarMateriais();
      let precoTotal = 0;
      let historicoAdicionar1 = new HistoricoPreco();
      historicoAdicionar1.id_Material = material.id;
      historicoAdicionar1.id_Acabamento = null;
      historicoAdicionar1.preco = { valor: this.precoMaterial, dataInicio: dataCriacao };
      this.historicoService.adicionarHistorico(historicoAdicionar1).subscribe(historico => {
        this.listaHistoricos.push(historico);
      });
      if (this.acabamentosSelecionados != null && this.acabamentosSelecionados.length > 0) {
        for (let i = 0; i < this.acabamentosSelecionados.length; i++) {
          precoTotal = this.precoAcabamento[i];
          let historicoAdicionar = new HistoricoPreco();
          historicoAdicionar.id_Material = material.id;
          historicoAdicionar.id_Acabamento = this.acabamentosSelecionados[i].id;
          historicoAdicionar.preco = { valor: precoTotal, dataInicio: dataCriacao };
          this.historicoService.adicionarHistorico(historicoAdicionar).subscribe(historico => {
            this.listaHistoricos.push(historico);
          });
        }
      }
    });
  }

  eliminarMaterial() {
    if (!this.materialApagar) {
      alert('Por favor selecione o material que deseja apagar.');
      return;
    }
    let idMaterialApagar = this.materialApagar.id;
    for (let i = 0; i < this.listaHistoricos.length; i++) {
      this.historicoService.buscarHistoricoPorId(this.listaHistoricos[i].id).subscribe(historicoApagar => {
        if (historicoApagar.id_Material == idMaterialApagar) {
          let idHistorico = historicoApagar.id;
          this.historicoService.eliminarHistorico(idHistorico).subscribe(historicoEliminado => {
            let novaListaHistorico: HistoricoPreco[] = [];
            for (let i = 0; i < this.listaHistoricos.length; i++) {
              if (idHistorico != historicoEliminado.id) {
                novaListaHistorico.push(historicoApagar);
              }
            }
            this.listaHistoricos = novaListaHistorico;
          });
        };
      })
    }
    this.materialService.eliminarMaterial(idMaterialApagar).subscribe(materialEliminado => {
      this.buscarMateriais();
      this.materialApagar = null;
    })
  }

  atualizarMaterial() {
    if (this.nomeEditar.trim().length < 3) {
      alert('Introduza um nome com pelo menos três caracteres.');
      return;
    }
    if (this.acabamentosSelecionadosEditar.length == 0) {
      alert('Selecione pelo menos um acabamento');
      return;
    }
    if (this.indiceTexturaEditar == null) {
      alert('Selecione uma textura');
      return;
    }
    if (!this.precoMaterialEditar || this.precoMaterialEditar <= 0) {
      alert("O material tem de ter preço válido.");
      return;
    }
    for (let i = 0; i < this.acabamentosSelecionadosEditar.length; i++) {
      if (this.precoAcabamentoEditar[i] == null || !this.precoAcabamentoEditar[i] || this.precoAcabamentoEditar[i] <= 0) {
        alert("Os acabamentos têm de ter preço válido.");
        return;
      }
    }

    let idsAcabamentos = [];
    for (let i = 0; i < this.acabamentosSelecionadosEditar.length; i++) {
      idsAcabamentos[i] = { idAcabamento: this.acabamentosSelecionadosEditar[i].id }
    }
    this.materialService.atualizarMaterial({
      id: this.materialEditar.id,
      nome: {
        conteudo: this.nomeEditar
      },
      acabamentos: idsAcabamentos,
      texturaImagem: this.indiceTexturaEditar
    } as Material).subscribe(_ => {   // atualizar a lista dos materiais
      this.buscarMateriais();
      // this.precoMaterialEditar = null;
      // this.precoAcabamentoEditar = [];
      let historicoPrecoMaterial = new HistoricoPreco();
      let dataAtual = new Date();
      let dia = dataAtual.getDate();
      let mes = dataAtual.getUTCMonth();
      let ano = dataAtual.getUTCFullYear();
      let dataCriacao = new Date(ano, mes, dia, 0, 0, 0, 0);
      historicoPrecoMaterial.id_Material = this.materialEditar.id;
      historicoPrecoMaterial.id_Acabamento = null;
      historicoPrecoMaterial.preco = { valor: this.precoMaterialEditar, dataInicio: dataCriacao };
      this.historicoService.adicionarHistorico(historicoPrecoMaterial).subscribe(_ => {
        for (let i = 0; i < this.acabamentosSelecionadosEditar.length; i++) {
          let historicoPrecoAcabamentoEditar = new HistoricoPreco();
          historicoPrecoAcabamentoEditar.id_Material = this.materialEditar.id;
          historicoPrecoAcabamentoEditar.id_Acabamento = this.acabamentosSelecionadosEditar[i].id;
          historicoPrecoAcabamentoEditar.preco = { valor: this.precoAcabamentoEditar[i], dataInicio: dataCriacao };
          this.historicoService.adicionarHistorico(historicoPrecoAcabamentoEditar).subscribe(_ => {
            if (i == this.acabamentosSelecionadosEditar.length - 1) { // quando fizer o post do último histórico
              this.precoMaterialEditar = 0;
              this.precoAcabamentoEditar = [];
              this.materialEditar = null;
              this.nomeEditar = "";
              this.acabamentosSelecionadosEditar = [];
              this.indiceTabSelecionar = null;
            }
          });

        }

      });

    });

  }



  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }
  selecionarTodosAcabamentos(checkAll, select: NgModel, values) {
    if (checkAll) {
      select.update.emit(values);
    } else {
      select.update.emit([]);
    }

  }

  listarAcabamentos() {
    this.nomeTexturaListar = this.texturasListar[this.materialListar.texturaImagem].nome;
    this.imagemTexturaListar = this.texturasListar[this.materialListar.texturaImagem].urlImagem;
    this.historicoService.buscarPrecoAtualMaterial(this.materialListar.id).subscribe(
      precoAtual => {
        console.log('ola');
        console.log(precoAtual);
      }
    );
    this.listaAcrescimos = [];
    this.colorListar = [];
    this.valorRadioButtonLista = [];
    this.acabamentoService.buscarAcabamentoDeMaterial(this.materialListar.id).subscribe(
      acabamentosMaterial => {
        this.acabamentosListar = acabamentosMaterial;
        this.acabamentosListar.forEach(acabamentoAtual => {
          this.colorListar.push(acabamentoAtual.cor);
          this.valorRadioButtonLista.push(acabamentoAtual.fosco ? "2" : "1");
          this.historicoService.buscarAcrescimoAtualMaterialAcabamento(this.materialListar.id, acabamentoAtual.id).subscribe(
            historicoAtual => {
              if (historicoAtual != null) {
                this.listaAcrescimos.push(historicoAtual.preco.valor.toLocaleString() + "€");
              } else {
                this.listaAcrescimos.push('Sem acréscimo definido');
              }

            }
          );
        });
      }
    );
  }

  estadoCriarMaterial() {
    this.materialHidden01 = !this.materialHidden01;
    this.materialHidden02 = false;
    this.materialHidden03 = false;
    this.materialHidden04 = false;
    this.materialHidden05 = false;
  }

  estadoListarMateriais() {
    this.materialHidden01 = false;
    this.materialHidden02 = !this.materialHidden02;
    this.materialHidden03 = false;
    this.materialHidden04 = false;
    this.materialHidden05 = false;

  }
  estadoProcurarMaterial() {
    this.materialHidden01 = false;
    this.materialHidden02 = false;
    this.materialHidden03 = !this.materialHidden03;
    this.materialHidden04 = false;
    this.materialHidden05 = false;
  }

  estadoEliminarMaterial() {
    this.materialHidden01 = false;
    this.materialHidden02 = false;
    this.materialHidden03 = false;
    this.materialHidden04 = !this.materialHidden04;
    this.materialHidden05 = false;
  }

  estadoEditarMaterial() {
    this.materialHidden01 = false;
    this.materialHidden02 = false;
    this.materialHidden03 = false;
    this.materialHidden04 = false;
    this.materialHidden05 = !this.materialHidden05;
  }

}
