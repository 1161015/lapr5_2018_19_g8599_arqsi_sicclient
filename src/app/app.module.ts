import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {MaterialModule} from './material.module';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import {HttpModule} from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EncomendaComponent } from './components/encomenda/encomenda.component';
import { ItemComponent } from './components/item/item.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { MaterialComponent } from './components/material/material.component';
import { AcabamentoComponent } from './components/acabamento/acabamento.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { ColecaoComponent } from './components/colecao/colecao.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { LoginComponent } from './components/login/login.component';
import { RegistarComponent } from './components/registar/registar.component';
import { RestricaoComponent } from './components/restricao/restricao.component';
import { HistoricoprecoComponent } from './components/historicopreco/historicopreco.component';
import { PainelutilizadorComponent } from './components/painelutilizador/painelutilizador.component';
import { JwtInterceptor } from './utils/jtw.interceptor';
import { ErrorInterceptor } from './utils/error.interceptor';



@NgModule({
  declarations: [
    AppComponent,
    EncomendaComponent,
    ItemComponent,
    CategoriaComponent,
    MaterialComponent,
    AcabamentoComponent,
    ProdutoComponent,
    ColecaoComponent,
    CatalogoComponent,
    LoginComponent,
    RegistarComponent,
    RestricaoComponent,
    HistoricoprecoComponent,
    PainelutilizadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
     { provide: HTTP_INTERCEPTORS, useClass:JwtInterceptor, multi:true},
     { provide: HTTP_INTERCEPTORS, useClass:ErrorInterceptor, multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
