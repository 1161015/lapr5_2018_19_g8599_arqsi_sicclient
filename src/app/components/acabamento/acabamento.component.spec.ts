import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcabamentoComponent } from './acabamento.component';

import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatRadioModule, MatSliderModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatFormFieldModule } from '@angular/material/form-field';

describe('AcabamentoComponent', () => {
  let component: AcabamentoComponent;
  let fixture: ComponentFixture<AcabamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcabamentoComponent ],
      imports: [ MatFormFieldModule, ColorPickerModule, FormsModule, MatRadioModule, MatSliderModule, MatSelectModule, HttpClientTestingModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcabamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#estadoCriarAcabamento() should toggle #acabamentoHidden01', () => {
    expect(component.acabamentoHidden01).toBe(false, 'off at first');
    component.estadoCriarAcabamento();
    expect(component.acabamentoHidden01).toBe(true, 'on after pressed');
    component.estadoCriarAcabamento();
    expect(component.acabamentoHidden01).toBe(false, 'off after pressed again');
  });

  it('#estadoListarAcabamentos() should toggle #acabamentoHidden02', () => {
    expect(component.acabamentoHidden02).toBe(false, 'off at first');
    component.estadoListarAcabamentos();
    expect(component.acabamentoHidden02).toBe(true, 'on after pressed');
    component.estadoListarAcabamentos();
    expect(component.acabamentoHidden02).toBe(false, 'off after pressed again');
  });

  it('#estadoProcurarAcabamento() should toggle #acabamentoHidden03', () => {
    expect(component.acabamentoHidden03).toBe(false, 'off at first');
    component.estadoProcurarAcabamento();
    expect(component.acabamentoHidden03).toBe(true, 'on after pressed');
    component.estadoProcurarAcabamento();
    expect(component.acabamentoHidden03).toBe(false, 'off after pressed again');
  });

  it('#estadoEliminarAcabamento() should toggle #acabamentoHidden04', () => {
    expect(component.acabamentoHidden04).toBe(false, 'off at first');
    component.estadoEliminarAcabamento();
    expect(component.acabamentoHidden04).toBe(true, 'on after pressed');
    component.estadoEliminarAcabamento();
    expect(component.acabamentoHidden04).toBe(false, 'off after pressed again');
  });

  it('#estadoEditarAcabamento() should toggle #acabamentoHidden05', () => {
    expect(component.acabamentoHidden05).toBe(false, 'off at first');
    component.estadoEditarAcabamento();
    expect(component.acabamentoHidden05).toBe(true, 'on after pressed');
    component.estadoEditarAcabamento();
    expect(component.acabamentoHidden05).toBe(false, 'off after pressed again');
  });
});
