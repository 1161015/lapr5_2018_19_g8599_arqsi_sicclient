import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './services/login.service';
import { Utilizador } from './models/utilizador';
import { Cargo } from './models/cargo';

import { ItemComponent } from './components/item/item.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sic-cliente';
  utilizadorAtual: Utilizador;

  constructor(private router: Router, private loginService: LoginService) {
    this.loginService.utilizadorAtual.subscribe(utilizadorAtual => {
      this.utilizadorAtual = utilizadorAtual;
    });
  }

  ngOnInit() {
    ItemComponent.podeCriarScene = true;
  }

  get isGestorCatalogos() {
    return this.utilizadorAtual && this.utilizadorAtual.cargo === Cargo.GestorCatalogo;
  }

  get isCliente() {
    return this.utilizadorAtual && this.utilizadorAtual.cargo === Cargo.Cliente;
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/login']);
  }

}
