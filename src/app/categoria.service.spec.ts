import { TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';

import { CategoriaService } from './services/categoria.service';

describe('CategoriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
  ],
  }));

  it('should be created', () => {
    const service: CategoriaService = TestBed.get(CategoriaService);
    expect(service).toBeTruthy();
  });
});
