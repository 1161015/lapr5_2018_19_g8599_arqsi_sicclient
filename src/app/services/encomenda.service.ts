import { Injectable } from '@angular/core';

import { Observable, of, BehaviorSubject } from 'rxjs';
import { Encomenda } from '../models/encomenda';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Response } from '@angular/http';
import { Cidade } from '../models/cidade';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EncomendaService {

  private encomendaUrl = 'https://desolate-savannah-83248.herokuapp.com/encomenda';
 // private encomendaUrl = 'http://localhost:8080/encomenda';
  private cidadeUrl = 'https://desolate-savannah-83248.herokuapp.com/cidade';

  public encomendaUtilizadorNaoLogado: Encomenda;

  constructor(private http: HttpClient, private loginService: LoginService, private router: Router) {
    this.encomendaUtilizadorNaoLogado = null;
  }

  private log(message: string): void {
    console.log(message);
  }

  buscarEncomendas(): Observable<Encomenda[]> {
    return this.http.get<Encomenda[]>(this.encomendaUrl).pipe(
      tap(encomendas => {}),
      catchError(this.handleError('buscar encomendas', []))
    )
  };

  buscarEncomendasClienteEstado(estado: string): Observable<Encomenda[]> {
    let url = this.encomendaUrl + '/buscarEncomendas/' + this.loginService.currentUserValue.id + '/' + estado;
    return this.http.get<Encomenda[]>(url).pipe(
      tap(_ => { }), catchError(this.handleError('buscar encomendas de cliente por estado', []))
    );
  }

  buscarCidades(): Observable<Cidade[]> {
    return this.http.get<Cidade[]>(this.cidadeUrl).pipe(
      tap(_ => { }),
      catchError(this.handleError('buscar cidades', []))
    )
  };

  adicionarEncomenda(encomenda: Encomenda): Observable<any> {
    if (this.loginService.currentUserValue == null) {
      alert('Será redirecionado para a página de login');
      this.router.navigate(['/login']);
      this.encomendaUtilizadorNaoLogado = encomenda;
      return of<any>(); // basicamente retornar um empty observable
    }
    encomenda.IdCliente = this.loginService.currentUserValue.id;
    return this.http.post(this.encomendaUrl, encomenda, httpOptions).pipe(
      tap(_ => alert(`Adicionada a nova encomenda`)),
      catchError(this.handleError('adicionar item'))
    );
  }

  atualizarEncomenda(encomenda: Encomenda): Observable<any> { // por enquanto só é usado para mudar o estado
    let url = this.encomendaUrl + '/estado/' + encomenda.idEncomenda;
    return this.http.put(url, encomenda, httpOptions).pipe(
      tap(_ => { alert('Alterado o estado da encomenda para rececionada.') }),
      catchError(this.handleError('Atualizar encomenda', []))
    );
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.log(error);

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
