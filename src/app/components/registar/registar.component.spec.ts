import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistarComponent } from './registar.component';
import { HttpClientModule } from '@angular/common/http';
import { MatSelectModule, MatInputModule, MatTabsModule, MatCardModule, MatTooltipModule, MatIconModule, MatCheckboxModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from 'src/app/material.module';

describe('RegistarComponent', () => {
  let component: RegistarComponent;
  let fixture: ComponentFixture<RegistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistarComponent],
      imports:[HttpClientModule, MaterialModule, MatCheckboxModule, MatIconModule, MatSelectModule, MatInputModule, FormsModule, RouterTestingModule,
        HttpClientTestingModule, MatTabsModule, MatCardModule, MatTooltipModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/

});
