export class Material {
    id: number;
    nome: { conteudo: string };
    acabamentos: [{
        idAcabamento: number;
    }];
    texturaImagem: number;
    precoAtual : number; // só para mostrar na UI
}