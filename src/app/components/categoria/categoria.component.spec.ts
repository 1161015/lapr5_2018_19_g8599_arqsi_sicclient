import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaComponent } from './categoria.component';
import { MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CategoriaComponent', () => {
  let component: CategoriaComponent;
  let fixture: ComponentFixture<CategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriaComponent ],
      imports: [ FormsModule,   MatSelectModule, HttpClientTestingModule ],      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#estadoCriarCategoria() should toggle #categoriaHidden01', () => {
    expect(component.categoriaHidden01).toBe(false, 'off at first');
    component.estadoCriarCategoria();
    expect(component.categoriaHidden01).toBe(true, 'on after pressed');
    component.estadoCriarCategoria();
    expect(component.categoriaHidden01).toBe(false, 'off after pressed again');
  });

  it('#estadoListarCategorias() should toggle #categoriaHidden02', () => {
    expect(component.categoriaHidden02).toBe(false, 'off at first');
    component.estadoListarCategorias();
    expect(component.categoriaHidden02).toBe(true, 'on after pressed');
    component.estadoListarCategorias();
    expect(component.categoriaHidden02).toBe(false, 'off after pressed again');
  });

  it('#estadoProcurarCategoria() should toggle #categoriaHidden03', () => {
    expect(component.categoriaHidden03).toBe(false, 'off at first');
    component.estadoProcurarCategoria();
    expect(component.categoriaHidden03).toBe(true, 'on after pressed');
    component.estadoProcurarCategoria();
    expect(component.categoriaHidden03).toBe(false, 'off after pressed again');
  });

  it('#estadoEliminarCategoria() should toggle #categoriaHidden04', () => {
    expect(component.categoriaHidden04).toBe(false, 'off at first');
    component.estadoEliminarCategoria();
    expect(component.categoriaHidden04).toBe(true, 'on after pressed');
    component.estadoEliminarCategoria();
    expect(component.categoriaHidden04).toBe(false, 'off after pressed again');
  });

  it('#estadoEditarCategoria() should toggle #categoriaHidden05', () => {
    expect(component.categoriaHidden05).toBe(false, 'off at first');
    component.estadoEditarCategoria();
    expect(component.categoriaHidden05).toBe(true, 'on after pressed');
    component.estadoEditarCategoria();
    expect(component.categoriaHidden05).toBe(false, 'off after pressed again');
  });
});
