import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EncomendaComponent } from './components/encomenda/encomenda.component';
import { ItemComponent } from './components/item/item.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { AcabamentoComponent } from './components/acabamento/acabamento.component';
import { MaterialComponent } from './components/material/material.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { ColecaoComponent } from './components/colecao/colecao.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { LoginComponent } from './components/login/login.component';
import { RegistarComponent } from './components/registar/registar.component';
import { RestricaoComponent } from './components/restricao/restricao.component';
import { HistoricoprecoComponent } from './components/historicopreco/historicopreco.component';
import { PainelutilizadorComponent } from './components/painelutilizador/painelutilizador.component';
import { GateKeeper } from './guards/gateKeeper';
import { Cargo } from './models/cargo';


const routes: Routes = [
  {
    path: 'encomenda', component: EncomendaComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.Cliente] }
  },
  {
    path: 'item', component: ItemComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.Cliente] }
  },
  {
    path: 'produto', component: ProdutoComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'material', component: MaterialComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'acabamento', component: AcabamentoComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'categoria', component: CategoriaComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'colecao', component: ColecaoComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'catalogo', component: CatalogoComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  { path: 'login', component: LoginComponent },
  { path: 'registar', component: RegistarComponent },
  {
    path: 'restricao', component: RestricaoComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'historico', component: HistoricoprecoComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.GestorCatalogo] }
  },
  {
    path: 'painelutilizador', component: PainelutilizadorComponent,
    canActivate: [GateKeeper], data: { roles: [Cargo.Cliente] }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
