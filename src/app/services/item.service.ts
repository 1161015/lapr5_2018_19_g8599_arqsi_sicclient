import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Item } from '../models/item';
import { ProdutoService } from './produto.service';
import { MaterialService } from './material.service';
import { Acabamento } from '../models/acabamento';
import { AcabamentoService } from './acabamento.service';
import { CategoriaService } from './categoria.service';
import { HistoricoPrecoService } from './historico-preco.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private itemUrl = 'https://desolate-savannah-83248.herokuapp.com/item';
  // private itemUrl = 'http://localhost:8080/item';

  constructor(private httpClient: HttpClient, private produtoService: ProdutoService, private materialService: MaterialService, private acabamentoService: AcabamentoService, private categoriaService: CategoriaService, private historicoPrecoService: HistoricoPrecoService) { }

  private log(message: string): void {
    console.log(message);
  }

  buscarItens(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(this.itemUrl).pipe(
      tap(itens => {
        itens.forEach(itemAtual => {
          this.materialService.buscarMaterialPorId(itemAtual.idMaterial).subscribe(material => {
            itemAtual.nomeMaterial = material.nome.conteudo;
            itemAtual.material = material;
            console.log("OLA");
            this.acabamentoService.buscarAcabamentoPorId(itemAtual.idAcabamento).subscribe(acabamento => {
              itemAtual.nomeAcabamento = acabamento.nome.conteudo;
              itemAtual.acabamento = acabamento;
              if (typeof itemAtual.material !== undefined)
                this.historicoPrecoService.buscarAcrescimoAtualMaterialAcabamento(itemAtual.material.id, itemAtual.idAcabamento).subscribe(
                  precoAcabamento => {
                    itemAtual.precoAcabamento = precoAcabamento;
                  }
                );
            });
            this.historicoPrecoService.buscarPrecoAtualMaterial(itemAtual.material.id).subscribe(
              precoMaterial => {
                itemAtual.precoMaterial = precoMaterial;
              }
            );
          });

          this.produtoService.buscarProdutoPorId(itemAtual.idProduto).subscribe(produto => {
            itemAtual.nomeProduto = produto.nome.conteudo;
            itemAtual.produto = produto;
            this.categoriaService.buscarCategoriaPorId(produto.categoria.id).subscribe(categoria => {
              itemAtual.categoria = categoria;
            })
          })
        });
      },
        catchError(this.handleError('buscar itens', []))
      ));
  }

  buscarItensNaoEncomendados(): Observable<Item[]> {
    let urlFinal = this.itemUrl + '/itensNaoEncomendados';
    return this.httpClient.get<Item[]>(urlFinal).pipe(
      tap(itens => {
        itens.forEach(itemAtual => {
          this.materialService.buscarMaterialPorId(itemAtual.idMaterial).subscribe(material => {
            itemAtual.nomeMaterial = material.nome.conteudo;
            itemAtual.material = material;
            this.acabamentoService.buscarAcabamentoPorId(itemAtual.idAcabamento).subscribe(acabamento => {
              itemAtual.nomeAcabamento = acabamento.nome.conteudo;
              itemAtual.acabamento = acabamento;
              if (typeof itemAtual.material !== undefined)
                this.historicoPrecoService.buscarAcrescimoAtualMaterialAcabamento(itemAtual.material.id, itemAtual.idAcabamento).subscribe(
                  precoAcabamento => {
                    itemAtual.precoAcabamento = precoAcabamento;
                  }
                );
            });
            this.historicoPrecoService.buscarPrecoAtualMaterial(itemAtual.material.id).subscribe(
              precoMaterial => {
                itemAtual.precoMaterial = precoMaterial;
              }
            );
          });

          this.produtoService.buscarProdutoPorId(itemAtual.idProduto).subscribe(produto => {
            itemAtual.nomeProduto = produto.nome.conteudo;
            itemAtual.produto = produto;
            this.categoriaService.buscarCategoriaPorId(produto.categoria.id).subscribe(categoria => {
              itemAtual.categoria = categoria;
            })
          })
        });
      },
        catchError(this.handleError('buscar itens não encomendados', []))
      )
    );
  }


  eliminarItem(idItem: string): Observable<Item> {
    let url = this.itemUrl + '/' + idItem;
    return this.httpClient.delete<Item>(url, httpOptions).pipe(
      tap(_ => alert(`Apagado item`)),
      catchError(this.handleError<Item>('Apagar item'))
    );
  }

  atualizarItem(item: Item): Observable<any> {
    let url = this.itemUrl + '/' + item._id;
    return this.httpClient.put(url, item, httpOptions).pipe(
      tap(_ => alert(`Atualizado item`)),
      catchError(this.handleError<any>('atualizar item'))
    );
  }

  itensProduto(idProduto: number): Observable<Item[]> {
    let url = this.itemUrl + '/itensProduto/' + idProduto;
    return this.httpClient.get<Item[]>(url).pipe(
      tap(itens => {
        itens.forEach(itemAtual => {
          this.materialService.buscarMaterialPorId(itemAtual.idMaterial).subscribe(material => {
            itemAtual.nomeMaterial = material.nome.conteudo;
            itemAtual.material = material;
            this.acabamentoService.buscarAcabamentoPorId(itemAtual.idAcabamento).subscribe(acabamento => {
              itemAtual.nomeAcabamento = acabamento.nome.conteudo;
              itemAtual.acabamento = acabamento;
              if (typeof itemAtual.material !== undefined)
                this.historicoPrecoService.buscarAcrescimoAtualMaterialAcabamento(itemAtual.material.id, itemAtual.idAcabamento).subscribe(
                  precoAcabamento => {
                    itemAtual.precoAcabamento = precoAcabamento;
                  }
                );
            });
            this.historicoPrecoService.buscarPrecoAtualMaterial(itemAtual.material.id).subscribe(
              precoMaterial => {
                itemAtual.precoMaterial = precoMaterial;
              }
            );
          });

          this.produtoService.buscarProdutoPorId(itemAtual.idProduto).subscribe(produto => {
            itemAtual.nomeProduto = produto.nome.conteudo;
            itemAtual.produto = produto;
            this.categoriaService.buscarCategoriaPorId(produto.categoria.id).subscribe(categoria => {
              itemAtual.categoria = categoria;
            })
          })
        });
      },
        catchError(this.handleError('buscar itens', []))
      ),
      catchError(this.handleError('buscar itens de produto', []))
    );
  }

  adicionarItem(item: Item): Observable<any> {
    return this.httpClient.post(this.itemUrl, item, httpOptions).pipe(
      tap(_ => alert(`Adicionado o novo item`)),
      catchError(this.handleError('adicionar item'))
    );
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.log(error);
      //alert(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
