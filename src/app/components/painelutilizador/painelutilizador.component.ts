import { Component, OnInit } from '@angular/core';
import { EncomendaService } from '../../services/encomenda.service';
import { Cidade } from 'src/app/models/cidade';
import { Utilizador } from '../../models/utilizador';
import { Router } from '@angular/router';
import { PainelutilizadorService } from '../../services/painelutilizador.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-painelutilizador',
  templateUrl: './painelutilizador.component.html',
  styleUrls: ['./painelutilizador.component.css']
})
export class PainelutilizadorComponent implements OnInit {

  hide1 = false;
  hide2 = false;
  hide3 = false;

  apagarContaAlert = 0;

  mostrarPainelUtilizador = false;
  mostrarPainelVerificacao = true;
  tratamento = false;
  termos = false;

  mostrarBotaoVerificar: boolean = true;
  mostrarSpinnerVerificar: boolean = false;

  mostrarBotaoApagar: boolean = true;
  mostrarBotaoAtualizar: boolean = true;
  mostrarSpinnerAtualizar: boolean = false;

  cidades: Cidade[] = [];
  cidadeSelecionada: Cidade = null;
  nomeEditar: string;
  passwordEditar: string;
  passwordVerificarEditar: string;
  aceitarTratamento: boolean;

  constructor(private painelutilizadorService: PainelutilizadorService, private encomendaService: EncomendaService, private _router: Router, private loginService: LoginService) { }

  ngOnInit() {
    this.buscarCidades();
    this.atualizarInformacao();
  }

  buscarCidades(): any {
    this.encomendaService.buscarCidades().subscribe(cidades => {
      this.cidades = cidades;
    });
  }

  atualizarInformacao() {
    this.painelutilizadorService.buscarUtilizador().subscribe(utilizadorRecebido => {
      console.log(utilizadorRecebido);
      this.encomendaService.buscarCidades().subscribe(cidadesSelecionadas => {
        cidadesSelecionadas.forEach(cidadeAtual => {
          console.log('esta é a cidade atual ' +  cidadeAtual.Nome);
          if (cidadeAtual.Nome == utilizadorRecebido.morada.morada) {
            console.log('entrei');
            this.cidadeSelecionada = cidadeAtual;
          }
        });
        console.log(this.cidadeSelecionada);
      });

      this.nomeEditar = utilizadorRecebido.nome.nome;
      this.aceitarTratamento = utilizadorRecebido.tratamento;
      this.passwordEditar = null;
      this.passwordVerificarEditar = null;
    });
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.Nome === objTwo.Nome;
    }
  }

  mostrarTermos() {
    this.mostrarPainelUtilizador = false;
    this.mostrarPainelVerificacao = false;
    this.tratamento = false;
    this.termos = true;
  }

  mostrarTratamento() {
    this.mostrarPainelUtilizador = false;
    this.mostrarPainelVerificacao = false;
    this.termos = false;
    this.tratamento = true;
  }


  voltarTermos(){
    this.mostrarPainelUtilizador = true;
    this.mostrarPainelVerificacao = false;
    this.tratamento = false;
    this.termos = false;
  }

  voltarTratamento() {
    this.mostrarPainelUtilizador = true;
    this.mostrarPainelVerificacao = false;
    this.termos = false;
    this.tratamento = false;
  }

  abrirPainelUtilizador() {
    this.mostrarPainelUtilizador = true;
    this.mostrarPainelVerificacao = false;
    this.tratamento = false;
    this.termos = false;
  }

  abrirPaginaInicial(){
    this._router.navigate(['/']);
  }

  verificacaoUtilizador(passwordInserida: string) {
    var verificarPassword = /[#$%^&*/\[\]()"\\:{}|<>]/.test(passwordInserida);
    var verificarPassword2 = /(.){8,}/.test(passwordInserida);

    if (!verificarPassword2) {
      alert("Por favor, insira uma Password com mais que 8 caracteres.");
      return;
    }
    if (verificarPassword) {
      alert("Por favor, insira uma Password válida.");
      return;
    }
    this.mostrarBotaoVerificar = false;
    this.mostrarSpinnerVerificar = true;

    this.painelutilizadorService.revalidarUtilizador(passwordInserida).subscribe(_ => {
      //Tira Spinner
      this.mostrarBotaoVerificar = true;
      this.mostrarSpinnerVerificar = false;
      this.abrirPainelUtilizador();
      alert("Palavra-passe verificada com sucesso.");
    }, error => {
      alert('Password introduzida Incorreta!');
      this.mostrarBotaoVerificar = true;
      this.mostrarSpinnerVerificar = false;
    });
  }

  apagarConta() {
    if (this.apagarContaAlert == 0) {
      alert("Esta ação apaga a conta em uso, prossiga com cuidado!");
      this.apagarContaAlert = 1;
    } else if (this.apagarContaAlert == 1) {
      this.painelutilizadorService.apagarConta().subscribe(_ => {
        //Tira Spinner
        this.mostrarBotaoApagar = true;
        this.mostrarBotaoAtualizar = true;
        this.mostrarSpinnerAtualizar = false;
        this.loginService.logout();
        this._router.navigate(['/']);
        alert("Conta de Utilizador eliminada com sucesso.");
      }, error => {
        alert('Erro a Eliminar a Conta, por favor contacte um Desenvolvedor!');
        this.mostrarBotaoApagar = true;
        this.mostrarBotaoAtualizar = true;
        this.mostrarSpinnerAtualizar = false;
      });
    }
  }

  atualizarConta() {
    var verificaNome = /^[a-záàâãéèêíïóôõöúçñ ]+$/i.test(this.nomeEditar);
    var verificarPassword = /[#$%^&*/\[\]\\()":{}|<>]/.test(this.passwordEditar);
    var verificarPassword2 = /(.){8,}/.test(this.passwordEditar);
    var verificarPassword3 = /[0-9]{1,}/.test(this.passwordEditar);
    var verificarPassword4 = /[A-Z]{1,}/.test(this.passwordEditar);
    var verificarPassword5 = /[a-z]{1,}/.test(this.passwordEditar);
    var verificarPassword6 = /[,.!?@]{1}/.test(this.passwordEditar);
    var passwordsIguais;

    if (this.passwordEditar == this.passwordVerificarEditar) {
      passwordsIguais = true;
    } else {
      passwordsIguais = false;
    }
    if (!verificaNome) {
      alert("Por favor, insira um Nome válido.")
      return;
    }

    if (this.cidadeSelecionada == null) {
      alert("Por favor, selecione uma Cidade.")
      return;
    }
    if (!(this.passwordEditar == null && this.passwordVerificarEditar == null)) {
      if (!verificarPassword2) {
        alert("Por favor, insira uma Password com pelo menos 8 caracteres.");
        return;
      }
      if (verificarPassword) {
        alert("Por favor, insira uma Password válida.");
        return;
      }
      if (!verificarPassword3) {
        alert("Por favor, insira uma Password com pelo menos 1 algarismo.");
        return;
      }
      if (!verificarPassword4) {
        alert("Por favor, insira uma Password com pelo menos 1 letra maiúscula.");
        return;
      }
      if (!verificarPassword5) {
        alert("Por favor, insira uma Password com pelo menos 1 letra minúscula.");
        return;
      }
      if (!verificarPassword6) {
        alert("Por favor, insira uma Password com pelo menos 1 caracter especial.");
        return;
      }
      if (!passwordsIguais) {
        alert("Por favor, as Passwords inseridas precisam de ser iguais.");
        return;
      }
    }

    this.painelutilizadorService.atualizarConta(this.nomeEditar, this.cidadeSelecionada.Nome, this.passwordEditar, this.aceitarTratamento).subscribe(_ => {
      //Tira Spinner
      this.mostrarBotaoApagar = true;
      this.mostrarBotaoAtualizar = true;
      this.mostrarSpinnerAtualizar = false;
      this.atualizarInformacao();
      alert("Conta Atualizada com Sucesso.");
    }, error => {
      alert('Erro a Atualizar a Conta, por favor contacte um Desenvolvedor!');
      this.mostrarBotaoApagar = true;
      this.mostrarBotaoAtualizar = true;
      this.mostrarSpinnerAtualizar = false;
    });
  }
}
