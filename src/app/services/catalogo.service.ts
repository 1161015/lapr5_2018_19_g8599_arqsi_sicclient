import { Injectable } from '@angular/core';
import { CategoriaService } from './categoria.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Produto } from '../models/produto';
import { Catalogo } from '../models/catalogo';
import { ProdutoService } from './produto.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {
  private catalogoURL = 'https://sic-productions.azurewebsites.net/api/catalogo'

  constructor(private http: HttpClient, private produtoService: ProdutoService) { }

  private log(message: string): void {
    console.log(message);
  }

  criarCatalogo(catalogo: Catalogo): Observable<Catalogo> {
    return this.http.post<Catalogo>(this.catalogoURL, catalogo, httpOptions).pipe(
      tap((catalogo: Catalogo) => alert('Catalogo adicionado')),
      catchError(this.handleError<Catalogo>('adicionar catalogo'))
    );
  }

  buscarCatalogosProduto(idProduto: number): Observable<Catalogo[]> {
    let url = this.catalogoURL + '/produto/' + idProduto;
    return this.http.get<Catalogo[]>(url).pipe(
      tap(_ => {
      }),
      catchError(this.handleError('buscar catalogos de produto', [])
      )
    );
  }

  buscarCatalogos(): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(this.catalogoURL).pipe(
      tap(_ => {}),
      catchError(this.handleError('buscar catalogos', [])
      )
    );
  }


  eliminarCatalogo(id: number): Observable<Catalogo> {
    let url = this.catalogoURL + '/' + id;
    return this.http.delete<Catalogo>(url, httpOptions).pipe(
      tap(_ => alert('Catalogo Apagado')),
      catchError(this.handleError<Catalogo>('Apagar Catalogo'))
    );
  }

  buscarCatalogoPorId(idCatalogo: number): Observable<Catalogo> {
    let urlFinal = this.catalogoURL + '/' + idCatalogo;
    return this.http.get<Catalogo>(urlFinal).pipe(
      tap(catalogo => { }),
      catchError(this.handleError<Catalogo>(`Erro ao buscar o catalogo`)));
  }

  atualizarCatalogo(catalogo: Catalogo): Observable<any> {
    let url = this.catalogoURL + '/' + catalogo.catalogoId;
    return this.http.put(url, catalogo, httpOptions).pipe(
      tap(_ => alert('Catalogo atualizado')),
      catchError(this.handleError<any>('Atualizar catalogo'))
    );
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
