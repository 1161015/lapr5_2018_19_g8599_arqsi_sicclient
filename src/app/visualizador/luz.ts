import * as THREE from 'three';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class Luz {

    constructor() {
    }

    ligarLuzHolofote(spotLight) {
        spotLight = new THREE.SpotLight(0xffffff, 0, 1500);
        spotLight.name = 'Spot Light';
        spotLight.position.set(-120, 170, 370);
        spotLight.castShadow = true;
        spotLight.angle = 1.9
        spotLight.penumbra = 1;
        spotLight.shadow.camera.near = 75;
        spotLight.shadow.camera.far = 1500;
        spotLight.shadow.mapSize.width = 2048;
        spotLight.shadow.mapSize.height = 2048;
        return spotLight;
    }

    alterarStatusLuzHolofote(booleano, spotLight, scene) {
        if (booleano) {
            scene.add(spotLight);
        } else {
            scene.remove(spotLight);
        }
    }

    alterarIntensidadeLuzHolofote(valor, spotLight) {
        spotLight.intensity = valor;
    }

    alterarCorHolofote(valor, spotLight) {
        spotLight.color.set(valor);
    }
}