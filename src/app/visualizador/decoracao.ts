import * as THREE from 'three';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class Decoracao {

    constructor() {
    }

    init(scene, renderer, camera) {
      
        //Chão
        var texParede = new THREE.TextureLoader().load('assets/texturas/betao.jpg');
        this.desenharParedes(1000, 1000, 0, 0, 0, texParede, 1, true, scene);

        //Teto
        var texParede = new THREE.TextureLoader().load('assets/texturas/warehouseTop.jpg');
        this.desenharParedes(900, 800, 0, 370, 0, texParede, 2, false, scene);

        //Frente
        var texParede = new THREE.TextureLoader().load('assets/texturas/warehouseWall.jpg');
        this.desenharParedes(800, 700, 0, 150, -391, texParede, 3, false, scene);

        //Tras
        var texParede = new THREE.TextureLoader().load('assets/texturas/warehouseWall.jpg');
        this.desenharParedes(800, 700, 0, 150, 380, texParede, 4, true, scene);

        //Esquerda
        var texParede = new THREE.TextureLoader().load('assets/texturas/warehouseWall.jpg');
        this.desenharParedes(790, 720, -400, 150, 0, texParede, 5, false, scene);

        //Direita
        var texParede = new THREE.TextureLoader().load('assets/texturas/warehouseWall.jpg');
        this.desenharParedes(790, 720, 400, 150, 0, texParede, 6, false, scene);

        renderer.render(scene, camera);

        //Imagem1
        var texImagem = new THREE.TextureLoader().load('assets/texturas/luis.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 170, -10, geoImagem, matImagem, scene);

        //Quadro1
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this.desenharCaixaRotY(-399, 170, -10, geoQuadro, matQuadro, scene);

        //Imagem2
        var texImagem = new THREE.TextureLoader().load('assets/texturas/paulo.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 260, -45, geoImagem, matImagem, scene);

        //Quadro2
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this.desenharCaixaRotY(-399, 260, -45, geoQuadro, matQuadro, scene);

        //Imagem3
        var texImagem = new THREE.TextureLoader().load('assets/texturas/marisa.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 170, 60, geoImagem, matImagem, scene);

        //Quadro3
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this.desenharCaixaRotY(-399, 170, 60, geoQuadro, matQuadro, scene);

        //Imagem4
        var texImagem = new THREE.TextureLoader().load('assets/texturas/rafa.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 260, 25, geoImagem, matImagem, scene);

        //Quadro4
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this. desenharCaixaRotY(-399, 260, 25, geoQuadro, matQuadro, scene);

        //Imagem5
        var texImagem = new THREE.TextureLoader().load('assets/texturas/ricardo.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 170, 130, geoImagem, matImagem, scene);

        //Quadro5
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this.desenharCaixaRotY(-399, 170, 130, geoQuadro, matQuadro, scene);

        //Imagem6
        var texImagem = new THREE.TextureLoader().load('assets/texturas/rui.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 260, 95, geoImagem, matImagem, scene);

        //Quadro6
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this.desenharCaixaRotY(-399, 260, 95, geoQuadro, matQuadro, scene);

        //Imagem7
        var texImagem = new THREE.TextureLoader().load('assets/texturas/joao.jpg');
        var geoImagem = new THREE.PlaneGeometry(35, 50);
        var matImagem = new THREE.MeshBasicMaterial({ map: texImagem });

        this.desenharCaixaRotY(-398.3, 170, -80, geoImagem, matImagem, scene);

        //Quadro7
        var texQuadro = new THREE.TextureLoader().load('assets/texturas/quadro.jpg');
        var geoQuadro = new THREE.BoxBufferGeometry(50, 70, 1);
        var matQuadro = new THREE.MeshLambertMaterial({ map: texQuadro });

        this.desenharCaixaRotY(-399, 170, -80, geoQuadro, matQuadro, scene);

        //Janela
        var texJanela = new THREE.TextureLoader().load('assets/texturas/window.jpg');
        var geoJanela = new THREE.PlaneGeometry(180, 140);
        var matJanela = new THREE.MeshLambertMaterial({ map: texJanela, side: THREE.BackSide });

        var janela = this.desenharCaixa(-120, 170, 379, geoJanela, matJanela, scene);
        janela.castShadow = false;

        //Portao
        var texPortao = new THREE.TextureLoader().load('assets/texturas/portao.jpg');
        var geoPortao = new THREE.BoxBufferGeometry(500, 470, 10);
        var matPortao = new THREE.MeshLambertMaterial({ map: texPortao });

        var portao = this.desenharCaixa(0, 20, -390, geoPortao, matPortao, scene);
        portao.castShadow = false;
        portao.name = "Portao";

        //PortaoLadoEsq
        var texPortaoLadoEsq = new THREE.TextureLoader().load('assets/texturas/guarda.jpg');
        var geoPortaoLadoEsq = new THREE.BoxBufferGeometry(10, 505, 10);
        var matPortaoLadoEsq = new THREE.MeshLambertMaterial({ map: texPortaoLadoEsq });

        var portaoLadoEsq = this.desenharCaixa(255, 20, -390, geoPortaoLadoEsq, matPortaoLadoEsq, scene);
        portaoLadoEsq.castShadow = false;

        //PortaoLadoDir
        var texPortaoLadoDir = new THREE.TextureLoader().load('assets/texturas/guarda.jpg');
        var geoPortaoLadoDir = new THREE.BoxBufferGeometry(10, 505, 10);
        var matPortaoLadoDir = new THREE.MeshLambertMaterial({ map: texPortaoLadoDir });

        var portaoLadoDir = this.desenharCaixa(-255, 20, -390, geoPortaoLadoDir, matPortaoLadoDir, scene);
        portaoLadoDir.castShadow = false;

        //Guarda
        var texGuarda = new THREE.TextureLoader().load('assets/texturas/guarda.jpg');
        var geoGuarda = new THREE.BoxBufferGeometry(500, 20, 10);
        var matGuarda = new THREE.MeshBasicMaterial({ map: texGuarda });

        var guarda = this.desenharCaixa(0, 265, -390, geoGuarda, matGuarda, scene);
        guarda.castShadow = false;

        //MesaMetal
        var texMesaMetal = new THREE.TextureLoader().load('assets/texturas/metal.jpg');
        var geoMesaMetalPerna = new THREE.CylinderGeometry(5, 5, 100, 50);
        var geoMesaMetalTopo = new THREE.BoxBufferGeometry(360, 10, 110);
        var matMesaMetal = new THREE.MeshLambertMaterial({ map: texMesaMetal });

        this.desenharCaixa(300, 30, -30, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixa(390, 30, -30, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixa(300, 30, 310, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixa(390, 30, 310, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixaRotY(340, 80, 137, geoMesaMetalTopo, matMesaMetal, scene);
        this.desenharCaixa(300, 130, -30, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixa(390, 130, -30, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixa(300, 130, 310, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixa(390, 130, 310, geoMesaMetalPerna, matMesaMetal, scene);
        this.desenharCaixaRotY(340, 180, 137, geoMesaMetalTopo, matMesaMetal, scene);

        //Mesa
        var texMesa = new THREE.TextureLoader().load('assets/texturas/table.jpg');
        var geoMesaPerna = new THREE.BoxBufferGeometry(20, 50, 10);
        var geoMesaTopo = new THREE.BoxBufferGeometry(140, 10, 180);
        var matMesa = new THREE.MeshLambertMaterial({ map: texMesa });

        this.desenharCaixa(-320, 20, 100, geoMesaPerna, matMesa, scene);
        this.desenharCaixa(-320, 20, -40, geoMesaPerna, matMesa, scene);
        this.desenharCaixa(-380, 50, 26.5, geoMesaTopo, matMesa, scene);

        //Caixao
        var texCaixao = new THREE.TextureLoader().load('assets/texturas/crate.jpg');
        var geoCaixao = new THREE.BoxBufferGeometry(130, 40, 60);
        var matCaixao = new THREE.MeshLambertMaterial({ map: texCaixao });

        this.desenharCaixaRotY(-370, 20, 310, geoCaixao, matCaixao, scene);
        this.desenharCaixaRotYX(-370, 45, 210, geoCaixao, matCaixao, scene);
        this.desenharCaixaRotY(340, 110, 137, geoCaixao, matCaixao, scene);
        this.desenharCaixaRotY(340, 210, 207, geoCaixao, matCaixao, scene);


        //CAIXA
        var texCaixa = new THREE.TextureLoader().load('assets/texturas/crate.jpg');
        var geoCaixa = new THREE.BoxBufferGeometry(60, 60, 60);
        var matCaixa = new THREE.MeshLambertMaterial({ map: texCaixa });

        this.desenharCaixa(-370, 30, -110, geoCaixa, matCaixa, scene);
        this.desenharCaixa(-370, 90, -110, geoCaixa, matCaixa, scene);
        this.desenharCaixa(-310, 30, -110, geoCaixa, matCaixa, scene);
        this.desenharCaixa(370, 30, -110, geoCaixa, matCaixa, scene);
        this.desenharCaixa(310, 30, -110, geoCaixa, matCaixa, scene);
        this.desenharCaixa(340, 210, 37, geoCaixa, matCaixa, scene);
        this.desenharCaixa(340, 210, 97, geoCaixa, matCaixa, scene);
    }

    desenharParedes(l1, l2, x, y, z, texParede, n, face, scene) {
        texParede.wrapS = THREE.RepeatWrapping;
        texParede.wrapT = THREE.RepeatWrapping;
        texParede.repeat.set(4, 4);
        var geometryParede = new THREE.PlaneGeometry(l1, l2);
        if (face) {
            if (n == 1) {
                var materialParede = new THREE.MeshLambertMaterial({ map: texParede, side: THREE.DoubleSide });
            } else {
                var materialParede = new THREE.MeshLambertMaterial({ map: texParede, side: THREE.BackSide });
            }
        } else {
            var materialParede = new THREE.MeshLambertMaterial({ map: texParede, side: THREE.FrontSide });
        }
        var planoParede = new THREE.Mesh(geometryParede, materialParede);
        if (n == 5) {
            planoParede.rotation.y = Math.PI / 2;
        } else if (n == 6) {
            planoParede.rotation.y = - Math.PI / 2;
        } else if (n == 2 || n == 1) {
            planoParede.rotation.x += Math.PI / 2;
        }
        planoParede.position.x += x;
        planoParede.position.y += y;
        planoParede.position.z += z;
        planoParede.receiveShadow = true;
        scene.add(planoParede);
    }

    desenharCaixaRotY(x, y, z, geoCaixao, matCaixao, scene) {
        var meshCaixao = new THREE.Mesh(geoCaixao, matCaixao);
        meshCaixao.rotation.y = Math.PI / 2;
        meshCaixao.position.x += x;
        meshCaixao.position.y += y;
        meshCaixao.position.z += z;
        meshCaixao.castShadow = true;
        meshCaixao.receiveShadow = true;
        scene.add(meshCaixao);
    }

    desenharCaixaRotYX(x, y, z, geoCaixao, matCaixao, scene) {
        var meshCaixao = new THREE.Mesh(geoCaixao, matCaixao);
        meshCaixao.rotation.y = Math.PI / 2;
        meshCaixao.rotation.x = Math.PI / 1.15;
        meshCaixao.position.x += x;
        meshCaixao.position.y += y;
        meshCaixao.position.z += z;
        meshCaixao.castShadow = true;
        meshCaixao.receiveShadow = true;
        scene.add(meshCaixao);
        return meshCaixao;
    }

    desenharCaixa(x, y, z, geoCaixa, matCaixa, scene) {
        var meshCaixa = new THREE.Mesh(geoCaixa, matCaixa);
        meshCaixa.position.x += x;
        meshCaixa.position.y += y;
        meshCaixa.position.z += z;
        meshCaixa.castShadow = true;
        meshCaixa.receiveShadow = true;
        scene.add(meshCaixa);
        return meshCaixa;
    }
}
