import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoprecoComponent } from './historicopreco.component';
import { MatDatepickerModule, MatRadioModule, MatSelectModule, MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HistoricoprecoComponent', () => {
  let component: HistoricoprecoComponent;
  let fixture: ComponentFixture<HistoricoprecoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoprecoComponent ],
      imports: [ MatTableModule, MatDatepickerModule, FormsModule, MatRadioModule, MatSelectModule, HttpClientTestingModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoprecoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#estadoDefinirHistorico() should toggle #historicoHidden01', () => {
    expect(component.historicoHidden01).toBe(false, 'off at first');
    component.estadoDefinirHistorico();
    expect(component.historicoHidden01).toBe(true, 'on after pressed');
    component.estadoDefinirHistorico();
    expect(component.historicoHidden01).toBe(false, 'off after pressed again');
  });

  it('#estadoListarHistorico() should toggle #historicoHidden02', () => {
    expect(component.historicoHidden02).toBe(false, 'off at first');
    component.estadoListarHistorico();
    expect(component.historicoHidden02).toBe(true, 'on after pressed');
    component.estadoListarHistorico();
    expect(component.historicoHidden02).toBe(false, 'off after pressed again');
  });

  it('#estadoEliminarHistorico() should toggle #historicoHidden03', () => {
    expect(component.historicoHidden03).toBe(false, 'off at first');
    component.estadoEliminarHistorico();
    expect(component.historicoHidden03).toBe(true, 'on after pressed');
    component.estadoEliminarHistorico();
    expect(component.historicoHidden03).toBe(false, 'off after pressed again');
  });
});
