import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSelectModule } from '@angular/material/select';
import { RestricaoComponent } from './restricao.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { HttpClientTestingModule } from '@angular/common/http/testing';


import { FormsModule } from '@angular/forms';

describe('RestricaoComponent', () => {
  let component: RestricaoComponent;
  let fixture: ComponentFixture<RestricaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RestricaoComponent],
      imports: [MatSelectModule, FormsModule, MatRadioModule, MatSlideToggleModule, HttpClientTestingModule]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestricaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#estadoCriarRestricao() should toggle #restricaoCriar', () => {
    expect(component.restricaoCriar).toBe(false, "off at first");
    component.estadoCriarRestricao();
    expect(component.restricaoCriar).toBe(true, "on after pressed");
    component.estadoCriarRestricao();
    expect(component.restricaoCriar).toBe(false, "off after pressed again");
  });

  it('#estadoListarRestricoes() should toggle #restricoesListar', () => {
    expect(component.restricoesListar).toBe(false, "off at first");
    component.estadoListarRestricoes();
    expect(component.restricoesListar).toBe(true, "on after pressed");
    component.estadoListarRestricoes();
    expect(component.restricoesListar).toBe(false, "off after pressed again");
  });


  it('#estadoApagarRestricao() should toggle #restricaoApagar', () => {
    expect(component.restricaoApagar).toBe(false, "off at first");
    component.estadoApagarRestricao();
    expect(component.restricaoApagar).toBe(true, "on after pressed");
    component.estadoApagarRestricao();
    expect(component.restricaoApagar).toBe(false, "off after pressed again");
  });

  it('#estadoEditarRestricao() should toggle #restricaoEditar', () => {
    expect(component.restricaoEditar).toBe(false, "off at first");
    component.estadoEditarRestricao();
    expect(component.restricaoEditar).toBe(true, "on after pressed");
    component.estadoEditarRestricao();
    expect(component.restricaoEditar).toBe(false, "off after pressed again");
  });
});
