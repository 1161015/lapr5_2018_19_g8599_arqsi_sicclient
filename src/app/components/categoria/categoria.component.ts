import { Component, OnInit } from '@angular/core';
import { Categoria } from '../../models/categoria';
import { CategoriaService } from 'src/app/services/categoria.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  listaCategorias: Categoria[];
  listaCategoriasPaisPossiveis: Categoria[] = [];
  categoriaPaiCriar: Categoria;
  categoriaApagar: Categoria;
  categoriaEditar: Categoria;
  categoriaPaiEditar: Categoria;

  nomeEditar : string;

  /**
   * flags de controlo da user interface
   */
  categoriaHidden01: boolean = false;
  categoriaHidden02: boolean = false;
  categoriaHidden03: boolean = false;
  categoriaHidden04: boolean = false;
  categoriaHidden05: boolean = false;


  constructor(private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.buscarCategorias();
  }
  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  eliminarCategoria(): void {
    if (this.categoriaApagar == null) {
      alert('Selecione uma categoria');
      return;
    }
    this.categoriaService.eliminarCategoria(this.categoriaApagar.id).subscribe(_ => {
      this.buscarCategorias();
      this.categoriaApagar = null;
    }
    );
  }

  buscarCategorias() {
    this.categoriaService.buscarCategorias().subscribe(
      categorias => {
        
        this.listaCategorias = categorias;
        console.log(this.listaCategorias);
      }
    )
  }

  adicionarCategoria(nomeCategoria: string): void {
    nomeCategoria = nomeCategoria.trim();
    if (!nomeCategoria || nomeCategoria.length < 2) {
      alert('Por favor insira um nome com mais de três caracteres.')
      return;
    }
    let categoriaEnviar;
    if (this.categoriaPaiCriar == null) { // se não tiver selecionado uma categoria pai
      categoriaEnviar = {
        nome: {
          conteudo: nomeCategoria
        }
      }
    } else { // se existir uma categoria pai
      categoriaEnviar = {
        nome: {
          conteudo: nomeCategoria
        },
        categoriaPai: {
          id: this.categoriaPaiCriar.id
        }
      }
    }
    this.categoriaService.adicionarCategoria(categoriaEnviar as Categoria).subscribe(
      _ => {
        this.buscarCategorias();
      }
    )
  }

  atualizarCategoria(){
    this.nomeEditar = this.nomeEditar.trim();
    if(!this.nomeEditar || this.nomeEditar.length < 2){
      alert('Por favor insira um nome com pelo menos três caracteres.');
      return;
    }
    let categoriaAtualizada;
    if(this.categoriaPaiEditar==null){
      categoriaAtualizada = {
        id: this.categoriaEditar.id,
        nome: {
          conteudo: this.nomeEditar
        }
      }
    } else {
      categoriaAtualizada = {
        id: this.categoriaEditar.id,
        nome: {
          conteudo: this.nomeEditar
        },
        categoriaPai: {
          id: this.categoriaPaiEditar.id
        }
      }
    }
    console.log(categoriaAtualizada);
    this.categoriaService.atualizarCategoria(categoriaAtualizada as Categoria).subscribe( _ => {
      this.buscarCategorias();
    })
  }

  atualizarDados(){
    let j;
    this.listaCategoriasPaisPossiveis = [];
    for(j=0;j<this.listaCategorias.length;j++){
      if(this.listaCategorias[j].id!=this.categoriaEditar.id){
        this.listaCategoriasPaisPossiveis.push(this.listaCategorias[j]);
      }
    }
    this.nomeEditar = this.categoriaEditar.nome.conteudo;
    let i;
    if(this.categoriaEditar.categoriaPai!=null){
      for(i=0;i<this.listaCategoriasPaisPossiveis.length;i++){
        if(this.listaCategoriasPaisPossiveis[i].id == this.categoriaEditar.categoriaPai.id){
          this.categoriaPaiEditar = this.listaCategoriasPaisPossiveis[i];
        }
      }
    } else {
      this.categoriaPaiEditar = null;
    }
  }

  estadoCriarCategoria() {
    this.categoriaHidden01 = !this.categoriaHidden01;
    this.categoriaHidden02 = false;
    this.categoriaHidden03 = false;
    this.categoriaHidden04 = false;
    this.categoriaHidden05 = false;

  }
  estadoListarCategorias() {
    this.categoriaHidden01 = false;
    this.categoriaHidden02 = !this.categoriaHidden02;
    this.categoriaHidden03 = false;
    this.categoriaHidden04 = false;
    this.categoriaHidden05 = false;

  }
  estadoProcurarCategoria() {
    this.categoriaHidden01 = false;
    this.categoriaHidden02 = false;
    this.categoriaHidden03 = !this.categoriaHidden03;
    this.categoriaHidden04 = false;
    this.categoriaHidden05 = false;

  }

  estadoEliminarCategoria() {
    this.categoriaHidden01 = false;
    this.categoriaHidden02 = false;
    this.categoriaHidden03 = false;
    this.categoriaHidden04 = !this.categoriaHidden04;
    this.categoriaHidden05 = false;
  }

  estadoEditarCategoria() {
    this.categoriaHidden01 = false;
    this.categoriaHidden02 = false;
    this.categoriaHidden03 = false;
    this.categoriaHidden04 = false;
    this.categoriaHidden05 = !this.categoriaHidden05;
  }
}
