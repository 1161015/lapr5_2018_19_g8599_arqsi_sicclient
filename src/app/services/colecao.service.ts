import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Colecao } from '../models/colecao';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ColecaoService {

  private colecaoURL = 'https://sic-productions.azurewebsites.net/api/colecao';

  constructor(private http: HttpClient) { }

  private log(message: string): void {
    console.log(message);
  }

  adicionarColecao(colecao: Colecao): Observable<Colecao> {
    return this.http.post<Colecao>(this.colecaoURL, colecao, httpOptions).pipe(
      tap((colecao: Colecao) => alert(`Colecao adicionada`)),
      catchError(this.handleError<Colecao>('adicionar colecao'))
    );
  }

  buscarColecoes(): Observable<Colecao[]> {
    return this.http.get<Colecao[]>(this.colecaoURL).pipe(
      tap(_ => {} ),
      catchError(this.handleError('buscar coleções', [])
      )
    );
  }

  buscarColecoesDeCatalogo(idCatalogo: number): Observable<Colecao[]> {
    let url = this.colecaoURL + '/catalogo/' + idCatalogo;
    return this.http.get<Colecao[]>(url).pipe(
      tap(),
      catchError(this.handleError<Colecao[]>('buscar coleções de um catálogo')))
      ;
  }

  buscarColecaoPorId(id: number): Observable<Colecao> {
    let url = this.colecaoURL + '/' + id;
    return this.http.get<Colecao>(url).pipe(
      tap(_ => this.log(`Coleção carregada`)),
      catchError(this.handleError<Colecao>(`Erro ao buscar coleção com o id=${id}`))
    );
  }

  eliminarColecao(id: number): Observable<Colecao> {
    let url = this.colecaoURL + '/' + id;
    return this.http.delete<Colecao>(url, httpOptions).pipe(
      tap(_ => alert(`Coleção eliminada`)),
      catchError(this.handleError<Colecao>('Apagar coleção'))
    );
  }

  atualizarColecao(colecao: Colecao): Observable<any> {
    let url = this.colecaoURL + '/' + colecao.id;
    return this.http.put(url, colecao, httpOptions).pipe(
      tap(_ => alert(`Colecao atualizada`)),
      catchError(this.handleError<any>('atualizar colecao'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
