import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as jwt from 'jsonwebtoken';

import { LoginService } from '../services/login.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private loginService: LoginService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // adicionar o token de autorização caso o user já se encontre logado
        let currentUser = this.loginService.currentUserValue;
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.token}`
                }
            });
        } else if (currentUser == null && (request.url.indexOf('https://desolate-savannah-83248.herokuapp.com/item') !== -1
            || request.url.indexOf('http://localhost:8080/item') !== -1)) {
            const { secret } = {
                "secret": "6cQ4vG4aeP6ATcJulsoiQBOUawU5VciM"
            };
            let token = jwt.sign({}, secret, {
                expiresIn: 30 // expira em meio minuto
            });
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });

        }
        return next.handle(request);
    }
}