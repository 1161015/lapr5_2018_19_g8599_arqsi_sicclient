import { Component, OnInit } from '@angular/core';
import { Acabamento } from 'src/app/models/acabamento';
import { AcabamentoService } from 'src/app/services/acabamento.service';

@Component({
  selector: 'app-acabamento',
  templateUrl: './acabamento.component.html',
  styleUrls: ['./acabamento.component.css']
})
export class AcabamentoComponent implements OnInit {

  acabamentos: Acabamento[] = [];

  acabamentoListar: Acabamento;

  acabamentoApagar: Acabamento;
  acabamentoEditar: Acabamento;
  acabamentoBuscar: Acabamento;

  color: string;
  colorListar: string;
  valorRadioButton: number;
  valorBrilho: number;
  valorEmissividade: number;
  valorOpacidade: number;

  /** 
   * editar acabamento
   */
  nomeAcabamentoEditar : string;
  colorEditar : string;
  valorBrilhoEditar : number;
  valorOpacidadeEditar : number;
  valorEmissividadeEditar : number;
  valorBrilhoFosco : string;




  acabamentoEditarNome: string;
  acabamentoMostrarNome: string;
  /**
   * flags de controlo da user interface
   */
  acabamentoHidden01: boolean = false;
  acabamentoHidden02: boolean = false;
  acabamentoHidden03: boolean = false;
  acabamentoHidden04: boolean = false;
  acabamentoHidden05: boolean = false;
  acabamentoHidden06: boolean = true;

  constructor(private acabamentoService: AcabamentoService) { }

  ngOnInit() {
    this.buscarAcabamentos();
  }

  listarCorAcabamento(){
    console.log(this.acabamentoListar);
    this.colorListar = this.acabamentoListar.cor;
  }

  buscarAcabamentos() {
    this.acabamentoService.buscarAcabamentos().subscribe(acabamentos => {
      this.acabamentos = acabamentos;
    });
  }

  buscarAcabamento() {
    if (this.acabamentoBuscar == null) {
      alert('Por favor selecione um acabamento.');
      return;
    }
    this.acabamentoService.buscarAcabamentoPorId(this.acabamentoBuscar.id).subscribe(acabamento => {
      this.acabamentoMostrarNome = acabamento.nome.conteudo;
    });
  }
  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  atualizarAcabamento(): void {
    this.nomeAcabamentoEditar = this.nomeAcabamentoEditar.trim();
    if (!this.nomeAcabamentoEditar || this.nomeAcabamentoEditar.length < 2) {
      alert("Certifique-se que introduziu um nome válido (3 caractéres mínimo).");
      return;
    }
    this.acabamentoService.atualizarAcabamento(
      {
        id: this.acabamentoEditar.id,
        nome: {
          conteudo: this.nomeAcabamentoEditar
        },
        brilho: this.valorBrilhoEditar,
        emissividade: this.valorEmissividadeEditar,
        opacidade: this.valorOpacidadeEditar,
        fosco: this.valorBrilhoFosco == "2" ? true : false,
        cor: this.colorEditar
      } as Acabamento
    ).subscribe(_ => {
      this.buscarAcabamentos();
    });
  }

  atualizarDados() {
    console.log(this.acabamentoEditar);
    this.nomeAcabamentoEditar = this.acabamentoEditar.nome.conteudo;
    this.colorEditar = this.acabamentoEditar.cor;
    this.valorBrilhoEditar = this.acabamentoEditar.brilho;
    this.valorEmissividadeEditar = this.acabamentoEditar.emissividade;
    this.valorOpacidadeEditar = this.acabamentoEditar.opacidade;
    this.valorBrilhoFosco = this.acabamentoEditar.fosco == true ? "2" : "1";
  }

  adicionarAcabamento(nomeAcabamento: string): void {
    console.log(this.color);
    nomeAcabamento = nomeAcabamento.trim();
    if (!nomeAcabamento || nomeAcabamento.length < 2) {
      alert("Certifique-se que introduziu um nome válido (3 caractéres mínimo).");
      return;
    }
    if(this.valorRadioButton==null){
      alert("Certifique-se que selecionou brilhante ou fosco.");
      return;
    }
    let acabamento = new Acabamento();
    acabamento.nome = {
      conteudo: nomeAcabamento
    };
    console.log(acabamento);
    acabamento.brilho = this.valorBrilho;
    acabamento.cor = this.color;
    acabamento.emissividade = this.valorEmissividade;
    acabamento.opacidade = this.valorOpacidade;
    if (this.valorRadioButton == 1) { // brilhante
      acabamento.fosco = false;
    } else if (this.valorRadioButton == 2) { // fosco
      acabamento.fosco = true;
    }
    console.log(acabamento);
    this.acabamentoService.adicionarAcabamento(acabamento).subscribe(acabamento => { this.acabamentos.push(acabamento) });
  }

  eliminarAcabamento(): void {
    if (this.acabamentoApagar == null) {
      alert('Por favor selecione um acabamento.');
      return;
    }
    this.acabamentoService.eliminarAcabamento(this.acabamentoApagar.id).subscribe(
      _ => { // efetuar atualização da lista de acabamentos 
        this.buscarAcabamentos();
        this.acabamentoApagar = null;
      }
      /*,
      _ => { 'Eventual tratamento de erros...'}*/
    );

  }


  estadoCriarAcabamento() {
    this.acabamentoHidden01 = !this.acabamentoHidden01;
    this.acabamentoHidden02 = false;
    this.acabamentoHidden03 = false;
    this.acabamentoHidden04 = false;
    this.acabamentoHidden05 = false;

  }
  estadoListarAcabamentos() {
    this.acabamentoHidden01 = false;
    this.acabamentoHidden02 = !this.acabamentoHidden02;
    this.acabamentoHidden03 = false;
    this.acabamentoHidden04 = false;
    this.acabamentoHidden05 = false;

  }
  estadoProcurarAcabamento() {
    this.acabamentoHidden01 = false;
    this.acabamentoHidden02 = false;
    this.acabamentoHidden03 = !this.acabamentoHidden03;
    this.acabamentoHidden04 = false;
    this.acabamentoHidden05 = false;

  }

  estadoEliminarAcabamento() {
    this.acabamentoHidden01 = false;
    this.acabamentoHidden02 = false;
    this.acabamentoHidden03 = false;
    this.acabamentoHidden04 = !this.acabamentoHidden04;
    this.acabamentoHidden05 = false;
  }

  estadoEditarAcabamento() {
    this.acabamentoHidden01 = false;
    this.acabamentoHidden02 = false;
    this.acabamentoHidden03 = false;
    this.acabamentoHidden04 = false;
    this.acabamentoHidden05 = !this.acabamentoHidden05;
  }
}
