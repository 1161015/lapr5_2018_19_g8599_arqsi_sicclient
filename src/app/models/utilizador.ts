export class Utilizador {
    nome: { nome: string };
    email: { email: string };
    password: { password: string };
    username: { username: string };
    morada: { morada: string }
    token: string;
    termos: boolean;
    tratamento: boolean;
    codigo: { codigo: string };
    id: number;
    cargo: string;
}