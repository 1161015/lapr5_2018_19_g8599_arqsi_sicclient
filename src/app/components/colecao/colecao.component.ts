import { Component, OnInit } from '@angular/core';
import { Produto } from '../../models/produto';
import { Catalogo } from '../../models/catalogo';
import { Colecao } from 'src/app/models/colecao';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { ProdutoService } from 'src/app/services/produto.service';
import { ColecaoService } from 'src/app/services/colecao.service';
import { MaterialService } from 'src/app/services/material.service';
import { NgModel } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { Material } from '../../models/material';

@Component({
  selector: 'app-colecao',
  templateUrl: './colecao.component.html',
  styleUrls: ['./colecao.component.css']
})
export class ColecaoComponent implements OnInit {

  listaColecoes: Colecao[] = [];
  listaCatalogos: Catalogo[] = [];
  listaProdutos: Produto[] = [];
  colecaoHidden01: boolean = false;
  colecaoHidden02: boolean = false;
  colecaoHidden03: boolean = false;
  colecaoHidden04: boolean = false;
  produtosSelecionados: Produto[] = [];
  produtosSelecionadosEditar: Produto[] = [];
  produtosColecao: Produto[] = [];

  colecaoSelecionada: Colecao;
  catalogoSelecionado: Catalogo;
  catalogoSelecionadoEditar: Catalogo;
  produtosCatalogo: Produto[];
  colecaoApagar: Colecao;
  colecaoEditar: Colecao;
  colecaoListar: Colecao;
  nomeEditar: string;
  nomePesquisado: string;
  catalogosProduto: Catalogo[];

  materiaisProduto: Material[] = [];
  alturasProduto: number[] = [];
  alturasMinProduto: number[] = [];
  alturasMaxProduto: number[] = [];
  largurasMinProduto: number[] = [];
  largurasMaxProduto: number[] = [];
  largurasProduto: number[] = [];
  profundidadesProduto: number[] = [];
  profundidadesMinProduto: number[] = [];
  profundidadesMaxProduto: number[] = [];
  produtosAgregados: string[] = [];
  imagemTexturaListar: string[] = [];
  texturasListar: any[] = [];
  nomeTexturaListar: string[] = [];

  constructor(private catalogoService: CatalogoService, private produtoService: ProdutoService, private materialService: MaterialService,
    private colecaoService: ColecaoService) { }

  ngOnInit() {
    this.buscarColecoes();
    this.buscarCatalogos();
    this.buscarProdutos();
    this.texturasListar.push({
      nome: 'Wenge 1',
      urlImagem: '../../../assets/materiais/madeira1.png'
    });
    this.texturasListar.push({
      nome: 'Wenge 2',
      urlImagem: '../../../assets/materiais/madeira2.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 1',
      urlImagem: '../../../assets/materiais/madeira3.jpg'
    });
    this.texturasListar.push({
      nome: 'Teka 2',
      urlImagem: '../../../assets/materiais/madeira4.jpg'
    });
    this.texturasListar.push({
      nome: 'Madeira 5',
      urlImagem: '../../../assets/materiais/madeira5.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 1',
      urlImagem: '../../../assets/materiais/vidro1.jpg'
    });
    this.texturasListar.push({
      nome: 'Vidro 2',
      urlImagem: '../../../assets/materiais/vidro2.jpg'
    });
  }

  limparDados() {
    this.produtosSelecionados = [];
    this.produtosSelecionadosEditar = [];
    this.produtosColecao = [];

    this.colecaoSelecionada = null;
    this.catalogoSelecionado = null;
    this.catalogoSelecionadoEditar = null;
    this.produtosCatalogo = [];
    this.colecaoApagar = null;
    this.colecaoEditar = null;
    this.colecaoListar = null;
  }


  estadoCriarColecao() {
    this.colecaoHidden01 = !this.colecaoHidden01;
    this.colecaoHidden02 = false;
    this.colecaoHidden03 = false;
    this.colecaoHidden04 = false;
    this.limparDados();
  }

  estadoListarColecoes() {
    this.colecaoHidden01 = false;
    this.colecaoHidden02 = !this.colecaoHidden02;
    this.colecaoHidden03 = false;
    this.colecaoHidden04 = false;
    this.limparDados();
  }


  estadoEliminarColecao() {
    this.colecaoHidden01 = false;
    this.colecaoHidden02 = false;
    this.colecaoHidden03 = !this.colecaoHidden03;
    this.colecaoHidden04 = false;
    this.limparDados();
  }

  estadoEditarColecao() {
    this.colecaoHidden01 = false;
    this.colecaoHidden02 = false;
    this.colecaoHidden03 = false;
    this.colecaoHidden04 = !this.colecaoHidden04;
    this.limparDados();
  }

  buscarCatalogos() {
    this.catalogoService.buscarCatalogos().subscribe(catalogos => { this.listaCatalogos = catalogos; });
  }
  buscarProdutos() {
    this.produtoService.buscarProdutos().subscribe(produtos => { this.listaProdutos = produtos; });

  }

  buscarColecoes() {
    this.colecaoService.buscarColecoes().subscribe(colecoes => { this.listaColecoes = colecoes });
  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  eliminarColecao() {
    if (!this.colecaoApagar) {
      alert('Por favor selecione a coleção que deseja apagar.');
      return;
    }
    let idColecaoApagar = this.colecaoApagar.id;

    this.colecaoService.eliminarColecao(idColecaoApagar).subscribe(_ => {
      this.buscarColecoes();
      this.colecaoApagar = null;
    }
    )
  }

  adicionarColecao(nomeColecao: string) {
    nomeColecao = nomeColecao.trim();
    if (!nomeColecao || nomeColecao.length < 2) {
      alert("O nome tem de conter pelo menos três caracteres!");
      return;
    }

    if (this.catalogoSelecionado == null) {
      alert("Tem de selecionar um catálogo.");
      return;
    }

    this.colecaoService.adicionarColecao({
      nome: {
        conteudo: nomeColecao
      },
      catalogo: { catalogoId: this.catalogoSelecionado.catalogoId }
    } as Colecao).subscribe(colecao => {

      this.listaColecoes.push(colecao);



      this.produtosSelecionados.forEach(produtoAtual => {
        produtoAtual.colecao = {
          id: colecao.id
        };
        this.produtoService.atualizarproduto(produtoAtual).subscribe();
      });

    });
  }

  ListarProdutosCatalogoAoCriar() {
    this.produtoService.buscarProdutosPorCatalogo(this.catalogoSelecionado.catalogoId).subscribe(
      listaProdutos => {
        this.produtosCatalogo = listaProdutos;

      }
    );
  }

  ListarProdutosCatalogoAoEditar() {
    this.produtoService.buscarProdutosPorCatalogo(this.catalogoSelecionadoEditar.catalogoId).subscribe(
      listaProdutos => {
        this.produtosCatalogo = listaProdutos;

      }
    );
  }

  selecionarTodosProdutos(checkAll, select: NgModel, values) {
    if (checkAll) {
      select.update.emit(values);
    } else {
      select.update.emit([]);
    }
  }


  atualizarDados() { // para atualizar a UI quando se aperta numa colecao
    this.nomeEditar = this.colecaoEditar.nome.conteudo;
    let idCatalogo = this.colecaoEditar.catalogo.catalogoId;


    this.catalogoService.buscarCatalogoPorId(idCatalogo).subscribe(catalogo => {
      this.catalogoSelecionadoEditar = catalogo;
    });
    this.produtoService.buscarProdutosPorCatalogo(idCatalogo).subscribe(lista => {
      this.produtosCatalogo = lista;
    });
    this.produtoService.buscarProdutosPorColecao(this.colecaoEditar.id).subscribe(lista => {
      this.produtosSelecionadosEditar = lista;
    });
    this.produtoService.buscarProdutosPorColecao(this.colecaoEditar.id).subscribe(lista => {
      this.produtosSelecionadosEditar = lista;
    });
  }



  atualizarColecao() {

    if (this.colecaoEditar == null) {
      alert('Selecione uma coleção.');
      return;
    }
    if (this.nomeEditar == null || this.nomeEditar.trim().length < 3) {
      alert('Introduza um nome com pelo menos três caracteres.');
      return;
    }
    if (this.catalogoSelecionadoEditar == null) {
      alert('Selecione um catálogo.');
      return;
    }

    let idsProdutos = [];
    for (let i = 0; i < this.produtosSelecionadosEditar.length; i++) {
      idsProdutos[i] = { idProduto: this.produtosSelecionadosEditar[i].produtoId }
    }

    this.colecaoService.atualizarColecao({
      id: this.colecaoEditar.id,
      nome: {
        conteudo: this.nomeEditar
      },
      catalogo: {
        catalogoId: this.catalogoSelecionadoEditar.catalogoId
      }
    } as Colecao).subscribe(_ => {// atualizar a lista dos colecoes
      this.produtoService.buscarProdutosPorCatalogo(this.colecaoEditar.catalogo.catalogoId).subscribe(
        produtosCatalogo => {
          produtosCatalogo.forEach(produtoAtual => {
            let contem = false;
            for (let i = 0; i < this.produtosSelecionadosEditar.length; i++) {
              if (produtoAtual.produtoId == this.produtosSelecionadosEditar[i].produtoId) {
                contem = true;
                break;
              }
            }
            if (!contem) {
              if (produtoAtual.colecao != null) {
                if (produtoAtual.colecao.id == this.colecaoEditar.id) {
                  produtoAtual.colecao = null;
                }
              }
            } else {
              produtoAtual.colecao = {
                id: this.colecaoEditar.id
              }
            }
            this.produtoService.atualizarproduto(produtoAtual).subscribe();
          });
        }
      );
      this.buscarColecoes();
    });


  }

  equalsProduto(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.produtoId === objTwo.produtoId;
    }
  }

  listarProdutos() {

    if (this.colecaoListar != null) {
      this.produtoService.buscarProdutosPorColecao(this.colecaoListar.id).subscribe(
        produtosDaColecao => {
          this.produtosColecao = produtosDaColecao;
        }
      )
    }
  }

  listarProduto(event: MatTabChangeEvent) {
    this.alturasProduto = []
    this.alturasMinProduto = [];
    this.alturasMaxProduto = [];
    this.largurasProduto = [];
    this.largurasMinProduto = [];
    this.largurasMaxProduto = [];
    this.profundidadesProduto = [];
    this.profundidadesMinProduto = [];
    this.profundidadesMaxProduto = [];
    this.produtosAgregados = [];
    this.nomeTexturaListar = [];
    this.imagemTexturaListar = [];
  

    for (let i = 0; i < this.produtosColecao.length; i++) {
      if (i == event.index) {
        this.materialService.buscarMateriaisDeProduto(this.produtosColecao[i].produtoId).subscribe(materiaisP => {
          this.materiaisProduto = materiaisP;
          for (let i = 0; i < materiaisP.length; i++) {
            this.nomeTexturaListar.push(this.texturasListar[materiaisP[i].texturaImagem].nome);
            this.imagemTexturaListar.push(this.texturasListar[materiaisP[i].texturaImagem].urlImagem);
          }
        });
        this.produtoService.buscarProdutoPorId(this.produtosColecao[i].produtoId).subscribe(produto => {
          for (let i = 0; i < produto.dimensoes.altura.length; i++) {
            if (produto.dimensoes.altura[i].dimensaoMin != null) {
              this.alturasMinProduto.push(produto.dimensoes.altura[i].dimensaoMin);
            }
            if (produto.dimensoes.altura[i].dimensaoMax != null) {
              this.alturasMaxProduto.push(produto.dimensoes.altura[i].dimensaoMax);
            }
            if (produto.dimensoes.altura[i].dimensao != null) {
              this.alturasProduto.push(produto.dimensoes.altura[i].dimensao);
            }
          }
          for (let i = 0; i < produto.dimensoes.largura.length; i++) {
            if (produto.dimensoes.largura[i].dimensaoMin != null) {
              this.largurasMinProduto.push(produto.dimensoes.largura[i].dimensaoMin);
            }
            if (produto.dimensoes.largura[i].dimensaoMax != null) {
              this.largurasMaxProduto.push(produto.dimensoes.largura[i].dimensaoMax);
            }
            if (produto.dimensoes.largura[i].dimensao != null) {
              this.largurasProduto.push(produto.dimensoes.largura[i].dimensao);
            }
          }
          for (let i = 0; i < produto.dimensoes.profundidade.length; i++) {
            if (produto.dimensoes.profundidade[i].dimensaoMin != null) {
              this.profundidadesMinProduto.push(produto.dimensoes.profundidade[i].dimensaoMin);
            }
            if (produto.dimensoes.profundidade[i].dimensaoMax != null) {
              this.profundidadesMaxProduto.push(produto.dimensoes.profundidade[i].dimensaoMax);
            }
            if (produto.dimensoes.profundidade[i].dimensao != null) {
              this.profundidadesProduto.push(produto.dimensoes.profundidade[i].dimensao);
            }
          }
          for (let i = 0; i < produto.produtosAgregados.length; i++) {
            this.produtoService.buscarProdutoPorId(produto.produtosAgregados[i].idFilho).subscribe(filho => {
              this.produtosAgregados.push(filho.nome.conteudo);
            })
          }
          this.catalogoService.buscarCatalogosProduto(produto.produtoId).subscribe(listaCatalogos => {
            this.catalogosProduto = listaCatalogos;
          })
        })
      }
    }
  }

}



