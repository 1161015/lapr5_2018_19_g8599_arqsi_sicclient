export class Produto {
    produtoId: number;
    nome: { conteudo: string };
    categoria: {
        id: number;
        nome?:{
            conteudo:string;
        }
    }
    colecao: {
        id: number
        nome?:{
            conteudo:string;
        };
    };
    materiais: [{
        idMaterial: number;
    }];
    produtosAgregados: [{
        idFilho: number;
    }];
    dimensoes: {
        altura: [{
            dimensaoMin: number;
            dimensaoMax: number;
            dimensao: number;
        }],
        largura: [{
            dimensaoMin: number;
            dimensaoMax: number;
            dimensao: number;
        }],
        profundidade: [{
            dimensaoMin: number;
            dimensaoMax: number;
            dimensao: number;
        }],
    };
    produtoCatalogos: [{
        catalogoId: number;
    }];

}
