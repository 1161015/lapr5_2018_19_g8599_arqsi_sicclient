import { Injectable } from '@angular/core';
import { Utilizador } from '../models/utilizador';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class RegistarService {

  private registarURL = 'https://sic-authz.azurewebsites.net/api/utilizador';

  constructor(private httpClient: HttpClient) { }

  registarUtilizador(utilizador: Utilizador): Observable<Utilizador> {
    return this.httpClient.post<Utilizador>(this.registarURL, utilizador, httpOptions).pipe(
      tap(_ => {
      }, catchError(this.handleError<Utilizador>('adicionar utilizador')))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      console.log(error.error.text);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
