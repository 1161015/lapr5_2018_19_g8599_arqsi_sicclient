import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { Acabamento } from '../models/acabamento';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AcabamentoService {

  private acabamentoURL = 'https://sic-productions.azurewebsites.net/api/acabamento';

  constructor(private http: HttpClient) { }

  private log(message: string) {
    console.log(message);
  }

  buscarAcabamentos(): Observable<Acabamento[]> {
    return this.http.get<Acabamento[]>(this.acabamentoURL).pipe(
      tap(_ => {}),
      catchError(this.handleError('buscar acabamentos', [])
      )
    );
  }

  buscarAcabamentoDeMaterial(idMaterial: number): Observable<Acabamento[]> {
    let url = this.acabamentoURL + '/material/' + idMaterial;
    return this.http.get<Acabamento[]>(url).pipe(
      tap(acabamentos => { }), catchError(this.handleError('buscarAcabamentos', []))
    );
  }

  adicionarAcabamento(acabamento: Acabamento): Observable<Acabamento> {
    return this.http.post<Acabamento>(this.acabamentoURL, acabamento, httpOptions).pipe(
      tap((_: Acabamento) => alert(`Acabamento adicionado`)),
      catchError(this.handleError<Acabamento>('adicionar acabamento'))
    );
  }

  atualizarAcabamento(acabamento: Acabamento): Observable<any> {
    let url = this.acabamentoURL + '/' + acabamento.id;
    return this.http.put(url, acabamento, httpOptions).pipe(
      tap(_ => alert('Acabamento atualizado')),
      catchError(this.handleError<any>('Atualizar acabamento'))
    );
  }

  eliminarAcabamento(idAcabamento: number): Observable<Acabamento> {
    let url = this.acabamentoURL + '/' + idAcabamento;
    return this.http.delete<Acabamento>(url, httpOptions).pipe(
      tap(_ => { alert('Acabamento eliminado com sucesso.'); }),
      catchError(this.handleError<Acabamento>('Apagar acabamento'))
    );
  }

  buscarAcabamentoPorId(idAcabamento: number): Observable<Acabamento> {
    let urlFinal = this.acabamentoURL + '/' + idAcabamento;
    return this.http.get<Acabamento>(urlFinal).pipe(
      tap(_ => {}),
      catchError(this.handleError<Acabamento>(`Erro ao buscar o acabamento com o id=${idAcabamento}`))
    );
  }




  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
