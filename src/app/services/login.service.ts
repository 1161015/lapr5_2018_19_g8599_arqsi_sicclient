import { Injectable, Injector } from '@angular/core';
import { Utilizador } from '../models/utilizador';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { EncomendaService } from './encomenda.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginURL = 'https://sic-authz.azurewebsites.net/api/utilizador';
  //private loginURL = 'https://localhost:44309/api/utilizador';

  private utilizadorAtualSujeito: BehaviorSubject<Utilizador>;
  public utilizadorAtual: Observable<Utilizador>;
  private encomendaService: EncomendaService;

  constructor(private httpClient: HttpClient, injector: Injector) {
    this.utilizadorAtualSujeito = new BehaviorSubject<Utilizador>(JSON.parse(localStorage.getItem('utilizadorAtual')));
    this.utilizadorAtual = this.utilizadorAtualSujeito.asObservable();
    setTimeout(() => this.encomendaService = injector.get(EncomendaService));
  }

  public get currentUserValue(): Utilizador {
    return this.utilizadorAtualSujeito.value;
  }

  entrarUtilizador(utilizador: Utilizador): Observable<Utilizador> {
    let url = this.loginURL + '/autenticacao';
    return this.httpClient.post<Utilizador>(url, utilizador, httpOptions).pipe(
      tap(_ => { }, catchError(this.handleError<Utilizador>('entrar utilizador')))
    );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('utilizadorAtual');
    this.utilizadorAtualSujeito.next(null);
  }


  recuperarPassword(utilizador: Utilizador): Observable<any> {
    let url = this.loginURL + '/recuperar';
    return this.httpClient.post(url, utilizador, httpOptions).pipe(
      tap(_ => { }),
      catchError(this.handleError<any>('Recuperar password'))
    );
  }

  logarDoisFatores(utilizador: Utilizador): Observable<Utilizador> {
    let url = this.loginURL + '/codigo';
    return this.httpClient.post<Utilizador>(url, utilizador, httpOptions).pipe(
      tap(utilizadorLogado => {
        if (utilizadorLogado && utilizadorLogado.token) {
          localStorage.setItem('utilizadorAtual', JSON.stringify(utilizadorLogado));
          this.utilizadorAtualSujeito.next(utilizadorLogado);
          alert("Foi logado com sucesso!");
          if (utilizadorLogado.cargo == 'Cliente') {
            if (this.encomendaService.encomendaUtilizadorNaoLogado != null) {
              if (confirm('Efetou uma encomenda enquanto não estava logado.\nPretende submetê-la?')) {
                this.encomendaService.adicionarEncomenda(this.encomendaService.encomendaUtilizadorNaoLogado).subscribe(
                  _ => {
                  }
                );
              }
              this.encomendaService.encomendaUtilizadorNaoLogado = null;
            }
          }

        }
      }, catchError(this.handleError<Utilizador>('codigo email')))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      alert(error);
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
